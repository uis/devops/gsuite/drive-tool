import io
import json
import logging
import re
from datetime import datetime, timedelta, timezone
from json import JSONDecodeError
from math import ceil
from ssl import SSLError
from time import sleep

from google.oauth2 import service_account
from googleapiclient import discovery, http
from googleapiclient.errors import HttpError

from .parse_config import ConfigurationConsumer
from .utils import grouper, human_size

LOG = logging.getLogger(__name__)


class EmptyHttpResponse:
    status = 400
    reason = "Empty Response"


class ListTimeoutError(Exception):
    """
    Exception class for timeout when performing a list all operation (typically scanning)
    """

    pass


class GoogleAPIConnection(ConfigurationConsumer):
    required_config = ("google", "connection")

    read_only_scopes = []
    write_scopes = []

    def __init__(self, configuration, write_mode=False):
        super().__init__(configuration)
        # Load appropriate Google credentials.
        creds_file = self.google_config.credentials
        if not write_mode and self.google_config.read_only_credentials is not None:
            creds_file = self.google_config.read_only_credentials
            LOG.info("Using read-only credentials.")

        LOG.info(f"Loading Google account credentials from {creds_file}.")
        self.creds = service_account.Credentials.from_service_account_file(creds_file)

        # With scopes based on read only
        self.creds = self.creds.with_scopes(
            self.read_only_scopes + ([] if not write_mode else self.write_scopes)
        )

        self.write_mode = write_mode

        self.service = None

    def process_requests(
        self, requests, ignore_404=False, ignore_invalid_shares=False, request_emails=None
    ):
        """
        Process an iterable list of requests to the specified Google service in batches.
        These APIs support a maximum batch size of 1000. See:
        https://developers.google.com/admin-sdk/directory/v1/guides/batch

        """

        # Number of 404s and 400s ignored
        ignore_count = 0

        # Whether current batch needs retrying
        retry_batch = False

        # Which Google accounts are missing
        missing_accounts = set()

        def _handle_batch_response(request_id, response, exception):
            nonlocal ignore_count, retry_batch, missing_accounts

            if exception is not None:
                # Whether to ignore this exception or re-raise it (causing retry of the same
                # batch content)
                ignore = False

                if isinstance(exception, HttpError):
                    # Removing permissions (especially anyoneWithLink ones) can result in a 404 as
                    # the permission has already been removed, so we can ignore these by setting
                    # ignore_404
                    if ignore_404 and exception.resp.status == 404:
                        ignore = True
                    # Adding permissions for someone who no longer has a Google account results in
                    # a 400 with error reason of "invalidSharingRequest". We can ignore these by
                    # setting ignore_invalid_shares
                    elif ignore_invalid_shares and exception.resp.status == 400:
                        try:
                            exception_content = json.loads(exception.content)
                            response_errors = exception_content["error"]["errors"]
                            if any(
                                error["reason"] == "invalidSharingRequest"
                                for error in response_errors
                            ):
                                ignore = True
                                # Extract the accounts from the exception message and add them to
                                # the missing_accounts set
                                accounts = _get_accounts_from_message(
                                    exception_content["error"]["message"]
                                )
                                if not accounts:
                                    LOG.warning(
                                        f"Failed to extract accounts from exception message "
                                        f"{exception_content['error']['message']}"
                                    )
                                new_missing = accounts - missing_accounts
                                if new_missing:
                                    LOG.warning(f"New missing accounts: {', '.join(new_missing)}")
                                    missing_accounts |= new_missing

                                # Flag that we need to retry the batch (ignoring the missing
                                # accounts). Don't do this if there weren't any else we're
                                # get stuck in a loop.
                                retry_batch = len(accounts) > 0
                        except (KeyError, JSONDecodeError):
                            LOG.warning(f"Failed to decode exception content {exception.content}")
                            pass

                if ignore:
                    LOG.debug(f"Ignoring {exception.resp.status} error: {exception}")
                    ignore_count += 1
                else:
                    LOG.error(f"Error performing request: {exception}")
                    LOG.error(f"Response: {response}")
                    raise exception

        first_in_loop = True
        total_batches = ceil(len(requests) / self.connection_config.batch_size)
        batch_number = 0
        # Keeping track of how far through the requests we are so we can index into request_emails
        request_idx = 0
        for request_batch in grouper(requests, n=self.connection_config.batch_size):
            batch_number += 1
            # convert to list as grouper returns an iterator, and we may be re-enumerating it
            request_batch = list(request_batch)
            while True:
                retry_batch = False

                # Form batch request.
                batch = self.service.new_batch_http_request()
                batch_count = 0
                for idx, request in enumerate(request_batch):
                    if (
                        request_emails is not None
                        and request_emails[idx + request_idx] in missing_accounts
                    ):
                        LOG.debug(f"Skipping request for {request_emails[idx + request_idx]}")
                        continue
                    batch.add(request, callback=_handle_batch_response)
                    batch_count += 1

                previous_ignore_count = ignore_count

                LOG.info(f"Batch {batch_number} of {total_batches} ({batch_count} requests)")

                # Execute the batch request if not in read only mode. Otherwise log that we
                # would have.
                if self.write_mode:
                    if first_in_loop:
                        first_in_loop = False
                    else:
                        sleep(self.connection_config.inter_batch_delay)
                    if batch_count > 0:
                        LOG.info("Issuing batch request to Google.")
                        _retry_http_errors(
                            batch.execute,
                            self.connection_config.http_retries,
                            self.connection_config.http_retry_delay,
                        )
                else:
                    LOG.info("Not issuing batch request in read-only mode.")

                if previous_ignore_count != ignore_count:
                    LOG.info(
                        f"Ignored {ignore_count - previous_ignore_count} 400/404s "
                        f"- {ignore_count} total for user"
                    )

                if retry_batch:
                    LOG.info(f"Retrying batch {batch_number} due to missing accounts")
                else:
                    # Move on index so we can index into request_emails for the next batch
                    request_idx += self.connection_config.batch_size
                    break

    def process_request(self, request, retries=True):
        if self.write_mode or request.method in ["GET", "HEAD"]:
            LOG.info("Issuing request to Google.")
            return _retry_http_errors(
                request.execute,
                self.connection_config.http_retries if retries else 0,
                self.connection_config.http_retry_delay,
            )
        else:
            LOG.info("Not issuing request in read-only mode.")

    def list_all(
        self,
        list_cb,
        *,
        items_key="items",
        allow_empty=False,
        timeout=None,
        **kwargs,
    ):
        """
        Simple wrapper for Google Client SDK list()-style callables. Repeatedly fetches pages of
        results merging all the responses together. Returns the merged "items" arrays from the
        responses. The key used to get the "items" array from the response may be overridden via
        the items_key argument. allow_empty determines if resulting resource list can be empty

        """
        # Loop while we wait for nextPageToken to be "none"
        page_token = None
        resources = []
        shared_count = 0

        if timeout is not None:
            LOG.info(f"List all operation will timeout at {timeout}")

        # Internal function to run as part of the _retry_http_errors function
        def return_and_check_response(list_cb, page_token, resources, allow_empty, **kwargs):
            response = list_cb(pageToken=page_token, **kwargs).execute()
            # Sometimes Google API will give a nextPageToken even if all data fitted exactly
            # into the page. This next page then returns an empty response. Allow this if we've
            # already retrieved some results or allow_empty was set
            if not response and not (resources or allow_empty):
                raise HttpError(EmptyHttpResponse(), b"")

            return response

        while True:
            list_response = _retry_http_errors(
                lambda: return_and_check_response(
                    list_cb, page_token, resources, allow_empty, **kwargs
                ),
                self.connection_config.http_retries,
                self.connection_config.http_retry_delay,
            )
            items = list_response.get(items_key, [])
            resources.extend(items)

            # Useful when monitoring scanning to get an idea of how many items so far have a
            # True shared property
            shared_count += len([True for i in items if i.get("shared")])
            LOG.info(
                f"Fetched page with {len(items)} items - total {len(resources)}"
                + (f" ({shared_count} shared)" if shared_count else "")
            )

            # Get the token for the next page
            page_token = list_response.get("nextPageToken")
            if page_token is None:
                break

            # If we're using a timeout and we've now passed it with still more pages to retrieve
            # then raise ListTimeoutError to abort
            if timeout is not None and datetime.now(timezone.utc) > timeout:
                raise ListTimeoutError()

        return resources


class GoogleDirectoryConnection(GoogleAPIConnection):
    required_config = ("google", "connection")

    read_only_scopes = [
        "https://www.googleapis.com/auth/admin.directory.user.readonly",
    ]
    write_scopes = [
        "https://www.googleapis.com/auth/admin.directory.user",
    ]

    def __init__(self, configuration, write_mode=False):
        super().__init__(configuration, write_mode)
        # Build the directory service using Google API discovery.
        self.service = discovery.build("admin", "directory_v1", credentials=self.creds)
        # Use admin_user if using service account with Domain-Wide Delegation

    def retrieve_users(self, show_deleted=False, suspended_only=True):
        classification = "deleted" if show_deleted else "suspended" if suspended_only else "all"
        LOG.info(f"Getting information on {classification} Google domain users.")
        fields = [
            "id",
            "isAdmin",
            "orgUnitPath",
            "primaryEmail",
            "suspended",
            "lastLoginTime",
            "name(givenName, familyName)",
            "customSchemas",
        ] + (["deletionTime"] if show_deleted else [])
        full_query = "isAdmin=false" + (" isSuspended=true" if suspended_only else "")
        all_google_users = self.list_all(
            self.service.users().list,
            items_key="users",
            domain=self.google_config.domain_name,
            showDeleted=show_deleted,
            query=full_query,
            fields="nextPageToken,users(" + ",".join(fields) + ")",
            projection="custom",
            customFieldMask="UCam",
            maxResults=500,
            allow_empty=show_deleted,
        )

        # Sanity check. There should be no admins in the returned results.
        if any(u.get("isAdmin", False) for u in all_google_users):
            raise RuntimeError("Sanity check failed: admin users in user list")

        # Also, there should be no active users in querying for suspended users
        if suspended_only and any(not u.get("suspended", False) for u in all_google_users):
            raise RuntimeError("Sanity check failed: non-suspended users in suspended user list")

        return all_google_users

    def update_user_ucam_fields(self, user_id, ucam_patch_data):
        LOG.info(f"Updating custom UCam fields for {user_id}")
        self.process_request(
            self.service.users().patch(
                userKey=user_id, body={"customSchemas": {"UCam": ucam_patch_data}}
            )
        )

    def batch_update_user_ucam_fields(self, data):
        LOG.info("Batch updating custom UCam fields.")
        requests = []
        for user_id, ucam_patch_data in data.items():
            requests.append(
                self.service.users().patch(
                    userKey=user_id, body={"customSchemas": {"UCam": ucam_patch_data}}
                )
            )

        self.process_requests(requests)

    def retrieve_user(self, user_key):
        # We possibly don't need all these but it matches those returned by retrieve_users()
        fields = [
            "id",
            "isAdmin",
            "orgUnitPath",
            "primaryEmail",
            "suspended",
            "lastLoginTime",
            "name(givenName, familyName)",
            "customSchemas",
        ]
        try:
            return (
                self.service.users()
                .get(
                    userKey=user_key,
                    fields=",".join(fields),
                    projection="custom",
                    customFieldMask="UCam",
                )
                .execute()
            )
        except HttpError as e:
            # users().get() can return 403 or 404 if the user doesn't exist
            if e.resp.status in (403, 404):
                return None
            raise

    def user_has_google_account(self, id):
        LOG.info(f"Checking if {id} has a Google account.")
        user = self.retrieve_user(id)
        return user is not None


class GoogleDriveConnection(GoogleAPIConnection):
    required_config = ("google", "connection", "limits")

    read_only_scopes = [
        "https://www.googleapis.com/auth/drive.metadata.readonly",
        "https://www.googleapis.com/auth/drive.readonly",
    ]
    write_scopes = [
        "https://www.googleapis.com/auth/drive",
    ]

    def __init__(self, configuration, impersonate=None, write_mode=False):
        super().__init__(configuration, write_mode)
        # load credentials
        self.impersonate = impersonate
        if self.impersonate:
            self.creds = self.creds.with_subject(self.impersonate)
        # Build the directory service using Google API discovery.
        self.service = discovery.build("drive", "v3", credentials=self.creds)

    def get_all_owned_files_metadata(self, query=""):
        LOG.info(
            "Getting file metadata information for files belonging to "
            f"{self.impersonate if self.impersonate else 'service account'}"
            f"{' with query: ' + query if query else ''}"
        )

        fields = [
            "id",
            "name",
            "mimeType",
            "parents",
            "shared",
            "permissions/id",
            "permissions/emailAddress",
            "permissions/type",
            "permissions/role",
            "permissions/domain",
            "permissions/allowFileDiscovery",
        ]

        full_query = "trashed=false and 'me' in owners"
        if query:
            full_query += " and " + query

        # Set timeout to be a specified minutes from now
        timeout = datetime.now(timezone.utc) + timedelta(
            minutes=self.limits_config.max_user_scan_duration
        )

        all_files_metadata = self.list_all(
            self.service.files().list,
            items_key="files",
            corpora="user",
            spaces="drive",
            q=full_query,
            allow_empty=False,
            pageSize=500,
            fields="nextPageToken,files(" + ",".join(fields) + ")",
            timeout=timeout,
        )

        return all_files_metadata

    def get_all_files_in_drive_metadata(self, drive_id):
        LOG.info(f"Getting all files in drive {drive_id}")
        search_args = {
            "q": "trashed=false",
            "corpora": "drive",
            "driveId": drive_id,
        }

        file_metadatas = self.list_all(
            self.service.files().list,
            items_key="files",
            allow_empty=False,
            pageSize=100,
            fields="nextPageToken,files(id,modifiedTime,name)",
            supportsAllDrives=True,
            includeItemsFromAllDrives=True,
            orderBy="modifiedTime",
            **search_args,
        )

        return file_metadatas

    def batch_remove_permissions(self, data, is_shared_drive=False):
        num_perms = sum([len(l) for _, l in data.items()])
        LOG.info(f"Removing {num_perms} permissions from {len(data)} items")
        requests = []
        for file_id, permission_list in data.items():
            for permission_id in permission_list:
                requests.append(
                    self.service.permissions().delete(
                        fileId=file_id,
                        permissionId=permission_id,
                        supportsAllDrives=is_shared_drive,
                    )
                )
        # Ignore 404s when removing permissions as some permissions are somehow linked and get
        # removed in a prior removal. However, an exception will be raised so that the scan doesn't
        # succeed. The user will get rescanned on next run as the 404s may have prevented removal
        # of others in the batch.
        self.process_requests(requests, True)

    def batch_create_shared_permissions(self, data, is_shared_drive=False):
        LOG.info(f"Adding shared permissions to {len(data)} items")
        requests = []
        request_emails = []
        for file_id, permission_list in data.items():
            LOG.info(
                f"File {file_id} has {len(permission_list.get('permissions', []))} permissions to "
                "restore"
            )
            for permission in permission_list.get("permissions", []):
                # Exclude owner permissions (only concerned with shared permissions) and user
                # permissions missing an emailAddress
                if permission["role"] != "owner" and (
                    permission["type"] != "user" or permission["emailAddress"] != ""
                ):
                    # Remove the id of the permission, this will be created by google and should
                    # not be submitted in the request.
                    permission.pop("id", None)
                    requests.append(
                        self.service.permissions().create(
                            fileId=file_id,
                            sendNotificationEmail=False,
                            body=permission,
                            supportsAllDrives=is_shared_drive,
                        )
                    )
                    request_emails.append(permission.get("emailAddress", ""))

        LOG.info(f"Restoring {len(requests)} total permissions in batch request.")
        # Ignore 400s with "invalidSharingRequest" reason (requires request_emails)
        self.process_requests(requests, False, True, request_emails)

    def batch_remove_drive_permissions(self, drive_id, permission_ids):
        LOG.info(f"Removing {len(permission_ids)} permissions from shared drive {drive_id!r}")
        requests = []
        for pid in permission_ids:
            requests.append(
                self.service.permissions().delete(
                    fileId=drive_id,
                    permissionId=pid,
                    supportsAllDrives=True,
                    useDomainAdminAccess=True,
                )
            )
        # Ignore 404s when removing permissions as some permissions may have already gone
        self.process_requests(requests, True)

    def batch_create_drive_permissions(self, drive_id, data):
        LOG.info(f"Adding drive {len(data)} permissions to drive {drive_id!r}")
        requests = []
        request_emails = []
        for permission in data:
            # Exclude user permissions missing an emailAddress
            if permission["type"] != "user" or permission["emailAddress"] != "":
                # Remove the id of the permission, this will be created by google and should
                # not be submitted in the request.
                permission.pop("id", None)
                requests.append(
                    self.service.permissions().create(
                        fileId=drive_id,
                        sendNotificationEmail=False,
                        body=permission,
                        supportsAllDrives=True,
                        useDomainAdminAccess=True,
                    )
                )
                request_emails.append(permission.get("emailAddress", ""))
        # Ignore 400s with "invalidSharingRequest" reason (requires request_emails)
        self.process_requests(requests, False, True, request_emails)

    def write_file(self, filename, content, mimetype="text/plain", parents=["root"]):
        existing_metadata = self._search_for_one_file_by_name(filename, exact=True)
        data = http.MediaIoBaseUpload(
            io.BytesIO(content.encode("utf-8")), mimetype=mimetype, resumable=True
        )
        if existing_metadata:
            LOG.info(
                f"Updating existing file {filename} id {existing_metadata['id']} "
                f"({human_size(len(content))} long)"
            )
            self.process_request(
                self.service.files().update(
                    fileId=existing_metadata["id"], media_body=data, supportsAllDrives=True
                )
            )
        else:
            LOG.info(f"Writing new file {filename} ({human_size(len(content))} long)")
            file_metadata = {
                "name": filename,
                "parents": parents,
            }
            self.process_request(
                self.service.files().create(
                    body=file_metadata, media_body=data, supportsAllDrives=True
                )
            )

    def search_for_files(self, query_term, drive_id=None, exact=False):
        LOG.info(
            f"Search for file with query term {query_term} in drive: "
            f"{drive_id if drive_id else 'user'}"
        )
        if exact:
            full_query = f"name='{query_term}' and trashed=false"
        else:
            full_query = f"name contains '{query_term}' and trashed=false"
        search_args = {
            "q": full_query,
            "corpora": "drive" if drive_id else "user",
        }
        if drive_id:
            search_args["driveId"] = drive_id

        found_file_metadata = self.list_all(
            self.service.files().list,
            items_key="files",
            allow_empty=False,
            pageSize=100,
            fields="nextPageToken,files(id,modifiedTime,name)",
            supportsAllDrives=True,
            includeItemsFromAllDrives=True,
            **search_args,
        )
        return found_file_metadata

    def _search_for_one_file_by_name(self, query_term, drive_id=None, exact=False):
        found_file_metadata = self.search_for_files(query_term, drive_id, exact)
        if found_file_metadata:
            if len(found_file_metadata) > 1:
                LOG.warning(
                    f"Multiple files matching query term {query_term} in drive: "
                    f"{drive_id if drive_id else 'user'}, returning first match."
                )
                # Get newest as it will be the most up-to date file.
                found_file_metadata.sort(key=lambda file: file["modifiedTime"], reverse=True)
            return found_file_metadata[0]
        else:
            return None

    def get_file_contents_by_id(self, file_id, drive_id=None):
        response = self.process_request(
            self.service.files().get_media(
                fileId=file_id,
                acknowledgeAbuse=True,
                supportsAllDrives=True,
            )
        )
        return response

    def get_file_contents_by_name(self, query_term, drive_id=None, expected=True):
        file_metadata = self._search_for_one_file_by_name(query_term, drive_id=drive_id)
        if file_metadata:
            return file_metadata["name"], self.get_file_contents_by_id(
                file_metadata["id"], drive_id=drive_id
            )
        elif expected:
            LOG.warning(
                f"No file found matching query term {query_term} in drive: "
                f"{drive_id if drive_id else 'user'}"
            )
        return None, None

    def update_file_metadata(self, file_name, metadata, drive_id=None):
        LOG.info(f"Updating file metadata for file {file_name}")
        file_metadata = self._search_for_one_file_by_name(file_name, drive_id=drive_id, exact=True)
        if file_metadata:
            self.process_request(
                self.service.files().update(
                    fileId=file_metadata["id"],
                    body=metadata,
                    supportsAllDrives=True,
                )
            )
        else:
            LOG.warning(
                f"No file with name {file_name} in drive: " f"{drive_id if drive_id else 'user'}"
            )

    def trash_file(self, file_id, file_name):
        LOG.info(f"Trashing file {file_name}, id: {file_id} ")
        response = self.process_request(
            self.service.files().update(
                fileId=file_id, body={"trashed": True}, supportsAllDrives=True
            )
        )
        return response

    def get_storage_quota_usage(self):
        LOG.info(f"Getting storage quota usage for {self.impersonate}")
        response = self.process_request(self.service.about().get(fields="storageQuota"))
        return response.get("storageQuota", {}).get("usage", 0)

    def get_shared_drive_cache(self):
        # get cached list of shared drives
        file_metadata = self._search_for_one_file_by_name(
            self.google_config.shared_drive_list_file,
            drive_id=self.google_config.shared_storage_drive,
            exact=True,
        )
        if file_metadata:
            return self.get_file_contents_by_id(
                file_metadata["id"], drive_id=self.google_config.shared_storage_drive
            )
        LOG.warning(
            f"No shared drive list {self.google_config.shared_drive_list_file!r} "
            f"found in {self.google_config.shared_storage_drive}"
        )
        return None

    def retrieve_shared_drives(self):
        LOG.info("Getting all shared drives")
        fields = [
            "id",
            "name",
            "createdTime",
            "orgUnitId",
        ]
        shared_drives = self.list_all(
            self.service.drives().list,
            items_key="drives",
            pageSize=100,
            useDomainAdminAccess=True,
            fields="nextPageToken,drives(" + ",".join(fields) + ")",
        )
        LOG.info(f"Found {len(shared_drives)} shared drives")
        return shared_drives

    def read_shared_drive_permissions(self, shared_drive_id):
        LOG.info(f"Reading permissions for shared drive {shared_drive_id!r}")
        fields = [
            "id",
            "emailAddress",
            "role",
            "type",
            "domain",
            "allowFileDiscovery",
        ]
        return self.list_all(
            self.service.permissions().list,
            items_key="permissions",
            fileId=shared_drive_id,
            pageSize=100,
            useDomainAdminAccess=True,
            supportsAllDrives=True,
            fields="nextPageToken,permissions(" + ",".join(fields) + ")",
        )

    def get_shared_drive_usage_and_augmented_perms(self, shared_drive_id):
        # Ideally, we'd use the drives.get() method to get the usage, like below,
        # but this doesn't work (yet...?)
        #
        # response = self.process_request(
        #     self.service.drives().get(
        #         driveId=shared_drive_id,
        #         useDomainAdminAccess=True,
        #         fields="quotaBytesUsed",
        #     )
        # )
        # return response.get("quotaBytesUsed", 0)

        # Scan the files/folders in a shared drive totalling their sizes
        # of files to get the usage.
        fields = [
            "id",
            "quotaBytesUsed",
            "hasAugmentedPermissions",
        ]
        files = self.list_all(
            self.service.files().list,
            items_key="files",
            pageSize=100,
            corpora="drive",
            driveId=shared_drive_id,
            supportsAllDrives=True,
            includeItemsFromAllDrives=True,
            fields="nextPageToken,files(" + ",".join(fields) + ")",
        )
        total_quota_bytes_used = sum([int(file.get("quotaBytesUsed", 0)) for file in files])
        files_with_augmented_perms = {
            f["id"] for f in files if f.get("hasAugmentedPermissions", False)
        }

        return len(files), total_quota_bytes_used, files_with_augmented_perms

    def get_shared_drive_augmented_permissions(self, file_ids, drive_permission_ids):
        augmented_permissions = []
        fields = [
            "id",
            "emailAddress",
            "role",
            "type",
            "domain",
            "allowFileDiscovery",
            "permissionDetails",
        ]
        for idx, file_id in enumerate(file_ids):
            LOG.info(f"Getting permissions for file {file_id} - {idx+1}/{len(file_ids)}")
            file_permissions = self.list_all(
                self.service.permissions().list,
                items_key="permissions",
                fileId=file_id,
                pageSize=100,
                supportsAllDrives=True,
                useDomainAdminAccess=False,
                fields="nextPageToken,permissions(" + ",".join(fields) + ")",
            )
            non_inherited_permissions = [
                p
                for p in file_permissions
                if (p["id"] not in drive_permission_ids)
                # Oddly permissionDetails is a list with only one element
                and not (
                    next(iter(p.get("permissionDetails", [])), {"inherited", False}).get(
                        "inherited", False
                    )
                )
            ]
            LOG.info(f"... {len(non_inherited_permissions)} non-inherited permissions found")
            if non_inherited_permissions:
                augmented_permissions.append(
                    {
                        "file_id": file_id,
                        "permissions": [
                            {k: v for k, v in p.items() if k != "permissionDetails"}
                            for p in non_inherited_permissions
                        ],
                    }
                )
        return sorted(augmented_permissions, key=lambda x: x["file_id"])

    def check_drive_access(self, shared_drive_id):
        # Perform a simple request to the shared drive to check if this connection has access
        # or even if the drive still exists
        self.process_request(
            self.service.drives().get(
                driveId=shared_drive_id,
                useDomainAdminAccess=False,  # explictly set to false to not elevate permissions
                fields="id",
            ),
            retries=False,
        )
        LOG.info(f"Connection as {self.impersonate} has access to shared drive {shared_drive_id}")

    def add_manager_to_shared_drive(self, shared_drive_id, manager_email):
        LOG.info(f"Adding {manager_email} as manager to shared drive {shared_drive_id}")
        self.process_request(
            self.service.permissions().create(
                fileId=shared_drive_id,
                sendNotificationEmail=False,
                body={
                    "role": "organizer",
                    "type": "user",
                    "emailAddress": manager_email,
                },
                supportsAllDrives=True,
                useDomainAdminAccess=True,
            )
        )


def _retry_http_errors(func, retries, retry_delay):
    while True:
        try:
            return func()
        except (HttpError, SSLError, TimeoutError) as err:
            if retries > 0:
                retry = False
                # Retry all SSL Errors
                if isinstance(err, SSLError):
                    LOG.warning(f"SSL error ({err.reason}) - retrying in {retry_delay}s")
                    retry = True
                # Retry all Timeout Errors
                elif isinstance(err, TimeoutError):
                    LOG.warning(f"Timeout error - retrying in {retry_delay}s")
                    retry = True
                # Only retry HttpErrors with status >= 400
                elif isinstance(err, HttpError) and err.resp.status >= 400:
                    LOG.warning(f"HTTP error ({err.resp.status}) - retrying in {retry_delay}s")
                    retry = True

                if retry:
                    retries -= 1
                    sleep(retry_delay)
                    retry_delay *= 2
                    continue

            elif hasattr(err, "content"):
                LOG.error(f"Retry count exceeded - Error content: {err.content!r}")
            else:
                LOG.error(f"Retry count exceeded - Error: {err}")

            # Unhandled error or retry count exceeded, so re-raise exception
            raise


def _get_accounts_from_message(message):
    """
    Extract one or more email addresses from 400 InvalidSharingRequest error message.

    Looks for "to invite {...}. As there", extracting the email address(es) from {...}

    >>> _get_accounts_from_message("blah to invite foo@bar.com. As there is blah")
    {'foo@bar.com'}
    >>> sorted(_get_accounts_from_message(
    ...    "blah to invite foo@bar.com, baz@gmail.com. As there are blah"))
    ['baz@gmail.com', 'foo@bar.com']
    >>> _get_accounts_from_message("blah blah foo@bar.com. Blah")
    set()

    """
    match = re.search(r"to invite ([^,]+)(?:, ([^,]+))*\. As there", message)
    if match is not None:
        return {email for email in match.groups() if email}
    return set()
