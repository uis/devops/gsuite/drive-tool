import dataclasses
import logging
import os
import typing

import yaml
from munch import Munch

LOG = logging.getLogger(__name__)


class ConfigurationNotFound(RuntimeError):
    """
    A suitable configuration could not be located.

    """

    def __init__(self):
        return super().__init__("Could not find a configuration file.")


class ConfigurationDataclassMixin:
    @classmethod
    def from_dict(cls, dict_):
        """
        Construct an instance from a dict.

        """
        field_names = {field.name for field in dataclasses.fields(cls)}
        required_field_names = {
            field.name
            for field in dataclasses.fields(cls)
            if (
                field.default is dataclasses.MISSING
                and field.default_factory is dataclasses.MISSING
            )
        }

        for key in dict_.keys():
            if key not in field_names:
                raise ValueError(f"Unknown configuration key: {key}")

        for key in required_field_names:
            if key not in dict_:
                raise ValueError(f"{key}: required field not set")

        return cls(**dict_)


@dataclasses.dataclass
class GoogleConfiguration(ConfigurationDataclassMixin):
    """
    Configuration for accessing the Google APIs.

    """

    # Name of the domain e.g. "example.com".
    domain_name: str

    # Id of the shared storage drive to store original permissions information
    shared_storage_drive: str

    # Path to on-disk YAML credentials used when accessing the API.
    credentials: str

    # Path to on-disk YAML credentials used when accessing the API in "read-only" mode. Use this if
    # you want to have a separate "safe" service account which can only read data. If null, use the
    # same credentials for reading and writing.
    read_only_credentials: typing.Union[str, None] = None

    # Certain API actions (specifically those involving shared drives) require the connection to
    # impersonate a domain admin account.
    admin_user: typing.Union[str, None] = None

    # Account to add as a manager of a shared drive if no manager can be found. We need at least
    # one manager to scan the files and their permissions in a shared drive.
    # Default: null (these actions will fail)
    management_user: str = None

    # The list of shared drives with permissions gets cached to the shared storage drive with this
    # name. Defaults to 'shared_drive_list.yaml'.
    shared_drive_list_file: str = "shared_drive_list.yaml"

    # How many days to keep the cached shared drive list for. Defaults to 7 days.
    shared_drive_list_cache_days: int = 7

    # How many days to keep the usage value for each drive in the cached shared drive list for.
    # Defaults to 7 days.
    shared_drive_usage_cache_days: int = 7

    # Rather than writing the cached shared drive list after getting the usage for each drive, only
    # update it if it has been at least this many minutes. Defaults to 5 minutes.
    shared_drive_usage_cache_update_frequency: int = 5


@dataclasses.dataclass
class ConnectionConfiguration(ConfigurationDataclassMixin):
    """
    Connection configuration.

    """

    timeout: int = 300

    # Inter-batch delay in seconds. This is useful to avoid hitting Google rate limits.
    inter_batch_delay: int = 5

    # Batch size for Google API calls. Google supports batching requests together into one API
    # call.
    batch_size: int = 50

    # Number of times to retry HTTP requests if a HTTP failure response is received
    http_retries: int = 5

    # Delay in seconds between retying a request that has failed
    http_retry_delay: int = 5


@dataclasses.dataclass
class LimitsConfiguration(ConfigurationDataclassMixin):
    """
    Configured limits for operations.

    """

    # Maximum number of users to scan and remove shared permissions for in the 'scan' operation
    max_scanned_users: int = 200

    # Processing of the next user in a 'scan' operation must start within this duration
    # in minutes
    max_total_scan_duration: int = 180

    # Scanning of the user's files/folders will abort after this duration in minutes, and the
    # user will be marked as needed manual attention
    max_user_scan_duration: int = 120

    # Maximum number of users to recover removed shared permissions for in the 'recover' operation
    max_recovered_users: int = 200

    # Limit the duration of the 'shared-drive-usage' operation in minutes
    max_shared_drive_usage_duration: int = 120

    # Limit number of permission document files to clean in the shared drive
    max_cleanup_files: int = 300

    # Limit how recently a shared drive has to have been scanned before permissions can be removed
    min_shared_drive_update_days: int = 3


@dataclasses.dataclass
class LookupConfiguration(ConfigurationDataclassMixin):
    """
    Configured for accessing Lookup API.

    """

    # Path to on-disk YAML credentials used when accessing the API.
    credentials: str = "./api_gateway_credentials.yaml"

    # Base URL for the Lookup API
    base_url: str = "https://api.apps.cam.ac.uk/lookup/v1"


@dataclasses.dataclass
class ReportConfiguration(ConfigurationDataclassMixin):
    """
    Configured specifically for "report" operation.

    """

    # Format of filename for user MyDrive report output
    mydrive_filename: str = "{id}-mydrive-{timestamp}.csv"

    # Format of filename for user Shared Drive report output
    shared_drive_filename: str = "{id}-shared-drive-{timestamp}.csv"

    # Format of filename for all Shared Drive usage report output
    all_shared_drive_filename: str = "shared-drive-usage-{timestamp}.csv"

    # Where to save the report output (Google Drive folder ID), or None for local filesystem
    output_location: str = None

    # Limit of when to switch from individual check to full Google account list check
    google_account_individual_check_limit: int = 50

    # Local mydrive user quota usage cache CSV file (local dev only to avoid repeated API calls
    # when producing multiple reports)
    mydrive_user_usage_cache: str = "mydrive-user-cache.csv"

    # Group domains to treat like active domain members in shared drive Reports
    # Defaults to just 'groups.lookup.cam.ac.uk'
    group_domains: list[str] = dataclasses.field(
        default_factory=lambda: ["groups.lookup.cam.ac.uk"]
    )


class ConfigurationConsumer:
    required_config = None

    def __init__(self, configuration):
        for c in self.required_config if self.required_config is not None else []:
            setattr(self, f"{c}_config", configuration.get(c, {}))


def load_configuration(location=None):
    """
    Load configuration and return a :py:class:`Configuration` instance. Pass a non-None location to
    override the default search path.

    :raises: ConfigurationError if the configuration cannot be loaded.

    """
    if location is not None:
        paths = [location]
    else:
        if "GDRIVEMANAGE_CONFIGURATION" in os.environ:
            paths = [os.environ["GDRIVEMANAGE_CONFIGURATION"]]
        else:
            paths = []
        paths.extend(
            [
                os.path.join(os.getcwd(), "configuration.yaml"),
                os.path.expanduser("~/.gdrivemanager/configuration.yaml"),
                "/etc/gdrivemanager/configuration.yaml",
            ]
        )

    valid_paths = [path for path in paths if os.path.isfile(path)]

    if len(valid_paths) == 0:
        LOG.error(f"Could not find a configuration file. Tried: {', '.join(paths)}")
        raise ConfigurationNotFound()

    with open(valid_paths[0]) as f:
        return yaml.safe_load(f)


def parse_configuration(configuration):
    """
    Parses the multiple parts of configuration using appropriate Configuration classes.
    Returns a dict containing parsed parts of configuration.

    """
    return Munch.fromDict(
        {
            "google": GoogleConfiguration.from_dict(configuration.get("google", {})),
            "connection": ConnectionConfiguration.from_dict(configuration.get("connection", {})),
            "limits": LimitsConfiguration.from_dict(configuration.get("limits", {})),
            "lookup": LookupConfiguration.from_dict(configuration.get("lookup", {})),
            "report": ReportConfiguration.from_dict(configuration.get("report", {})),
        }
    )
