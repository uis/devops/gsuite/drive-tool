import logging
import yaml

from identitylib.lookup_client import ApiClient as LookupApiClient
from identitylib.lookup_client.api.institution_api import InstitutionApi
from identitylib.lookup_client.api.person_api import PersonApi
from identitylib.lookup_client.api.group_api import GroupApi
from identitylib.lookup_client_configuration import LookupClientConfiguration

from .parse_config import ConfigurationConsumer
from .utils import grouper

LOG = logging.getLogger(__name__)


class LookupConnection(ConfigurationConsumer):
    required_config = ("lookup",)

    def __init__(self, configuration):
        super().__init__(configuration)

        creds_file = self.lookup_config.credentials
        LOG.info('Loading API Gateway credentials from "%s"', creds_file)
        with open(creds_file, "r") as stream:
            settings = yaml.safe_load(stream)

        config = LookupClientConfiguration(
            settings["client_id"], settings["client_secret"], base_url=self.lookup_config.base_url
        )
        self.lookup_api_client = LookupApiClient(config, pool_threads=10)
        self.person_api_client = PersonApi(self.lookup_api_client)
        self.inst_api_client = InstitutionApi(self.lookup_api_client)
        self.group_api_client = GroupApi(self.lookup_api_client)

    def retrieve_users_by_instid(self, instid, include_children=False):
        """
        Retrieve users from Lookup API by Institution ID, optionally recursively including
        child institution members. Removes duplicates by returning dict keyed on id.

        """
        instid = instid.upper()
        inst_members_by_id = {
            member.identifier.value: self._user_from_person(member)
            for member in (
                self._people_in_response(
                    self.inst_api_client.institution_get_members(instid, fetch="all_insts")
                )
                + self._people_in_response(
                    self.inst_api_client.institution_get_cancelled_members(
                        instid, fetch="jdInstid"
                    )
                )
            )
        }
        if include_children:
            response = self.inst_api_client.institution_get_inst(instid, fetch="child_insts")
            institution = response.get("result", {}).get("institution")
            if institution is None:
                raise RuntimeError(f"Institution with ID {instid} missing from response")
            for child in institution.child_insts:
                LOG.info(f"Retrieving members of child institution {child.instid}")
                inst_members_by_id.update(
                    self.retrieve_users_by_instid(child.instid, include_children=True)
                )
        return inst_members_by_id

    def retrieve_user_by_crsid(self, crsid):
        person = (
            self.person_api_client.person_get_person(
                "crsid", crsid.lower(), fetch="all_insts,jdInstid"
            )
            .get("result", {})
            .get("person")
        )
        if person is None:
            raise ValueError(f"User with CRSid {crsid.lower()} not found")
        return self._user_from_person(person)

    def retrieve_users_by_group(self, groupid):
        """
        Retrieve users from Lookup API by Group ID (or short name).

        """

        return sorted(
            [
                self._user_from_person(member)
                for member in (
                    self._people_in_response(
                        self.group_api_client.group_get_members(groupid.lower(), fetch="all_insts")
                    )
                    + self._people_in_response(
                        self.group_api_client.group_get_cancelled_members(
                            groupid.lower(), fetch="all_insts,jdInstid"
                        )
                    )
                )
            ],
            key=lambda x: x["id"],
        )

    def retrieve_users_from_list(self, ids):
        """
        Retrieve users from Lookup API from a list of identifiers (in batches if needed)

        """

        return sorted(
            [
                self._user_from_person(person)
                for batch_of_ids in grouper(ids, n=100)
                for person in self._people_in_response(
                    self.person_api_client.person_list_people(
                        ",".join(batch_of_ids), fetch="all_insts,jdInstid"
                    )
                )
            ],
            key=lambda x: x["id"],
        )

    def _people_in_response(self, response):
        return response.get("result", {}).get("people", [])

    def _user_from_person(self, person):
        insts = set()
        if hasattr(person, "institutions") and person.institutions:
            insts = {inst.instid for inst in person.institutions}
        if hasattr(person, "attributes"):
            instid = next(
                (attr.value for attr in person.attributes if attr.scheme == "jdInstid"), None
            )
            if instid is not None:
                insts.add(instid)
        return {
            "id": person.identifier.value,
            "display_name": getattr(person, "display_name", ""),
            "cancelled": person.cancelled,
            "insts": sorted(list(insts)),
        }
