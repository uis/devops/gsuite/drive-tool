from collections import namedtuple
from identitylib.lookup_client.exceptions import NotFoundException

# A holder class for the lookup person
LookupPerson = namedtuple(
    "LookupPerson", ["display_name", "identifier", "cancelled", "institutions", "attributes"]
)

# A holder class for the lookup person without a display_name
LookupNamelessPerson = namedtuple(
    "LookupNamelessPerson", ["identifier", "cancelled", "institutions", "attributes"]
)

# A holder class for the lookup person without any institutions (such as cancelled people)
LookupInstlessPerson = namedtuple(
    "LookupInstlessPerson", ["display_name", "identifier", "cancelled", "attributes"]
)

# A holder class for the lookup person identifier
LookupPersonIdentifier = namedtuple("LookupPersonIdentifier", ["scheme", "value"])

# A holder class for the lookup person
LookupPersonAttribute = namedtuple("LookupPersonAttribute", ["value", "scheme"])

# A holder class for the lookup institution
LookupInstitution = namedtuple("LookupInstitution", ["instid", "members", "child_insts"])

# A holder class for the lookup group
LookupGroup = namedtuple("LookupGroup", ["groupid", "name", "members"])


class MockConfiguration:
    """Mock the Identitylib API configurations"""

    def __init__(self, *args, **kwargs):
        self.host = "mock_host"
        self.access_token = "mock_access_token"

    def __iter__(self):
        return self

    def __next__(self):
        return self


class MockApiClient:
    """Mock the Identitylib API clients"""

    def __init__(self, *args, **kwargs):
        pass

    def __iter__(self):
        return self


class MockLookupApiBase:
    def __init__(self, *args, **kwargs):
        self.api_client = MockApiClient()
        self.entities = []

    def _match(self, entity, id):
        return entity.id.upper() == id.upper()

    def _get_members(self, id, cancelled):
        entities = [entity for entity in self.entities if self._match(entity, id)]
        if len(entities) == 0:
            raise NotFoundException()

        return {
            "result": {
                "people": [
                    person for person in entities[0].members if person.cancelled == cancelled
                ]
            }
        }


class MockLookupInstitutionApi(MockLookupApiBase):
    """Mock the Identitylib Lookup Institution API"""

    def _match(self, entity, id):
        return entity.instid.upper() == id.upper()

    def institution_get_members(self, instid, *args, **kwargs):
        return self._get_members(instid, False)

    def institution_get_cancelled_members(self, instid, *args, **kwargs):
        return self._get_members(instid, True)

    def institution_get_inst(self, instid, *args, **kwargs):
        entities = [entity for entity in self.entities if self._match(entity, instid)]
        if len(entities) == 0:
            raise NotFoundException()
        return {
            "result": {
                "institution": entities[0],
            }
        }


class MockLookupGroupApi(MockLookupApiBase):
    """Mock the Identitylib Lookup Group API"""

    def _match(self, entity, id):
        return entity.groupid == id or entity.name == id.lower()

    def group_get_members(self, groupid, *args, **kwargs):
        return self._get_members(groupid, False)

    def group_get_cancelled_members(self, groupid, *args, **kwargs):
        return self._get_members(groupid, True)


class MockLookupPersonApi(MockLookupApiBase):
    """Mock the Identitylib Lookup Person API"""

    def person_get_person(self, scheme, value, *args, **kwargs):
        entities = [
            entity
            for entity in self.entities
            if entity.identifier.scheme == scheme and entity.identifier.value == value
        ]
        if len(entities) == 0:
            return {"result": {}}
        return {"result": {"person": entities[0]}}
