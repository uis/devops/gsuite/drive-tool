import json
from tempfile import NamedTemporaryFile
from unittest import TestCase
from random import shuffle

from . import datafile, index_by_id

from gdrivemanager.utils import (
    FOLDER_MIMETYPE,
    generate_csv,
    human_size,
    read_csv,
    sort_files_for_permission_removal,
    write_csv,
)


class FileSorting(TestCase):
    def assertFileBefore(self, files, fileid1, fileid2):
        index1 = index_by_id(files, fileid1)
        self.assertIsNotNone(index1)
        index2 = index_by_id(files, fileid2)
        self.assertIsNotNone(index2)
        self.assertLess(index1, index2)

    def assertFoldersBeforeFiles(self, files):
        folder_indexes = [index for (index, file) in enumerate(files) if file["isFolder"]]
        self.assertNotEqual(len(folder_indexes), 0)
        last_folder_index = folder_indexes[-1]

        first_file_index = next(
            (index for (index, file) in enumerate(files) if not file["isFolder"]), None
        )
        self.assertIsNotNone(first_file_index)

        self.assertLess(last_folder_index, first_file_index)

    def test_hierarchical_sort(self):
        # Hierarchical response structure:
        #
        # - orphanfolder1
        #    - subfolder1
        #       - grandchild1
        #    - child1
        # - insharefolder1
        #    - subfolder2
        #       - grandchild2
        #    - child2
        # - insharefile1

        files = json.loads(datafile("drive-hierarchical-response.json"))["files"]

        # Try multiple shuffled versions of this list
        for i in range(10):
            shuffle(files)
            sorted_files = sort_files_for_permission_removal(files)

            # assert that the parents are before their children
            self.assertFileBefore(sorted_files, "orphanfolder1", "subfolder1")
            self.assertFileBefore(sorted_files, "orphanfolder1", "child1")
            self.assertFileBefore(sorted_files, "subfolder1", "grandchild1")
            self.assertFileBefore(sorted_files, "insharefolder1", "subfolder2")
            self.assertFileBefore(sorted_files, "insharefolder1", "child2")
            self.assertFileBefore(sorted_files, "subfolder2", "grandchild2")

            # assert all folders are before files
            self.assertFoldersBeforeFiles(sorted_files)

    def test_multiparent_sort(self):
        files = [
            {"id": "root", "mimeType": FOLDER_MIMETYPE},
            {"id": "folder1", "mimeType": FOLDER_MIMETYPE, "parents": ["root"]},
            {"id": "folder2", "mimeType": FOLDER_MIMETYPE, "parents": ["folder1", "root"]},
            {"id": "file1", "mimeType": "text/plain", "parents": ["folder1", "root"]},
            {"id": "file2", "mimeType": "text/plain", "parents": ["folder2", "root"]},
        ]

        # Try multiple shuffled versions of this list
        for i in range(10):
            shuffle(files)
            sorted_files = sort_files_for_permission_removal(files)

            # assert that the parents are before their children
            self.assertFileBefore(sorted_files, "root", "folder1")
            self.assertFileBefore(sorted_files, "root", "folder2")
            self.assertFileBefore(sorted_files, "folder1", "folder2")

            # assert all folders are before files
            self.assertFoldersBeforeFiles(sorted_files)


class TestUtils(TestCase):
    def test_read_csv(self):
        tempfile = NamedTemporaryFile(mode="r+", delete=True)
        tempfile.write("field1,field2\nvalue1,value2\nvalue3,value4\n")
        tempfile.seek(0)
        self.assertEqual(
            read_csv(tempfile.name),
            [
                {"field1": "value1", "field2": "value2"},
                {"field1": "value3", "field2": "value4"},
            ],
        )

    def test_write_csv(self):
        tempfile = NamedTemporaryFile(mode="r+", delete=True)
        data = [
            {"field1": "value1", "field2": "value2"},
            {"field1": "value3", "field2": "value4"},
        ]
        write_csv(data, tempfile.name)
        tempfile.seek(0)
        # We don't actually care if we end up with \r\n or \n, so just replace it for the
        # comparison
        self.assertEqual(
            tempfile.read().replace("\r\n", "\n"), "field1,field2\nvalue1,value2\nvalue3,value4\n"
        )

    def test_generate_csv(self):
        data = [
            {"field1": "value1", "field2": "value2"},
            {"field1": "value3", "field2": "value4"},
        ]
        # We don't actually care if we end up with \r\n or \n, so just replace it for the
        # comparison
        self.assertEqual(
            generate_csv(data).replace("\r\n", "\n"),
            "field1,field2\nvalue1,value2\nvalue3,value4\n",
        )

    def test_human_size(self):
        self.assertEqual(human_size(0), "0 B")
        self.assertEqual(human_size(1000), "1000 B")
        self.assertEqual(human_size(1024), "1.00 KiB")
        self.assertEqual(human_size(1024 * 1024), "1.00 MiB")
        self.assertEqual(human_size(1024 * 1024 * 1024), "1.00 GiB")
        self.assertEqual(human_size(1024 * 1024 * 1024 * 1024), "1.00 TiB")
        self.assertEqual(human_size(1024 * 1024 * 1024 * 1024 * 1024), "1.00 PiB")
        self.assertEqual(human_size(1024 * 1024 * 1024 * 1024 * 1024 * 1024), "1024.00 PiB")
