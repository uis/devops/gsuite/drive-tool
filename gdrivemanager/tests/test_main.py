from unittest import TestCase, mock

from . import CONFIGURATION

from gdrivemanager.manage import ReportType
from gdrivemanager import main


class MainTestCases(TestCase):
    def setUp(self):
        # Patch the configuration loading functions so CONFIGURATION is returned.
        patch_config = [
            mock.patch("gdrivemanager.load_configuration", return_value={}),
            mock.patch("gdrivemanager.parse_configuration", return_value=CONFIGURATION),
        ]
        for p in patch_config:
            p.start()
            self.addCleanup(p.stop)

    @mock.patch("gdrivemanager.manage.scan")
    def test_main_scan(self, mock_scan):
        with mock.patch("gdrivemanager.sys.argv", ["gdrivemanager", "scan"]):
            main()

        with mock.patch("gdrivemanager.sys.argv", ["gdrivemanager", "scan", "--write"]):
            main()

        mock_scan.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=False),
                mock.call(CONFIGURATION, write_mode=True),
            ]
        )

    @mock.patch("gdrivemanager.manage.recover")
    def test_main_recover(self, mock_recover):
        with mock.patch("gdrivemanager.sys.argv", ["gdrivemanager", "recover"]):
            main()

        with mock.patch("gdrivemanager.sys.argv", ["gdrivemanager", "recover", "--write"]):
            main()

        with mock.patch("gdrivemanager.sys.argv", ["gdrivemanager", "recover", "--rescan"]):
            main()

        with mock.patch("gdrivemanager.sys.argv", ["gdrivemanager", "recover", "--user=ABC123"]):
            main()

        mock_recover.assert_has_calls(
            [
                mock.call(CONFIGURATION, user_id=None, rescan=False, write_mode=False),
                mock.call(CONFIGURATION, user_id=None, rescan=False, write_mode=True),
                mock.call(CONFIGURATION, user_id=None, rescan=True, write_mode=False),
                mock.call(CONFIGURATION, user_id="ABC123", rescan=False, write_mode=False),
            ]
        )

    @mock.patch("gdrivemanager.manage.scan_shared_drives")
    def test_main_shared_drive_usage(self, mock_scan_shared_drives):
        with mock.patch("gdrivemanager.sys.argv", ["gdrivemanager", "shared-drive-usage"]):
            main()

        with mock.patch(
            "gdrivemanager.sys.argv", ["gdrivemanager", "shared-drive-usage", "--write"]
        ):
            main()

        with mock.patch(
            "gdrivemanager.sys.argv", ["gdrivemanager", "shared-drive-usage", "--skip-rebuild"]
        ):
            main()

        with mock.patch(
            "gdrivemanager.sys.argv", ["gdrivemanager", "shared-drive-usage", "--driveid", "foo"]
        ):
            main()

        with mock.patch(
            "gdrivemanager.sys.argv",
            [
                "gdrivemanager",
                "shared-drive-usage",
                "--skip-rebuild",
                "--driveid",
                "foo",
                "--write",
            ],
        ):
            main()

        mock_scan_shared_drives.assert_has_calls(
            [
                mock.call(CONFIGURATION, skip_rebuild=False, drive_id=None, write_mode=False),
                mock.call(CONFIGURATION, skip_rebuild=False, drive_id=None, write_mode=True),
                mock.call(CONFIGURATION, skip_rebuild=True, drive_id=None, write_mode=False),
                mock.call(CONFIGURATION, skip_rebuild=False, drive_id="foo", write_mode=False),
                mock.call(CONFIGURATION, skip_rebuild=True, drive_id="foo", write_mode=True),
            ]
        )

    @mock.patch("gdrivemanager.manage.report")
    def test_main_report_user(self, mock_report):
        with mock.patch("gdrivemanager.sys.argv", ["gdrivemanager", "report", "--user", "abc123"]):
            main()

        with mock.patch(
            "gdrivemanager.sys.argv", ["gdrivemanager", "report", "--user", "xyz789", "--write"]
        ):
            main()

        with mock.patch(
            "gdrivemanager.sys.argv",
            ["gdrivemanager", "report", "--user", "uvw456", "--cache-users"],
        ):
            main()

        mock_report.assert_has_calls(
            [
                mock.call(
                    CONFIGURATION,
                    ReportType.USER,
                    entity_id="abc123",
                    cache_users=False,
                    write_mode=False,
                ),
                mock.call(
                    CONFIGURATION,
                    ReportType.USER,
                    entity_id="xyz789",
                    cache_users=False,
                    write_mode=True,
                ),
                mock.call(
                    CONFIGURATION,
                    ReportType.USER,
                    entity_id="uvw456",
                    cache_users=True,
                    write_mode=False,
                ),
            ]
        )

    @mock.patch("gdrivemanager.manage.report")
    def test_main_report_inst(self, mock_report):
        with mock.patch("gdrivemanager.sys.argv", ["gdrivemanager", "report", "--instid", "foo"]):
            main()

        with mock.patch(
            "gdrivemanager.sys.argv",
            ["gdrivemanager", "report", "--instid", "bar", "--children", "--write"],
        ):
            main()

        with mock.patch(
            "gdrivemanager.sys.argv",
            ["gdrivemanager", "report", "--instid", "baz", "--cache-users"],
        ):
            main()

        mock_report.assert_has_calls(
            [
                mock.call(
                    CONFIGURATION,
                    ReportType.INSTITUTION,
                    entity_id="foo",
                    include_children=False,
                    cache_users=False,
                    write_mode=False,
                ),
                mock.call(
                    CONFIGURATION,
                    ReportType.INSTITUTION,
                    entity_id="bar",
                    include_children=True,
                    cache_users=False,
                    write_mode=True,
                ),
                mock.call(
                    CONFIGURATION,
                    ReportType.INSTITUTION,
                    entity_id="baz",
                    include_children=False,
                    cache_users=True,
                    write_mode=False,
                ),
            ]
        )

    @mock.patch("gdrivemanager.manage.report")
    def test_main_report_group(self, mock_report):
        with mock.patch(
            "gdrivemanager.sys.argv", ["gdrivemanager", "report", "--groupid", "foo-group1"]
        ):
            main()

        with mock.patch(
            "gdrivemanager.sys.argv", ["gdrivemanager", "report", "--groupid", "001234", "--write"]
        ):
            main()

        with mock.patch(
            "gdrivemanager.sys.argv",
            ["gdrivemanager", "report", "--groupid", "bar-group2", "--cache-users"],
        ):
            main()

        mock_report.assert_has_calls(
            [
                mock.call(
                    CONFIGURATION,
                    ReportType.GROUP,
                    entity_id="foo-group1",
                    cache_users=False,
                    write_mode=False,
                ),
                mock.call(
                    CONFIGURATION,
                    ReportType.GROUP,
                    entity_id="001234",
                    cache_users=False,
                    write_mode=True,
                ),
                mock.call(
                    CONFIGURATION,
                    ReportType.GROUP,
                    entity_id="bar-group2",
                    cache_users=True,
                    write_mode=False,
                ),
            ]
        )

    @mock.patch("gdrivemanager.manage.report")
    def test_main_report_request(self, mock_report):
        with mock.patch("gdrivemanager.sys.argv", ["gdrivemanager", "report", "--request"]):
            main()

        with mock.patch(
            "gdrivemanager.sys.argv",
            ["gdrivemanager", "report", "--request", "--write", "--cache-users"],
        ):
            main()

        mock_report.assert_has_calls(
            [
                mock.call(
                    CONFIGURATION,
                    ReportType.REQUEST,
                    cache_users=False,
                    write_mode=False,
                ),
                mock.call(
                    CONFIGURATION,
                    ReportType.REQUEST,
                    cache_users=True,
                    write_mode=True,
                ),
            ]
        )

    @mock.patch("gdrivemanager.manage.shared_drive_report")
    def test_main_shared_drive_report(self, mock_shared_drive_report):
        with mock.patch("gdrivemanager.sys.argv", ["gdrivemanager", "shared-drive-report"]):
            main()

        with mock.patch(
            "gdrivemanager.sys.argv", ["gdrivemanager", "shared-drive-report", "--write"]
        ):
            main()

        mock_shared_drive_report.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=False),
                mock.call(CONFIGURATION, write_mode=True),
            ]
        )

    @mock.patch("gdrivemanager.manage.reset_manual_actions")
    def test_main_reset_manual(self, mock_reset_manual_actions):
        with mock.patch("gdrivemanager.sys.argv", ["gdrivemanager", "reset-manual"]):
            main()

        with mock.patch("gdrivemanager.sys.argv", ["gdrivemanager", "reset-manual", "--write"]):
            main()

        mock_reset_manual_actions.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=False),
                mock.call(CONFIGURATION, write_mode=True),
            ]
        )

    @mock.patch("gdrivemanager.manage.clean_recovery_drive")
    def test_main_clean_recovery_drive(self, mock_clean_recovery_drive):
        with mock.patch("gdrivemanager.sys.argv", ["gdrivemanager", "clean-recovery-drive"]):
            main()

        with mock.patch(
            "gdrivemanager.sys.argv", ["gdrivemanager", "clean-recovery-drive", "--write"]
        ):
            main()

        mock_clean_recovery_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=False),
                mock.call(CONFIGURATION, write_mode=True),
            ]
        )

    @mock.patch("gdrivemanager.manage.remove_shared_drive_permissions")
    def test_main_shared_drive_permissions_remove(self, mock_remove_shared_drive_permissions):
        with mock.patch(
            "gdrivemanager.sys.argv",
            ["gdrivemanager", "shared-drive-permissions", "remove", "--driveid", "foo"],
        ):
            main()

        with mock.patch(
            "gdrivemanager.sys.argv",
            ["gdrivemanager", "shared-drive-permissions", "remove", "--driveid", "bar", "--write"],
        ):
            main()

        mock_remove_shared_drive_permissions.assert_has_calls(
            [
                mock.call(CONFIGURATION, drive_id="foo", write_mode=False),
                mock.call(CONFIGURATION, drive_id="bar", write_mode=True),
            ]
        )

    @mock.patch("gdrivemanager.manage.restore_shared_drive_permissions")
    def test_main_shared_drive_permissions_restore(self, mock_restore_shared_drive_permissions):
        with mock.patch(
            "gdrivemanager.sys.argv",
            ["gdrivemanager", "shared-drive-permissions", "restore", "--driveid", "foo"],
        ):
            main()

        with mock.patch(
            "gdrivemanager.sys.argv",
            [
                "gdrivemanager",
                "shared-drive-permissions",
                "restore",
                "--driveid",
                "bar",
                "--write",
            ],
        ):
            main()

        mock_restore_shared_drive_permissions.assert_has_calls(
            [
                mock.call(CONFIGURATION, drive_id="foo", write_mode=False),
                mock.call(CONFIGURATION, drive_id="bar", write_mode=True),
            ]
        )
