from copy import deepcopy
from unittest import TestCase, mock
from freezegun import freeze_time

from googleapiclient.errors import HttpError
import yaml

from . import (
    check_json_bodies_matched,
    check_urls_contained,
    datafile,
    inject_discovery_mock,
    MockBatchHttpRequestFactory,
    CONFIGURATION,
    STATUS_200,
    STATUS_200_LOCATION,
    STATUS_204,
    STATUS_302,
    STATUS_400,
    STATUS_403,
    STATUS_404,
)
from gdrivemanager.gapi import (
    GoogleDirectoryConnection,
    GoogleDriveConnection,
    ListTimeoutError,
)


class GoogleDirectoryConnectionTests(TestCase):
    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    def test_default_construction(self, mock_cred_build):
        _ = GoogleDirectoryConnection(CONFIGURATION)
        mock_cred_build.assert_called_once_with(CONFIGURATION.google.read_only_credentials)
        mock_cred_build.return_value.with_scopes.assert_called_once_with(
            GoogleDirectoryConnection.read_only_scopes
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    def test_non_ro_construction(self, mock_cred_build):
        _ = GoogleDirectoryConnection(CONFIGURATION, write_mode=True)
        mock_cred_build.assert_called_once_with(CONFIGURATION.google.credentials)
        mock_cred_build.return_value.with_scopes.assert_called_once_with(
            GoogleDirectoryConnection.read_only_scopes + GoogleDirectoryConnection.write_scopes
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    def test_ro_credentials_missing_construction(self, mock_cred_build):
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.google.read_only_credentials = None
        _ = GoogleDirectoryConnection(temp_configuration, write_mode=False)
        mock_cred_build.assert_called_once_with(CONFIGURATION.google.credentials)
        mock_cred_build.return_value.with_scopes.assert_called_once_with(
            GoogleDirectoryConnection.read_only_scopes
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("directory-ordinary-response.json")),
            ]
        ),
    )
    def test_retrieve_users(self, mock_cred_build):
        gapi = GoogleDirectoryConnection(CONFIGURATION)
        users = gapi.retrieve_users()
        self.assertEqual(len(users), 7)
        self.assertEqual(len(gapi.service._http.request_sequence), 1)
        check_urls_contained([r"admin/directory/v1/users\?"], gapi.service._http.request_sequence)

    @mock.patch("gdrivemanager.gapi.sleep")
    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_400, datafile("directory-ordinary-response.json")),
                (STATUS_200, datafile("directory-ordinary-response.json")),
            ]
        ),
    )
    def test_retry_retrieve_users(self, mock_cred_build, mock_sleep):
        gapi = GoogleDirectoryConnection(CONFIGURATION)
        users = gapi.retrieve_users()
        self.assertEqual(len(users), 7)
        mock_sleep.assert_called_once_with(CONFIGURATION.connection.http_retry_delay)
        self.assertEqual(len(gapi.service._http.request_sequence), 2)
        check_urls_contained(
            ["admin/directory/v1/users?"] * 2, gapi.service._http.request_sequence
        )

    @mock.patch("gdrivemanager.gapi.sleep")
    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_400, datafile("directory-ordinary-response.json")),
                (STATUS_400, datafile("directory-ordinary-response.json")),
                (STATUS_200, datafile("directory-ordinary-response.json")),
            ]
        ),
    )
    def test_retries_exceeded_retrieve_users(self, mock_cred_build, mock_sleep):
        gapi = GoogleDirectoryConnection(CONFIGURATION)
        with self.assertRaises(HttpError):
            _ = gapi.retrieve_users()
        mock_sleep.assert_called_once_with(CONFIGURATION.connection.http_retry_delay)

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_302, datafile("directory-ordinary-response.json")),
            ]
        ),
    )
    def test_redirect_retrieve_users(self, mock_cred_build):
        gapi = GoogleDirectoryConnection(CONFIGURATION)
        with self.assertRaises(HttpError):
            _ = gapi.retrieve_users()

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, "{}"),
            ]
        ),
    )
    def test_empty_retrieve_users(self, mock_cred_build):
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.connection.http_retries = 0
        gapi = GoogleDirectoryConnection(temp_configuration)
        with self.assertRaises(HttpError):
            _ = gapi.retrieve_users()

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, "{}"),
            ]
        ),
    )
    def test_empty_retrieve_deleted_users(self, mock_cred_build):
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.connection.http_retries = 0
        gapi = GoogleDirectoryConnection(temp_configuration)
        users = gapi.retrieve_users(show_deleted=True)
        self.assertEqual(len(users), 0)

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("directory-page1-response.json")),
                (STATUS_200, datafile("directory-page2-response.json")),
            ]
        ),
    )
    def test_paging_retrieve_all_users(self, mock_cred_build):
        gapi = GoogleDirectoryConnection(CONFIGURATION)
        users = gapi.retrieve_users(suspended_only=False)
        self.assertEqual(len(users), 5)
        self.assertEqual(len(gapi.service._http.request_sequence), 2)
        check_urls_contained(
            [
                "admin/directory/v1/users?",
                "admin/directory/v1/users?.*pageToken=here-is-the-next-page",
            ],
            gapi.service._http.request_sequence,
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("directory-has-admins-response.json")),
            ]
        ),
    )
    def test_has_admin_retrieve_users(self, mock_cred_build):
        gapi = GoogleDirectoryConnection(CONFIGURATION)
        with self.assertRaises(RuntimeError):
            _ = gapi.retrieve_users()

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("directory-page1-response.json")),
                (STATUS_200, datafile("directory-page2-response.json")),
            ]
        ),
    )
    def test_has_non_suspended_retrieve_users(self, mock_cred_build):
        gapi = GoogleDirectoryConnection(CONFIGURATION)
        with self.assertRaises(RuntimeError):
            _ = gapi.retrieve_users(suspended_only=True)

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_204, None),
            ]
        ),
    )
    def test_update_user_ucam_fields(self, mock_cred_build):
        gapi = GoogleDirectoryConnection(CONFIGURATION, write_mode=True)
        gapi.update_user_ucam_fields(
            "abc123",
            {
                "mydrive-shared-action": "",
                "mydrive-shared-result": "permissions-removed",
                "mydrive-shared-filecount": 3,
            },
        )
        self.assertEqual(len(gapi.service._http.request_sequence), 1)
        check_urls_contained(
            [r"admin/directory/v1/users/abc123\?"], gapi.service._http.request_sequence
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_204, None),
                (STATUS_204, None),
                (STATUS_204, None),
            ]
        ),
    )
    def test_batch_update_user_ucam_fields(self, mock_cred_build):
        gapi = GoogleDirectoryConnection(CONFIGURATION, write_mode=True)
        # 'Monkey patch' this function, can't be done as a decorator because the service object is
        # dynamically populated.
        batch_factory = MockBatchHttpRequestFactory()
        gapi.service.new_batch_http_request = batch_factory
        gapi.batch_update_user_ucam_fields(
            {
                "abc123": {
                    "mydrive-shared-action": "",
                    "mydrive-shared-result": "permissions-removed",
                    "mydrive-shared-filecount": 3,
                },
                "cba321": {
                    "mydrive-shared-action": "",
                    "mydrive-shared-result": "permissions-none",
                    "mydrive-shared-filecount": 0,
                },
                "xyz789": {
                    "mydrive-shared-action": "scan",
                },
            }
        )
        self.assertEqual(len(gapi.service._http.request_sequence), 3)
        self.assertEqual(batch_factory.created, 1)
        self.assertEqual(len(batch_factory.execute_list), 3)
        batch_factory.check_urls_contained(
            [
                r"admin/directory/v1/users/abc123\?",
                r"admin/directory/v1/users/cba321\?",
                r"admin/directory/v1/users/xyz789\?",
            ]
        )
        batch_factory.check_json_bodies_matched(
            [
                {
                    "customSchemas": {
                        "UCam": {
                            "mydrive-shared-action": "",
                            "mydrive-shared-result": "permissions-removed",
                            "mydrive-shared-filecount": 3,
                        }
                    }
                },
                {
                    "customSchemas": {
                        "UCam": {
                            "mydrive-shared-action": "",
                            "mydrive-shared-result": "permissions-none",
                            "mydrive-shared-filecount": 0,
                        }
                    }
                },
                {
                    "customSchemas": {
                        "UCam": {
                            "mydrive-shared-action": "scan",
                        }
                    }
                },
            ]
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(),
    )
    def test_read_only_batch_update_user_ucam_fields(self, mock_cred_build):
        gapi = GoogleDirectoryConnection(CONFIGURATION, write_mode=False)
        gapi.batch_update_user_ucam_fields(
            {
                "abc123": {
                    "mydrive-shared-action": "",
                    "mydrive-shared-result": "permissions-removed",
                    "mydrive-shared-filecount": 3,
                },
                "cba321": {
                    "mydrive-shared-action": "",
                    "mydrive-shared-result": "permissions-none",
                    "mydrive-shared-filecount": 0,
                },
                "xyz789": {
                    "mydrive-shared-action": "scan",
                },
            }
        )
        self.assertEqual(len(gapi.service._http.request_sequence), 0)

    @mock.patch("gdrivemanager.gapi.sleep")
    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_204, None),
                (STATUS_204, None),
                (STATUS_204, None),
            ]
        ),
    )
    def test_batch_size_batch_update_user_ucam_fields(self, mock_cred_build, mock_sleep):
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.connection.batch_size = 2
        gapi = GoogleDirectoryConnection(temp_configuration, write_mode=True)
        batch_factory = MockBatchHttpRequestFactory()
        gapi.service.new_batch_http_request = batch_factory
        gapi.batch_update_user_ucam_fields(
            {
                "abc123": {
                    "mydrive-shared-action": "",
                    "mydrive-shared-result": "permissions-removed",
                    "mydrive-shared-filecount": 3,
                },
                "cba321": {
                    "mydrive-shared-action": "",
                    "mydrive-shared-result": "permissions-none",
                    "mydrive-shared-filecount": 0,
                },
                "xyz789": {
                    "mydrive-shared-action": "scan",
                },
            }
        )
        mock_sleep.assert_called_once_with(CONFIGURATION.connection.inter_batch_delay)
        self.assertEqual(batch_factory.created, 2)
        self.assertEqual(len(batch_factory.execute_list), 3)
        batch_factory.check_urls_contained(
            [
                r"admin/directory/v1/users/abc123\?",
                r"admin/directory/v1/users/cba321\?",
                r"admin/directory/v1/users/xyz789\?",
            ]
        )

    @mock.patch("gdrivemanager.gapi.sleep")
    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_204, None),
                (STATUS_400, "{}"),
                (STATUS_204, None),
                (STATUS_204, None),
                (STATUS_204, None),
            ]
        ),
    )
    def test_retry_batch_update_user_ucam_fields(self, mock_cred_build, mock_sleep):
        gapi = GoogleDirectoryConnection(CONFIGURATION, write_mode=True)
        batch_factory = MockBatchHttpRequestFactory()
        gapi.service.new_batch_http_request = batch_factory
        gapi.batch_update_user_ucam_fields(
            {
                "abc123": {
                    "mydrive-shared-action": "",
                    "mydrive-shared-result": "permissions-removed",
                    "mydrive-shared-filecount": 3,
                },
                "cba321": {
                    "mydrive-shared-action": "",
                    "mydrive-shared-result": "permissions-none",
                    "mydrive-shared-filecount": 0,
                },
                "xyz789": {
                    "mydrive-shared-action": "scan",
                },
            }
        )
        self.assertEqual(batch_factory.created, 1)
        mock_sleep.assert_called_once_with(CONFIGURATION.connection.http_retry_delay)

    @mock.patch("gdrivemanager.gapi.sleep")
    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_400, "{}"),
                (STATUS_400, "{}"),
            ]
        ),
    )
    def test_retry_exceeded_batch_update_user_ucam_fields(self, mock_cred_build, mock_sleep):
        gapi = GoogleDirectoryConnection(CONFIGURATION, write_mode=True)
        batch_factory = MockBatchHttpRequestFactory()
        gapi.service.new_batch_http_request = batch_factory
        with self.assertRaises(HttpError):
            gapi.batch_update_user_ucam_fields(
                {
                    "abc123": {
                        "mydrive-shared-action": "",
                        "mydrive-shared-result": "permissions-removed",
                        "mydrive-shared-filecount": 3,
                    },
                    "cba321": {
                        "mydrive-shared-action": "",
                        "mydrive-shared-result": "permissions-none",
                        "mydrive-shared-filecount": 0,
                    },
                    "xyz789": {
                        "mydrive-shared-action": "scan",
                    },
                }
            )
        self.assertEqual(batch_factory.created, 1)
        mock_sleep.assert_called_once_with(CONFIGURATION.connection.http_retry_delay)

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("directory-single-user-response.json")),
                (STATUS_403, b""),
                (STATUS_404, b""),
            ]
        ),
    )
    def test_user_has_google_account(self, mock_cred_build):
        gapi = GoogleDirectoryConnection(CONFIGURATION)
        self.assertTrue(gapi.user_has_google_account("af123"))
        self.assertFalse(gapi.user_has_google_account("xyz789"))
        self.assertFalse(gapi.user_has_google_account("uvw321"))

        self.assertEqual(len(gapi.service._http.request_sequence), 3)
        check_urls_contained(
            [
                r"admin/directory/v1/users/af123\?",
                r"admin/directory/v1/users/xyz789\?",
                r"admin/directory/v1/users/uvw321\?",
            ],
            gapi.service._http.request_sequence,
        )


class GoogleDriveConnectionTests(TestCase):
    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    def test_default_construction(self, mock_cred_build):
        _ = GoogleDriveConnection(CONFIGURATION)
        mock_cred_build.assert_called_once_with(CONFIGURATION.google.read_only_credentials)
        mock_cred_build.return_value.with_scopes.assert_called_once_with(
            GoogleDriveConnection.read_only_scopes
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    def test_non_ro_construction(self, mock_cred_build):
        _ = GoogleDriveConnection(CONFIGURATION, write_mode=True)
        mock_cred_build.assert_called_once_with(CONFIGURATION.google.credentials)
        mock_cred_build.return_value.with_scopes.assert_called_once_with(
            GoogleDriveConnection.read_only_scopes + GoogleDriveConnection.write_scopes
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    def test_ro_credentials_missing_construction(self, mock_cred_build):
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.google.read_only_credentials = None
        _ = GoogleDriveConnection(temp_configuration, write_mode=False)
        mock_cred_build.assert_called_once_with(CONFIGURATION.google.credentials)
        mock_cred_build.return_value.with_scopes.assert_called_once_with(
            GoogleDriveConnection.read_only_scopes
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    def test_impersonate_construction(self, mock_cred_build):
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.google.read_only_credentials = None
        _ = GoogleDriveConnection(temp_configuration, impersonate="abc123", write_mode=False)
        mock_cred_build.assert_called_once_with(CONFIGURATION.google.credentials)
        mock_cred_build.return_value.with_scopes.assert_called_once_with(
            GoogleDriveConnection.read_only_scopes
        )
        mock_cred_build.return_value.with_scopes.return_value.with_subject.assert_called_once_with(
            "abc123"
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-ordinary-response.json")),
            ]
        ),
    )
    def test_get_all_owned_files_metadata(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION)
        files = gapi.get_all_owned_files_metadata()
        self.assertEqual(len(files), 5)

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-page1-response.json")),
                (STATUS_200, datafile("drive-page2-response.json")),
            ]
        ),
    )
    def test_get_all_owned_files_metadata_multipage(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION)
        files = gapi.get_all_owned_files_metadata()
        self.assertEqual(len(files), 5)

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-page1-response.json")),
                (STATUS_200, datafile("drive-page2-response.json")),
            ]
        ),
    )
    @freeze_time("2024-02-12 12:00:00", auto_tick_seconds=2 * 60)  # 2 minute tick
    def test_get_all_owned_files_metadata_timeout(self, mock_cred_build):
        temp_configuration = deepcopy(CONFIGURATION)
        # Timeout set for 1 minute but 2 minute tick on freeze_time means it will timeout
        # after the first page has loaded.
        temp_configuration.limits.max_user_scan_duration = 1
        gapi = GoogleDriveConnection(temp_configuration)
        with self.assertRaises(ListTimeoutError):
            _ = gapi.get_all_owned_files_metadata()

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_204, None),
                (STATUS_204, None),
                (STATUS_204, None),
                (STATUS_204, None),
                (STATUS_204, None),
                (STATUS_204, None),
            ]
        ),
    )
    def test_batch_remove_permissions(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=True)
        batch_factory = MockBatchHttpRequestFactory()
        gapi.service.new_batch_http_request = batch_factory
        gapi.batch_remove_permissions(
            {
                "file_id_1": ["permission_id_1", "permission_id_2"],
                "file_id_2": ["permission_id_3"],
                "file_id_3": [],
                "file_id_4": ["permission_id_4", "permission_id_5", "permission_id_6"],
            }
        )
        self.assertEqual(batch_factory.created, 1)
        self.assertEqual(len(batch_factory.execute_list), 6)
        batch_factory.check_urls_contained(
            [
                r"drive/v3/files/file_id_1/permissions/permission_id_1",
                r"drive/v3/files/file_id_1/permissions/permission_id_2",
                r"drive/v3/files/file_id_2/permissions/permission_id_3",
                r"drive/v3/files/file_id_4/permissions/permission_id_4",
                r"drive/v3/files/file_id_4/permissions/permission_id_5",
                r"drive/v3/files/file_id_4/permissions/permission_id_6",
            ]
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                # First permission successfully removed
                (STATUS_204, None),
                # 400 causes batch to be retried
                (STATUS_400, b""),
                # 404 as first permission already removed is ignored
                (STATUS_404, b""),
                # Second permission successfully removed
                (STATUS_204, None),
            ]
        ),
    )
    def test_batch_remove_permissions_with_errors(self, mock_cred_build):
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.connection.http_retry_delay = 2
        gapi = GoogleDriveConnection(temp_configuration, write_mode=True)
        batch_factory = MockBatchHttpRequestFactory()
        gapi.service.new_batch_http_request = batch_factory
        gapi.batch_remove_permissions(
            {
                "file_id_1": ["permission_id_1"],
                "file_id_2": ["permission_id_2"],
            }
        )
        self.assertEqual(batch_factory.created, 1)
        self.assertEqual(len(batch_factory.execute_list), 4)
        batch_factory.check_urls_contained(
            [
                r"drive/v3/files/file_id_1/permissions/permission_id_1",
                r"drive/v3/files/file_id_2/permissions/permission_id_2",
                # Retry
                r"drive/v3/files/file_id_1/permissions/permission_id_1",
                # 404 ignored
                r"drive/v3/files/file_id_2/permissions/permission_id_2",
            ]
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_204, None),  # domain permission
                # 400 (with invalidSharingRequest reason), exception will be ignored and batch
                # retried without missing accounts
                (STATUS_400, datafile("drive-400-error-with-invalidSharingRequest-reason.json")),
                (STATUS_204, None),  # user permission
                # Retries of domain and user permissions, owner permissions are not created.
                (STATUS_204, None),
                (STATUS_204, None),
                # ?
                (STATUS_204, None),
            ]
        ),
    )
    def test_batch_create_shared_permissions(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=True)
        batch_factory = MockBatchHttpRequestFactory()
        gapi.service.new_batch_http_request = batch_factory
        gapi.batch_create_shared_permissions(
            {
                "file_id_1": {
                    "permissions": [
                        {
                            "allowFileDiscovery": False,
                            "domain": "gdev.apps.cam.ac.uk",
                            "id": "p1",
                            "role": "reader",
                            "type": "domain",
                        },
                        {
                            "emailAddress": "foo@bar.com",
                            "id": "p9",
                            "role": "editor",
                            "type": "user",
                        },
                        {
                            "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
                            "id": "p3",
                            "role": "editor",
                            "type": "user",
                        },
                        {
                            "emailAddress": "mk2155@gdev.apps.cam.ac.uk",
                            "id": "p2",
                            "role": "owner",
                            "type": "user",
                        },
                    ]
                },
                "file_id_2": {
                    "permissions": [
                        {
                            "emailAddress": "mk2155@gdev.apps.cam.ac.uk",
                            "id": "p2",
                            "role": "owner",
                            "type": "user",
                        }
                    ]
                },
                "file_id_3": {"permissions": []},
            }
        )
        # Check number of batch requests created was correct (2 due to retry)
        self.assertEqual(batch_factory.created, 2)
        # Check number of requests executed was correct (initial 3 plus 2 in retry)
        self.assertEqual(len(batch_factory.execute_list), 5)
        # Check request URLs all go to 'file_id_1' permissions endpoint
        batch_factory.check_urls_contained(["files/file_id_1/permissions"] * 5)
        # Check request bodies are correct - expecting the
        batch_factory.check_json_bodies_matched(
            [
                {
                    "allowFileDiscovery": False,
                    "domain": "gdev.apps.cam.ac.uk",
                    "role": "reader",
                    "type": "domain",
                },
                {
                    "emailAddress": "foo@bar.com",
                    "role": "editor",
                    "type": "user",
                },
                {
                    "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
                    "role": "editor",
                    "type": "user",
                },
                # Then batch retry without missing account
                {
                    "allowFileDiscovery": False,
                    "domain": "gdev.apps.cam.ac.uk",
                    "role": "reader",
                    "type": "domain",
                },
                {
                    "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
                    "role": "editor",
                    "type": "user",
                },
            ],
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_204, None),
                (STATUS_204, None),
                (STATUS_204, None),
            ]
        ),
    )
    def test_batch_remove_drive_permissions(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=True)
        batch_factory = MockBatchHttpRequestFactory()
        gapi.service.new_batch_http_request = batch_factory
        gapi.batch_remove_drive_permissions(
            "drive1", ["permission_id_1", "permission_id_2", "permission_id_3"]
        )
        self.assertEqual(batch_factory.created, 1)
        self.assertEqual(len(batch_factory.execute_list), 3)
        batch_factory.check_urls_contained(
            [
                r"drive/v3/files/drive1/permissions/permission_id_1",
                r"drive/v3/files/drive1/permissions/permission_id_2",
                r"drive/v3/files/drive1/permissions/permission_id_3",
            ]
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_204, None),
                (STATUS_204, None),
            ]
        ),
    )
    def test_batch_create_drive_permissions(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=True)
        batch_factory = MockBatchHttpRequestFactory()
        gapi.service.new_batch_http_request = batch_factory
        gapi.batch_create_drive_permissions(
            "drive1",
            [
                {
                    "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
                    "role": "organiser",
                    "type": "user",
                },
                {
                    "role": "reader",
                    "type": "anyone",
                },
                # User with no email address isn't created
                {
                    "emailAddress": "",
                    "role": "reader",
                    "type": "user",
                },
            ],
        )
        self.assertEqual(batch_factory.created, 1)
        self.assertEqual(len(batch_factory.execute_list), 2)
        batch_factory.check_urls_contained(["files/drive1/permissions"] * 2)
        # Check request bodies are correct - expecting the
        batch_factory.check_json_bodies_matched(
            [
                {
                    "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
                    "role": "organiser",
                    "type": "user",
                },
                {
                    "role": "reader",
                    "type": "anyone",
                },
            ],
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-single-file-response.json")),
                (
                    STATUS_200,
                    b"The file contents!",
                ),
            ]
        ),
    )
    def test_get_file_contents_by_name(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=True)
        name, content = gapi.get_file_contents_by_name("my-file")
        self.assertEqual(name, "my-file-001.json")
        self.assertEqual(content.decode("utf-8"), "The file contents!")
        self.assertEqual(len(gapi.service._http.request_sequence), 2)

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-no-files-response.json")),
            ]
        ),
    )
    def test_not_found_get_file_contents_by_name(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=True)
        name, content = gapi.get_file_contents_by_name("my-file")
        self.assertIsNone(name)
        self.assertIsNone(content)
        self.assertEqual(len(gapi.service._http.request_sequence), 1)

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-multiple-files-response.json")),
                (
                    STATUS_200,
                    b"The file contents!",
                ),
            ]
        ),
    )
    def test_multiple_found_get_file_contents_by_name(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=True)
        name, content = gapi.get_file_contents_by_name("my-file")
        self.assertEqual(name, "my-file-002.json")
        self.assertEqual(content.decode("utf-8"), "The file contents!")
        self.assertEqual(len(gapi.service._http.request_sequence), 2)
        # Second request should be for f1 as it was modified earlier
        check_urls_contained(
            ["drive/v3/files", "drive/v3/files/f2"], gapi.service._http.request_sequence
        )

    @mock.patch("gdrivemanager.gapi.sleep")
    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                # Retry search
                (STATUS_400, datafile("drive-single-file-response.json")),
                (STATUS_200, datafile("drive-single-file-response.json")),
                (
                    STATUS_200,
                    b"The file contents!",
                ),
                # Retry content
                (STATUS_200, datafile("drive-single-file-response.json")),
                (STATUS_400, b""),
                (
                    STATUS_200,
                    b"The file contents!",
                ),
            ]
        ),
    )
    def test_retry_get_file_contents_by_name(self, mock_cred_build, mock_sleep):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=True)
        name, content = gapi.get_file_contents_by_name("my-file")
        self.assertEqual(name, "my-file-001.json")
        self.assertEqual(content.decode("utf-8"), "The file contents!")
        name, content = gapi.get_file_contents_by_name("my-file")
        self.assertEqual(name, "my-file-001.json")
        self.assertEqual(content.decode("utf-8"), "The file contents!")
        mock_sleep.assert_has_calls([mock.call(CONFIGURATION.connection.http_retry_delay)] * 2)
        self.assertEqual(len(gapi.service._http.request_sequence), 6)
        check_urls_contained(
            [
                "drive/v3/files?",
                "drive/v3/files?",
                "drive/v3/files/f1?",
                "drive/v3/files?",
                "drive/v3/files/f1?",
                "drive/v3/files/f1?",
            ],
            gapi.service._http.request_sequence,
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-no-files-response.json")),
                # Multi-part upload makes two calls, a POST to start expecting a location for a PUT
                # to upload the data to.
                (STATUS_200_LOCATION, "content"),
                (STATUS_200, "content"),
            ]
        ),
    )
    def test_write_file(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=True)
        gapi.write_file("my-file", "This is the file content")
        self.assertEqual(len(gapi.service._http.request_sequence), 3)
        check_urls_contained(
            # Search for existing, create, upload to 'location' specified in return
            [r"drive/v3/files?", r"drive/v3/files\?", STATUS_200_LOCATION["location"]],
            gapi.service._http.request_sequence,
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-single-file-response.json")),
                # Multi-part upload makes two calls, a POST to start expecting a location for a PUT
                # to upload the data to.
                (STATUS_200_LOCATION, "content"),
                (STATUS_200, "content"),
            ]
        ),
    )
    def test_write_existing_file(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=True)
        gapi.write_file("my-file", "This is the file content")
        self.assertEqual(len(gapi.service._http.request_sequence), 3)
        check_urls_contained(
            [r"drive/v3/files\?", r"drive/v3/files/f1\?", STATUS_200_LOCATION["location"]],
            gapi.service._http.request_sequence,
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-no-files-response.json")),
            ]
        ),
    )
    def test_read_only_write_file(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=False)
        gapi.write_file("my-file", "This is the file content")
        self.assertEqual(len(gapi.service._http.request_sequence), 1)
        check_urls_contained(
            [r"drive/v3/files\?"],
            gapi.service._http.request_sequence,
        )

    @mock.patch("gdrivemanager.gapi.sleep")
    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-no-files-response.json")),
                # Test that 400 in first call & in second call both provoke retries
                (STATUS_400, "content"),
                (STATUS_200_LOCATION, "content"),
                (STATUS_400, "content"),
                (STATUS_200_LOCATION, "content"),
                (STATUS_200, "content"),
            ]
        ),
    )
    def test_retry_write_file(self, mock_cred_build, mock_sleep):
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.connection.http_retries = 2
        gapi = GoogleDriveConnection(temp_configuration, write_mode=True)
        gapi.write_file("my-file", "This is the file content")
        mock_sleep.assert_has_calls(
            [
                mock.call(temp_configuration.connection.http_retry_delay),
                mock.call(temp_configuration.connection.http_retry_delay * 2),
            ]
        )

    @mock.patch("gdrivemanager.gapi.sleep")
    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-no-files-response.json")),
                (STATUS_400, "content"),
                (STATUS_200_LOCATION, "content"),
                (STATUS_400, "content"),
            ]
        ),
    )
    def test_retry_exceeded_write_file(self, mock_cred_build, mock_sleep):
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.connection.http_retries = 1
        gapi = GoogleDriveConnection(temp_configuration, write_mode=True)
        with self.assertRaises(HttpError):
            gapi.write_file("my-file", "This is the file content")
        mock_sleep.assert_called_once_with(temp_configuration.connection.http_retry_delay)

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-single-file-response.json")),
                (STATUS_200, "ok"),
            ]
        ),
    )
    def test_update_file_metadata(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=True)

        gapi.update_file_metadata("my-file", {"name": "new-file-name.txt"})
        self.assertEqual(len(gapi.service._http.request_sequence), 2)
        check_urls_contained(
            [r"drive/v3/files\?", r"drive/v3/files/f1\?"],
            gapi.service._http.request_sequence,
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-single-file-response.json")),
            ]
        ),
    )
    def test_read_only_update_file_metadata(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=False)

        gapi.update_file_metadata("my-file", {"name": "new-file-name.txt"})
        self.assertEqual(len(gapi.service._http.request_sequence), 1)
        check_urls_contained(
            [r"drive/v3/files\?"],
            gapi.service._http.request_sequence,
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-no-files-response.json")),
            ]
        ),
    )
    def test_no_file_update_file_metadata(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=True)

        gapi.update_file_metadata("my-file", {"name": "new-file-name.txt"})
        self.assertEqual(len(gapi.service._http.request_sequence), 1)
        check_urls_contained(
            [r"drive/v3/files\?"],
            gapi.service._http.request_sequence,
        )

    @mock.patch("gdrivemanager.gapi.sleep")
    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-single-file-response.json")),
                (STATUS_400, "content"),
                (STATUS_200, "content"),
            ]
        ),
    )
    def test_retry_update_file_metadata(self, mock_cred_build, mock_sleep):
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.connection.http_retries = 2
        gapi = GoogleDriveConnection(temp_configuration, write_mode=True)
        gapi.update_file_metadata("my-file", {"name": "new-file-name.txt"})
        mock_sleep.assert_has_calls([mock.call(temp_configuration.connection.http_retry_delay)])

    @mock.patch("gdrivemanager.gapi.sleep")
    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-single-file-response.json")),
                (STATUS_400, "content"),
                (STATUS_400, "content"),
                (STATUS_200, "content"),
            ]
        ),
    )
    def test_retries_exceeded_update_file_metadata(self, mock_cred_build, mock_sleep):
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.connection.http_retries = 1
        gapi = GoogleDriveConnection(temp_configuration, write_mode=True)
        with self.assertRaises(HttpError):
            gapi.update_file_metadata("my-file", {"name": "new-file-name.txt"})
        mock_sleep.assert_has_calls([mock.call(temp_configuration.connection.http_retry_delay)])

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-about-response.json")),
            ]
        ),
    )
    def test_get_storage_quota_usage(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, impersonate="abc123", write_mode=False)
        usage = gapi.get_storage_quota_usage()
        self.assertEqual(usage, "1234567890")
        self.assertEqual(len(gapi.service._http.request_sequence), 1)
        check_urls_contained(
            [r"drive/v3/about\?"],
            gapi.service._http.request_sequence,
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-shared-drive-cache-response.json")),
                (STATUS_200, datafile("shared_drive_list.yaml")),
            ]
        ),
    )
    def test_get_shared_drive_cache(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=False)
        drive_cache_doc = gapi.get_shared_drive_cache()
        drive_cache = yaml.safe_load(drive_cache_doc.decode("utf-8"))
        self.assertEqual(
            drive_cache["last_updated"].strftime("%Y-%m-%d %H:%M:%S %Z"),
            "2023-05-16 10:11:53 UTC",
        )
        "%Y-%m-%dT%H:%M:%S.%f%z"
        self.assertEqual(len(drive_cache["shared_drives"]), 4)

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-no-files-response.json")),
            ]
        ),
    )
    def test_not_found_get_shared_drive_cache(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=False)
        drive_cache_doc = gapi.get_shared_drive_cache()
        self.assertIsNone(drive_cache_doc)
        self.assertEqual(len(gapi.service._http.request_sequence), 1)

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-shared-drive-list-response.json")),
            ]
        ),
    )
    def test_retrieve_shared_drives(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=False)
        shared_drives = gapi.retrieve_shared_drives()
        self.assertEqual(len(shared_drives), 4)
        expected_orgUnitIds = ["abc123", "def456", "abc123", "def456"]
        for i in range(4):
            self.assertEqual(shared_drives[i]["id"], f"drive{i+1}")
            self.assertEqual(shared_drives[i]["orgUnitId"], expected_orgUnitIds[i])

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, '{"drives": [{"id": "drive1", "name": "Single Drive"}]}'),
                (
                    STATUS_200,
                    """
                    {"permissions": [
                        {"id": "p1", "role": "organizer", "type": "user",
                         "emailAddress": "rjg21@gdev.apps.cam.ac.uk"},
                        {"id": "p2", "role": "fileOrganizer", "type": "user",
                         "emailAddress": "mk2155@gdev.apps.cam.ac.uk"}
                    ]}
                    """,
                ),
            ]
        ),
    )
    def test_read_shared_drive_permissions(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=False)
        shared_drives = gapi.retrieve_shared_drives()
        self.assertEqual(len(shared_drives), 1)
        permissions = gapi.read_shared_drive_permissions(shared_drives[0]["id"])
        self.assertEqual(len(permissions), 2)
        for idx, user in enumerate(["rjg21", "mk2155"]):
            self.assertEqual(permissions[idx]["id"], f"p{idx+1}")
            self.assertEqual(permissions[idx]["emailAddress"].split("@")[0], user)

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-shared-drive-multiple-files-response.json")),
                (STATUS_200, datafile("drive-no-files-response.json")),
            ]
        ),
    )
    def test_get_shared_drive_usage_and_augmented_perms(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=False)
        num_files, usage, aug_perms = gapi.get_shared_drive_usage_and_augmented_perms("drive1")
        self.assertEqual(num_files, 2)
        self.assertEqual(usage, 2000 + 3000)
        self.assertEqual(aug_perms, {"f2"})

        num_files, usage, aug_perms = gapi.get_shared_drive_usage_and_augmented_perms("drive2")
        self.assertEqual(num_files, 0)
        self.assertEqual(usage, 0)
        self.assertEqual(aug_perms, set())

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (
                    STATUS_200,
                    """
                    {"permissions": [
                        {"id": "p1", "role": "organizer", "type": "user",
                         "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
                         "permissionDetails": [{"inherited": false}]},
                        {"id": "p2", "role": "fileOrganizer", "type": "user",
                         "emailAddress": "mk2155@gdev.apps.cam.ac.uk",
                         "permissionDetails": [{"inherited": false}]},
                        {"id": "p3", "role": "writer", "type": "user",
                         "emailAddress": "abc123@gdev.apps.cam.ac.uk",
                         "permissionDetails": [{"inherited": false}]},
                        {"id": "p4", "role": "reader", "type": "user",
                         "emailAddress": "def456@gdev.apps.cam.ac.uk",
                         "permissionDetails": [{"inherited": false}]},
                        {"id": "p5", "role": "reader", "type": "user",
                         "emailAddress": "ghi789@gdev.apps.cam.ac.uk",
                         "permissionDetails": [{"inherited": true}]}
                    ]}
                    """,
                ),
                (
                    STATUS_200,
                    """
                    {"permissions": [
                        {"id": "p1", "role": "organizer", "type": "user",
                         "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
                         "permissionDetails": [{"inherited": false}]},
                        {"id": "p2", "role": "fileOrganizer", "type": "user",
                         "emailAddress": "mk2155@gdev.apps.cam.ac.uk",
                         "permissionDetails": [{"inherited": false}]},
                        {"id": "p6", "role": "commenter", "type": "user",
                         "emailAddress": "abc123@gdev.apps.cam.ac.uk",
                         "permissionDetails": [{"inherited": false}]}
                    ]}
                    """,
                ),
            ]
        ),
    )
    def test_get_shared_drive_augmented_permissions(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=False)

        aug_file_perms = gapi.get_shared_drive_augmented_permissions(["f1", "f2"], {"p1", "p2"})

        self.assertEqual(len(aug_file_perms), 2)
        f1_aug_perms = next(ap for ap in aug_file_perms if ap["file_id"] == "f1")
        self.assertEqual(
            f1_aug_perms,
            {
                "file_id": "f1",
                "permissions": [
                    # p1 and p2 removed as they are drive permissions
                    {
                        "id": "p3",
                        "role": "writer",
                        "type": "user",
                        "emailAddress": "abc123@gdev.apps.cam.ac.uk",
                    },
                    {
                        "id": "p4",
                        "role": "reader",
                        "type": "user",
                        "emailAddress": "def456@gdev.apps.cam.ac.uk",
                    },
                    # p5 removed as it is inherited
                ],
            },
        )
        f2_aug_perms = next(ap for ap in aug_file_perms if ap["file_id"] == "f2")
        self.assertEqual(
            f2_aug_perms,
            {
                "file_id": "f2",
                "permissions": [
                    {
                        "id": "p6",
                        "role": "commenter",
                        "type": "user",
                        "emailAddress": "abc123@gdev.apps.cam.ac.uk",
                    }
                ],
            },
        )

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_200, datafile("drive-permissions-documents.json")),
            ]
        ),
    )
    def test_get_all_files_in_drive_metadata(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, write_mode=False)
        metadatas = gapi.get_all_files_in_drive_metadata("my-drive-id")
        self.assertEqual(len(metadatas), 3)
        self.assertEqual(metadatas[0]["id"], "f1")
        check_urls_contained([r"driveId=my-drive-id"], gapi.service._http.request_sequence)

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (
                    STATUS_403,
                    datafile("drive-403-error-with-teamDriveMembershipRequired-reason.json"),
                ),
                (STATUS_200, b'{"id": "drive2"}'),
            ]
        ),
    )
    def test_check_drive_access(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, "xyz789@cam.ac.uk", write_mode=False)
        # First call should fail with 403
        with self.assertRaises(HttpError):
            gapi.check_drive_access("drive1")
        # second should succeed
        gapi.check_drive_access("drive2")

    @mock.patch("gdrivemanager.gapi.service_account.Credentials.from_service_account_file")
    @mock.patch(
        "gdrivemanager.gapi.discovery.build",
        inject_discovery_mock(
            [
                (STATUS_204, None),
            ]
        ),
    )
    def test_add_manager_to_shared_drive(self, mock_cred_build):
        gapi = GoogleDriveConnection(CONFIGURATION, "xyz789@cam.ac.uk", write_mode=True)
        gapi.add_manager_to_shared_drive("drive1", "abc123@cam.ac.uk")
        self.assertEqual(len(gapi.service._http.request_sequence), 1)
        check_urls_contained(
            [r"drive/v3/files/drive1/permissions"], gapi.service._http.request_sequence
        )
        check_json_bodies_matched(
            [{"role": "organizer", "type": "user", "emailAddress": "abc123@cam.ac.uk"}],
            gapi.service._http.request_sequence,
        )
