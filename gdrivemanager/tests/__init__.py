import collections
import itertools
import json
import os
from unittest import TestCase

from googleapiclient import discovery
from googleapiclient.http import HttpMockSequence

from gdrivemanager.parse_config import parse_configuration

CONFIGURATION = parse_configuration(
    {
        "google": {
            "domain_name": "gdev.apps.cam.ac.uk",
            "credentials": "my-credential-file.txt",
            "read_only_credentials": "my-read-only-credential-file.txt",
            "shared_storage_drive": "my-shared-drive",
            "admin_user": "admin-account@gdev.apps.cam.ac.uk",
            "management_user": "manager@gdev.apps.cam.ac.uk",
        },
        "connection": {
            "timeout": 142,
            "inter_batch_delay": 5,
            "batch_size": 50,
            "http_retries": 1,
            "http_retry_delay": 9,
        },
        "limits": {"max_scanned_users": 19},
        "lookup": {
            "credentials": os.path.join(
                os.path.dirname(__file__), "data", "lookup_credentials.yaml"
            ),
        },
    }
)

# We need a reference back to the original build function to avoid recursion when patching.
BUILD_FUNCTION = discovery.build

# Status object shorthand.
STATUS_200 = {"status": 200}
STATUS_200_LOCATION = {"status": 200, "location": "http://the-location.location"}
STATUS_204 = {"status": 204, "content-type": "none"}
STATUS_302 = {"status": 302}
STATUS_400 = {"status": 400}
STATUS_403 = {"status": 403}
STATUS_404 = {"status": 404}
STATUS_500 = {"status": 500}


def datafile(name):
    """
    Open test json data file.

    """
    with open(os.path.join(os.path.dirname(__file__), "data", name)) as data_file:
        return data_file.read()


def inject_discovery_mock(mock_sequence=[]):
    """
    Function to inject a sequence of responses into the return stack for the discovery function.

    """

    def injector(*args, **kwargs):
        # Can't have credentials & http in the kwargs
        kwargs.pop("credentials")
        kwargs["http"] = HttpMockSequence(mock_sequence)
        # No actual call is made to the HTTP endpoint, so developerKey can be anything
        kwargs["developerKey"] = "none"
        kwargs["cache_discovery"] = False
        return BUILD_FUNCTION(*args, **kwargs)

    return injector


def inject_multiple_discovery_mocks(mock_sequence_sequence=[]):
    """
    Function to inject a sequence of different response sequences for a number of distinct calls
    to the discovery function.

    """

    def injector(*args, **kwargs):
        nonlocal mock_sequence_sequence
        kwargs.pop("credentials")
        kwargs["http"] = HttpMockSequence(mock_sequence_sequence.pop())
        kwargs["developerKey"] = "none"
        kwargs["cache_discovery"] = False
        return BUILD_FUNCTION(*args, **kwargs)

    return injector


class MockBatchHttpRequestFactory:
    """
    Factory class for MockBatchHttpRequest - this allows us to keep track of how many batch
    requests are created as part of a test run.
    """

    # Mock batch request, from https://github.com/googleapis/google-api-python-client/issues/154
    class MockBatchHttpRequest:
        """
        An object that represents an HTTP request from the batch api.
        """

        def __init__(self, factory, callback=None):
            self._callback = callback
            self._callbacks = collections.OrderedDict()
            self._counter = itertools.count()
            self._request_list = []
            self._factory = factory

        def _new_id(self):
            """
            Generate a new id.

            """
            return next(self._counter)

        def add(self, request, callback=None, request_id=None):
            """
            Add a request to be executed as part of the batch.

            """
            request_id = request_id or self._new_id()
            self._callbacks[request_id] = callback
            self._request_list.append(request)

        def execute(self, http=None):
            """
            Execute the request.

            """
            for request, request_id in zip(self._request_list, self._callbacks):
                self._factory.execute_list.append(request)
                response, exception = None, None
                try:
                    response = request.execute(http=http)
                except Exception as e:
                    exception = e

                callback = self._callbacks.get(request_id)
                if callback:
                    callback(request_id, response, exception)
                if self._callback:
                    self._callback(request_id, response, exception)

    def __init__(self):
        self.created = 0
        self.execute_list = []

    def __call__(self, *args, **kwargs):
        self.created += 1
        return MockBatchHttpRequestFactory.MockBatchHttpRequest(self, *args, **kwargs)

    def check_urls_contained(self, expected_url_matches):
        check_urls_contained(expected_url_matches, self.execute_list)

    def check_json_bodies_matched(self, expected_bodies):
        check_json_bodies_matched(expected_bodies, self.execute_list)


def check_urls_contained(expected_url_matches, request_list):
    tc = TestCase()
    try:
        [
            tc.assertRegex(*pair)
            for pair in zip(
                [request.uri for request in request_list],
                expected_url_matches,
            )
        ]
    # Request wasn't expected type, probably a tuple instead
    except AttributeError:
        [
            tc.assertRegex(*pair)
            for pair in zip(
                [request[0] for request in request_list],
                expected_url_matches,
            )
        ]


def check_json_bodies_matched(expected_bodies, request_list):
    tc = TestCase()
    try:
        tc.assertListEqual(
            [json.loads(request.body) for request in request_list],
            expected_bodies,
        )
    # Request wasn't expected type, probably a tuple instead
    except AttributeError:
        tc.assertListEqual(
            [json.loads(request[2]) for request in request_list],
            expected_bodies,
        )


def index_by_id(list_of_items, id):
    return next((index for (index, item) in enumerate(list_of_items) if item["id"] == id), None)


# Mock os.DirEntry for mocking os.scandir results
class Mock_DirEntry:
    def __init__(self, name, is_dir=False):
        self.name = name
        self.is_dir = is_dir

    def is_file(self):
        return not self.is_dir
