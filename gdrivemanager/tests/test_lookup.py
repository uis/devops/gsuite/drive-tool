from unittest import TestCase, mock
from identitylib.lookup_client.exceptions import NotFoundException

from . import (
    CONFIGURATION,
)
from .mock_api_clients import (
    MockConfiguration,
    MockApiClient,
    MockLookupPersonApi,
    MockLookupInstitutionApi,
    MockLookupGroupApi,
    LookupPerson,
    LookupPersonAttribute,
    LookupNamelessPerson,
    LookupInstlessPerson,
    LookupPersonIdentifier,
    LookupInstitution,
    LookupGroup,
)

from gdrivemanager.lookup import LookupConnection


class LookupConnectionTests(TestCase):
    @mock.patch("gdrivemanager.lookup.GroupApi", return_value=MockLookupGroupApi())
    @mock.patch("gdrivemanager.lookup.PersonApi", return_value=MockLookupPersonApi())
    @mock.patch("gdrivemanager.lookup.InstitutionApi", return_value=MockLookupInstitutionApi())
    @mock.patch("gdrivemanager.lookup.LookupApiClient", return_value=MockApiClient())
    @mock.patch("gdrivemanager.lookup.LookupClientConfiguration", return_value=MockConfiguration())
    def setUp(
        self,
        mock_config: mock.MagicMock,
        mock_apiclient: mock.MagicMock,
        mock_inst_api: mock.MagicMock,
        mock_person_api: mock.MagicMock,
        mock_group_api: mock.MagicMock,
    ):
        self.mock_config = mock_config
        self.mock_apiclient = mock_apiclient
        self.mock_inst_api = mock_inst_api
        self.mock_person_api = mock_person_api
        self.mock_group_api = mock_group_api

        self.lookup_connection = LookupConnection(CONFIGURATION)

        # Create some common mock people fixtures
        self.person = {
            "abc123": self._person(
                "abc123",
                "A. B. C. One Two Three",
                False,
                ["FOO", "BAR"],
                "FOO",
            ),
            "uvw456": self._person(
                "uvw456",
                None,  # Person with no display name
                False,
                ["BAZ"],
                "QUX",
            ),
            "xyz789": self._person(
                "xyz789",
                "X. Y. Z. Seven Eight Nine",
                True,
                None,  # Person with no institutions
                "BAR",
            ),
            "qux123": self._person(
                "qux123",
                "Q. U. X. One Two Three",
                False,
                ["QUX"],
                "QUX",
            ),
        }

    # Helper function to create Mock Person
    def _person(
        self, crsid, display_name=None, cancelled=False, institutions=None, js_inst_id=None
    ):
        if institutions is None:
            return LookupInstlessPerson(
                identifier=LookupPersonIdentifier(scheme="crsid", value=crsid),
                display_name=display_name,
                cancelled=cancelled,
                attributes=[LookupPersonAttribute(value=js_inst_id, scheme="jdInstid")],
            )
        # Convert a list of instids to a list of LookupInstitution objects
        if len(institutions) and not isinstance(institutions[0], LookupInstitution):
            institutions = [self._inst(instid) for instid in institutions]

        if display_name is None:
            return LookupNamelessPerson(
                identifier=LookupPersonIdentifier(scheme="crsid", value=crsid),
                cancelled=cancelled,
                institutions=institutions,
                attributes=[LookupPersonAttribute(value=js_inst_id, scheme="jdInstid")],
            )
        return LookupPerson(
            identifier=LookupPersonIdentifier(scheme="crsid", value=crsid),
            display_name=display_name,
            cancelled=cancelled,
            institutions=institutions,
            attributes=[LookupPersonAttribute(value=js_inst_id, scheme="jdInstid")],
        )

    # Helper function to create a minimal Mock Institution
    def _inst(self, instid):
        return LookupInstitution(
            instid=instid,
            members=[],
            child_insts=[],
        )

    def test_default_construction(self):
        self.assertIsInstance(self.lookup_connection, LookupConnection)
        self.mock_config.assert_called_once_with(
            "Mock API Gateway Client ID",
            "Mock API Gateway Client Secret",
            base_url=CONFIGURATION.lookup.base_url,
        )
        self.mock_apiclient.assert_called_once_with(self.mock_config.return_value, pool_threads=10)
        self.mock_inst_api.assert_called_once_with(self.mock_apiclient.return_value)
        self.mock_person_api.assert_called_once_with(self.mock_apiclient.return_value)

    def test_retrieve_users_by_instid(self):
        # Mock institution data with both active and cancelled members and someone without a
        # display name.
        self.lookup_connection.inst_api_client.entities = [
            LookupInstitution(
                "FOO",
                members=[self.person["abc123"], self.person["xyz789"], self.person["uvw456"]],
                child_insts=[],
            )
        ]

        users = sorted(
            self.lookup_connection.retrieve_users_by_instid("FOO").values(), key=lambda x: x["id"]
        )
        self.assertEqual(len(users), 3)
        self.assertEqual(users[0]["id"], "abc123")
        self.assertEqual(users[0]["display_name"], "A. B. C. One Two Three")
        # Users are sorted by ID so xyz789 comes after uvw456
        self.assertEqual(users[1]["id"], "uvw456")
        self.assertEqual(users[1]["display_name"], "")
        self.assertEqual(users[2]["id"], "xyz789")
        self.assertEqual(users[2]["display_name"], "X. Y. Z. Seven Eight Nine")

        # Institutions get merged from the institutions and attributes fields, if available
        self.assertEqual(users[0]["insts"], ["BAR", "FOO"])
        self.assertEqual(users[1]["insts"], ["BAZ", "QUX"])
        self.assertEqual(users[2]["insts"], ["BAR"])

        with self.assertRaises(NotFoundException):
            users = self.lookup_connection.retrieve_users_by_instid("BAR")

    def test_retrieve_users_by_instid_children(self):
        # Mock institution data with at least two levels of child institutions
        child_child_inst = LookupInstitution(
            instid="QUX", members=[self.person["qux123"]], child_insts=[]
        )
        child_inst_1 = LookupInstitution(
            instid="BAR", members=[self.person["xyz789"]], child_insts=[]
        )
        child_inst_2 = LookupInstitution(
            instid="BAZ", members=[self.person["uvw456"]], child_insts=[child_child_inst]
        )
        self.lookup_connection.inst_api_client.entities = [
            LookupInstitution(
                instid="FOO",
                members=[self.person["abc123"]],
                child_insts=[child_inst_1, child_inst_2],
            ),
            child_inst_1,
            child_inst_2,
            child_child_inst,
        ]

        ids = sorted(
            self.lookup_connection.retrieve_users_by_instid("FOO", include_children=True).keys()
        )
        # IDs contain all 4 persons including those in child institutions
        self.assertEqual(ids, ["abc123", "qux123", "uvw456", "xyz789"])

    def test_retrieve_user_by_crsid(self):
        # Mock person data
        self.lookup_connection.person_api_client.entities = [
            self.person["abc123"],
        ]

        user = self.lookup_connection.retrieve_user_by_crsid("abc123")
        self.assertEqual(user["id"], "abc123")
        self.assertEqual(user["display_name"], "A. B. C. One Two Three")
        self.assertEqual(user["cancelled"], False)
        # Insts are sorted
        self.assertEqual(user["insts"], ["BAR", "FOO"])

        with self.assertRaises(ValueError):
            user = self.lookup_connection.retrieve_user_by_crsid("xyz789")

    def test_retrieve_users_by_groupid(self):
        # Mock institution data with both active and cancelled members and someone without a
        # display name.
        self.lookup_connection.group_api_client.entities = [
            LookupGroup(
                groupid="123",
                name="foo-group",
                members=[
                    self.person["abc123"],
                    self.person["xyz789"],
                    self.person["uvw456"],
                ],
            )
        ]

        # By groupid

        users = self.lookup_connection.retrieve_users_by_group("123")
        self.assertEqual(len(users), 3)
        self.assertEqual(users[0]["id"], "abc123")
        self.assertEqual(users[0]["display_name"], "A. B. C. One Two Three")
        self.assertEqual(users[0]["insts"], ["BAR", "FOO"])
        # Users are sorted by ID so xyz789 comes after uvw456
        self.assertEqual(users[1]["id"], "uvw456")
        self.assertEqual(users[1]["display_name"], "")
        self.assertEqual(users[1]["insts"], ["BAZ", "QUX"])
        self.assertEqual(users[2]["id"], "xyz789")
        self.assertEqual(users[2]["display_name"], "X. Y. Z. Seven Eight Nine")
        self.assertEqual(users[2]["insts"], ["BAR"])

        # By short name

        users = self.lookup_connection.retrieve_users_by_group("foo-group")
        self.assertEqual(len(users), 3)

        with self.assertRaises(NotFoundException):
            users = self.lookup_connection.retrieve_users_by_group("bar-group")
