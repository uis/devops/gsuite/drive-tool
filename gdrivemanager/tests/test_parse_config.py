from unittest import TestCase, mock

from gdrivemanager.parse_config import ConfigurationNotFound, load_configuration


class ParseConfigTestCases(TestCase):
    basic_yaml = """
top_level:
  key: value
  list:
  - 1
  - 2
"""
    basic_yaml_dict = {"top_level": {"key": "value", "list": [1, 2]}}

    @mock.patch("os.path.isfile", side_effect=[True])
    @mock.patch("builtins.open", new_callable=mock.mock_open, read_data=basic_yaml)
    def test_load_configuration_with_location(self, mock_open, mock_isfile):
        config = load_configuration("my-config.yaml")
        self.assertDictEqual(config, self.basic_yaml_dict)

    @mock.patch("os.path.isfile", side_effect=[False])
    @mock.patch("builtins.open", new_callable=mock.mock_open, read_data=basic_yaml)
    def test_load_configuration_with_bad_location(self, mock_open, mock_isfile):
        with self.assertRaises(ConfigurationNotFound):
            load_configuration("my-config.yaml")

    @mock.patch("os.path.isfile", side_effect=[True, True, True, True])
    @mock.patch("builtins.open", new_callable=mock.mock_open, read_data=basic_yaml)
    @mock.patch.dict("os.environ", {"GDRIVEMANAGE_CONFIGURATION": "my-env-config.yaml"})
    def test_load_configuration_with_env_var(self, mock_open, mock_isfile):
        load_configuration()
        mock_open.assert_called_once_with("my-env-config.yaml")

    @mock.patch("os.getcwd", return_value="/path/to")
    @mock.patch("os.path.isfile", side_effect=[False, True, True, True])
    @mock.patch("builtins.open", new_callable=mock.mock_open, read_data=basic_yaml)
    @mock.patch.dict("os.environ", {"GDRIVEMANAGE_CONFIGURATION": "my-env-config.yaml"})
    def test_load_configuration_with_bad_env_var(self, mock_open, mock_isfile, mock_getcwd):
        load_configuration()
        mock_open.assert_called_once_with("/path/to/configuration.yaml")
