import logging
from collections import namedtuple
from pprint import pformat

import yaml
from googleapiclient.errors import HttpError

from ..gapi import GoogleDirectoryConnection, GoogleDriveConnection
from ..utils import info_from_filename, merge_dict, user_email_is_crsid
from .user_drives import UserProcessingError

LOG = logging.getLogger(__name__)

UserStatus = namedtuple("UserStatus", ["suspended", "recovery_documents"])


# Management Command Function - `gdrivemanager clean-recovery-drive`


def clean_recovery_drive(configuration, write_mode=False):
    """
    Get list of oldest recovery files and process each.

    """
    LOG.info(f"Running CLEAN RECOVERY DRIVE in {'WRITE' if write_mode else 'READ ONLY'} mode.")

    # Shorthand this operation for readability
    def extract_crsid(user):
        return user.get("primaryEmail").split("@")[0]

    directory_connection = GoogleDirectoryConnection(configuration)

    shared_drive_connection = GoogleDriveConnection(configuration, write_mode=write_mode)
    shared_drive_id = configuration.google.shared_storage_drive

    files = [
        {**file, **{"permission_info": info_from_filename(file["name"])}}
        for file in shared_drive_connection.get_all_files_in_drive_metadata(shared_drive_id)
    ]

    valid_files = [file for file in files if file["permission_info"]]

    # Get the X oldest files, this effectively caps the number of processed users as we ensure
    # each processed entry in the `all_users` list has a file that appears in this list
    oldest_files = [
        file
        for file in sorted(valid_files, key=lambda file: file["modifiedTime"])[
            : configuration.limits.max_cleanup_files
        ]
    ]
    oldest_files_crsids = [info_from_filename(file["name"]).crsid for file in oldest_files]

    live_users = directory_connection.retrieve_users(suspended_only=False)
    deleted_users = directory_connection.retrieve_users(show_deleted=True, suspended_only=False)
    all_valid_crsids = set([extract_crsid(user) for user in live_users + deleted_users])

    # Any files with no user entry in the live or deleted list will be removed
    files_without_users = [
        file for file in oldest_files if file["permission_info"].crsid not in all_valid_crsids
    ]

    live_users_by_crsid = {
        extract_crsid(user): user for user in live_users if user_email_is_crsid(user)
    }

    # All users is a dictionary where for each entry:
    # - The UserStatus contains the full list of recovery documents for the user.
    # - Every entry has a document in the recovery document list that appears in the
    #   `oldest_files_crsids` list.
    # - Every user will have at least one file in their UserStatus.recovery_documents, as they must
    #   have an entry in the `oldest_files_crsids` list.
    # - Every users' list of recovery_documents will be unique, there will be no duplicate
    #   documents in the set of all UserStatus.recovery_documents objects in this dict.
    # This means that there are, at most, `configuration.limits.max_cleanup_files` entries in this
    # list. If a user has multiple files in the oldest files, they will still appear in this list
    # only once.
    live_users_for_clean = {
        crsid: UserStatus(
            suspended=user.get("suspended"),
            recovery_documents=sorted(
                [file for file in valid_files if file["permission_info"].crsid == crsid],
                key=lambda file: file.get("modifiedTime"),
            ),
        )
        for crsid, user in live_users_by_crsid.items()
        if crsid in oldest_files_crsids
    }

    LOG.info(f"Cleaning files for {len(live_users_for_clean)} users")

    # Clean files for live users
    for crsid, user_status in live_users_for_clean.items():
        try:
            clean_up_user_files(crsid, user_status, shared_drive_connection, shared_drive_id)
        except (HttpError, UserProcessingError):
            continue

    LOG.info(f"Trashing {len(files_without_users)} unused files")

    # Trash any files with no associated user
    for file in files_without_users:
        try:
            shared_drive_connection.trash_file(file["id"], file["name"])
        except HttpError:
            continue

    # Files with recently deleted users are ignored, will be cleaned when the user is fully
    # removed from the directory.


def clean_up_user_files(crsid, user_status, shared_drive_connection, shared_drive_id):
    """
    Process a single file for clean-up:
      - if user is suspended, conglomerate all historic files into one file (the oldest one), and
        conglomerate all live files into another file (the oldest live file).
      - if user is unsuspended, ensure file is historic and consolidate all files into one.
      - touch file to update modified time.
    """
    LOG.info(f"Cleaning files for user {crsid}")
    if user_status.suspended:
        LOG.info(f"{crsid} is suspended")
        live_files = [
            file
            for file in user_status.recovery_documents
            if not info_from_filename(file["name"]).historic
        ]
        historic_files = [
            file
            for file in user_status.recovery_documents
            if info_from_filename(file["name"]).historic
        ]
        LOG.info(
            f"{crsid} has {len(live_files)} live files and {len(historic_files)} historic files"
        )
        LOG.debug(f"Live files:\n{pformat(live_files)}")
        conglomerate_or_update(
            live_files,
            shared_drive_connection,
            shared_drive_id,
        )
        LOG.debug(f"Historic files:\n{pformat(historic_files)}")
        conglomerate_or_update(
            historic_files,
            shared_drive_connection,
            shared_drive_id,
        )
    else:
        LOG.info(
            f"{crsid} is not suspended, conglomerating {len(user_status.recovery_documents)} "
            "and marking as historic"
        )
        LOG.debug(f"User files:\n{pformat(user_status.recovery_documents)}")
        # Conglomerate all files
        # Mark as historic
        # Guaranteed to exist
        file = user_status.recovery_documents[0]
        historic, crsid, google_id = info_from_filename(file["name"])
        new_filename = file["name"]
        # May be empty
        other_user_files = user_status.recovery_documents[1:]
        if not historic:
            new_filename = "HISTORIC-" + new_filename
        # This may not actually change the name, but it's important to update the
        # modifiedTime field, so even if this update would be a no-op it should still be
        # performed.
        shared_drive_connection.update_file_metadata(
            file["name"],
            {"name": new_filename},
            shared_drive_id,
        )
        file["name"] = new_filename
        if other_user_files:
            conglomerate_permissions_documents(
                file,
                other_user_files,
                shared_drive_connection,
                shared_drive_id,
            )


def conglomerate_or_update(file_list, shared_drive_connection, shared_drive_id):
    """
    Takes in a file list files and will either conglomerate the files together or, if there are no
    files to merge, will perform a no-op patch on the file to update the single files
    'modifiedTime' time.
    """
    if len(file_list) > 1:
        conglomerate_permissions_documents(
            file_list[0],
            file_list[1:],
            shared_drive_connection,
            shared_drive_id,
        )
    # Update modifiedTime, need to include at least one field in the PATCH document, even if this
    # is effectively a no-op.
    elif file_list:
        LOG.info(f"Updating file id {file_list[0]['id']} to set modifiedTime")
        shared_drive_connection.update_file_metadata(
            file_list[0]["name"],
            {"name": file_list[0]["name"]},
            shared_drive_id,
        )


def conglomerate_permissions_documents(
    main_file, other_files, shared_drive_connection, shared_drive_id
):
    """
    Takes in a 'primary' file id and a list of other file ids, and conglomerates them together,
    then saves the resulting merged document back to the original 'primary' file id with the name
    specified by 'main_filename'. Once the merge is complete all the other file ids will be
    trashed.

    """
    LOG.info(
        f"Conglomerating {len(other_files) + 1} documents and saving to file id "
        f"{main_file['id']}, name {main_file['name']}"
    )
    raw_doc = shared_drive_connection.get_file_contents_by_id(
        main_file["id"], drive_id=shared_drive_id
    )
    try:
        permissions_recovery_doc = yaml.load(raw_doc.decode("utf-8"), Loader=yaml.CLoader)
    except yaml.YAMLError:
        LOG.error(
            f"Permissions document {main_file['name']} was not valid YAML. If it has been "
            "modified the user scan may end up in an invalid state."
        )
        raise UserProcessingError()

    for other in other_files:
        LOG.info(f"Reading and parsing {other['id']} - {other['name']}")
        raw_other = shared_drive_connection.get_file_contents_by_id(
            other["id"], drive_id=shared_drive_id
        )
        try:
            other_loaded = yaml.load(raw_other.decode("utf-8"), Loader=yaml.CLoader)
        except yaml.YAMLError:
            LOG.error(
                f"Permissions document {other} was not valid YAML. If it has been modified the "
                "user scan may end up in an invalid state."
            )
            raise UserProcessingError()

        merge_dict(permissions_recovery_doc, other_loaded)

    LOG.info("Writing conglomerated permissions document")
    LOG.debug(f"Conglomerated permissions document:\n{pformat(permissions_recovery_doc)}")
    shared_drive_connection.write_file(
        main_file["name"],
        yaml.dump(permissions_recovery_doc, Dumper=yaml.CDumper),
        mimetype="text/yaml",
        parents=[shared_drive_id],
    )

    # Trash in a separate loop, to ensure that the full permissions document has been created
    # before deleting any files.
    LOG.info(
        f"Trashing {len(other_files)} documents as their contents are merged into "
        f"{main_file['name']}"
    )
    for other in other_files:
        shared_drive_connection.trash_file(other["id"], other["name"])
