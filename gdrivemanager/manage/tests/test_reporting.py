from copy import deepcopy
from datetime import datetime, timezone
from unittest import TestCase, mock

from freezegun import freeze_time

from gdrivemanager.manage.reporting import (
    ReportType,
    filter_users_by_google_account,
    find_report_request_file,
    get_user_usage_cache,
    get_users_for_report,
    report,
    shared_drive_report,
    update_user_usage_cache,
    write_report,
)
from gdrivemanager.tests import CONFIGURATION, Mock_DirEntry, datafile
from gdrivemanager.utils import generate_csv


class ManageReportingTests(TestCase):
    frozen_time = datetime(2023, 1, 1, 1, 30, tzinfo=timezone.utc)

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.reporting.get_user_usage_cache")
    @mock.patch("gdrivemanager.manage.reporting.write_report")
    @mock.patch("gdrivemanager.manage.reporting.get_users_for_report")
    @mock.patch("gdrivemanager.manage.reporting.GoogleDriveConnection")
    @mock.patch("gdrivemanager.manage.reporting.GoogleDirectoryConnection")
    def test_report_for_user_no_output_location(
        self,
        mock_directory,
        mock_drive,
        mock_get_users_for_report,
        mock_write_report,
        mock_get_user_usage_cache,
    ):
        # Get report for a user without a output_location set
        self.assertIsNone(CONFIGURATION.report.output_location)

        mock_get_users_for_report.return_value = (
            [
                {
                    "id": "mk2155",
                    "email": "mk2155@gdev.apps.cam.ac.uk",
                    "display_name": "Michael Knight",
                    "cancelled": False,
                    "insts": ["FOO", "TESTINST"],
                }
            ],
            None,
            "mk2155",
        )
        mock_directory.return_value.user_has_google_account.return_value = True

        # Mock user usage and shared drive cache returned by drive connection
        mock_drive.return_value.get_storage_quota_usage.return_value = 12345
        mock_drive.return_value.get_shared_drive_cache.return_value = bytes(
            datafile("shared_drive_list.yaml"), "utf-8"
        )

        report(CONFIGURATION, ReportType.USER, "MK2155", cache_users=False, write_mode=True)

        # Get users for report called for just user with no drive connection
        mock_get_users_for_report.assert_called_once_with(
            CONFIGURATION, ReportType.USER, "MK2155", False, None
        )

        # Not using user usage cache file so get_user_usage_cache not called
        mock_get_user_usage_cache.assert_not_called()

        # GoogleDriveConnection created twice (to read user's MyDrive usage and look for
        # shared drives cache) but not to create output files
        mock_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, "mk2155@gdev.apps.cam.ac.uk", write_mode=False),
                mock.call().get_storage_quota_usage(),
                mock.call(CONFIGURATION, write_mode=False),
                mock.call().get_shared_drive_cache(),
            ]
        )

        # Report files to be written to local directory
        mock_write_report.assert_has_calls(
            [
                mock.call(
                    CONFIGURATION,
                    [
                        {
                            "id": "mk2155",
                            "display_name": "Michael Knight",
                            "cancelled": False,
                            "usage": 12345,
                            "insts": "FOO, TESTINST",
                        }
                    ],
                    "mk2155-mydrive-20230101-013000.csv",
                    None,
                    True,
                ),
                mock.call(
                    CONFIGURATION,
                    [
                        {
                            "id": "drive1",
                            "name": "Drive with all types of permissions",
                            "managers": "mk2155",
                            "content_managers": "",
                            "insts": "FOO, TESTINST",
                            "files": 22,
                            "usage": 123456,
                            "as_of": datetime(2023, 5, 15, 11, 4, 40, 661098, tzinfo=timezone.utc),
                        },
                        {
                            "id": "drive2",
                            "name": "Empty Drive",
                            "managers": "",
                            "content_managers": "mk2155",
                            "insts": "FOO, TESTINST",
                            "files": 0,
                            "usage": 0,
                            "as_of": "Unknown",
                        },
                    ],
                    "mk2155-shared-drive-20230101-013000.csv",
                    None,
                    True,
                ),
            ]
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.reporting.get_user_usage_cache")
    @mock.patch("gdrivemanager.manage.reporting.write_report")
    @mock.patch("gdrivemanager.manage.reporting.get_users_for_report")
    @mock.patch("gdrivemanager.manage.reporting.GoogleDriveConnection")
    @mock.patch("gdrivemanager.manage.reporting.GoogleDirectoryConnection")
    def test_report_for_user_with_output_location_and_cache(
        self,
        mock_directory,
        mock_drive,
        mock_get_users_for_report,
        mock_write_report,
        mock_get_user_usage_cache,
    ):
        # Get report for a user using an output_location and using a user usage cache
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.report.output_location = "drive1"

        # Multiple calls to GoogleDriveConnection are made during report() when using
        # an output_location so have a separate mock for each
        output_drive_connection = mock.Mock(name="output")
        shared_drive_connection = mock.Mock(name="shared_drive")
        mock_list = [
            output_drive_connection,
            shared_drive_connection,
        ]

        def mock_construct(configuration, impersonate=None, write_mode=False):
            nonlocal mock_list
            # Keep track of which mock was constructed so we can assert on it later
            next_mock = mock_list.pop(0)
            next_mock.constructed_with = (configuration, impersonate, write_mode)
            return next_mock

        mock_drive.side_effect = mock_construct

        mock_get_users_for_report.return_value = (
            [
                {
                    "id": "mk2155",
                    "email": "mk2155@gdev.apps.cam.ac.uk",
                    "display_name": "Michael Knight",
                    "cancelled": False,
                    "insts": ["FOO", "TESTINST"],
                }
            ],
            None,
            "mk2155",
        )
        mock_directory.return_value.user_has_google_account.return_value = True

        # Mock user usage cache file content
        mock_get_user_usage_cache.return_value = {
            "abc123": 9999,
            "mk2155": 12345,
            "xyz789": 123456,
        }

        # Mock shared drive cache returned by drive connection
        shared_drive_connection.get_shared_drive_cache.return_value = bytes(
            datafile("shared_drive_list.yaml"), "utf-8"
        )

        report(temp_configuration, ReportType.USER, "MK2155", cache_users=True, write_mode=True)

        # output_drive_connection created to write report files
        self.assertEqual(
            output_drive_connection.constructed_with, (temp_configuration, None, True)
        )

        # Get users for report called for just user with no drive connection
        mock_get_users_for_report.assert_called_once_with(
            temp_configuration, ReportType.USER, "MK2155", False, output_drive_connection
        )

        # User usage cache file is retrieved
        mock_get_user_usage_cache.assert_called_once_with(temp_configuration)

        # User storage quota is not retrieved as it is in the cache
        mock_drive.return_value.get_storage_quota_usage.assert_not_called()

        # Shared drive connection created and cache is retrieved
        self.assertEqual(
            shared_drive_connection.constructed_with, (temp_configuration, None, False)
        )
        shared_drive_connection.get_shared_drive_cache.assert_called_once()

        # Report files to be written to drive location
        mock_write_report.assert_has_calls(
            [
                mock.call(
                    temp_configuration,
                    [
                        {
                            "id": "mk2155",
                            "display_name": "Michael Knight",
                            "cancelled": False,
                            "usage": 12345,
                            "insts": "FOO, TESTINST",
                        }
                    ],
                    "mk2155-mydrive-20230101-013000.csv",
                    output_drive_connection,
                    True,
                ),
                mock.call(
                    temp_configuration,
                    [
                        {
                            "id": "drive1",
                            "name": "Drive with all types of permissions",
                            "managers": "mk2155",
                            "content_managers": "",
                            "insts": "FOO, TESTINST",
                            "files": 22,
                            "usage": 123456,
                            "as_of": datetime(2023, 5, 15, 11, 4, 40, 661098, tzinfo=timezone.utc),
                        },
                        {
                            "id": "drive2",
                            "name": "Empty Drive",
                            "managers": "",
                            "content_managers": "mk2155",
                            "insts": "FOO, TESTINST",
                            "files": 0,
                            "usage": 0,
                            "as_of": "Unknown",
                        },
                    ],
                    "mk2155-shared-drive-20230101-013000.csv",
                    output_drive_connection,
                    True,
                ),
            ]
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.reporting.write_report")
    @mock.patch("gdrivemanager.manage.reporting.get_users_for_report")
    @mock.patch("gdrivemanager.manage.reporting.GoogleDriveConnection")
    @mock.patch("gdrivemanager.manage.reporting.GoogleDirectoryConnection")
    def test_report_for_group(
        self,
        mock_directory,
        mock_drive,
        mock_get_users_for_report,
        mock_write_report,
    ):
        mock_get_users_for_report.return_value = (
            [
                {
                    "id": "abc123",
                    "email": "abc123@gdev.apps.cam.ac.uk",
                    "display_name": "A. Cole",
                    "cancelled": True,
                    "insts": ["QUX"],
                },
                {
                    "id": "mk2155",
                    "email": "mk2155@gdev.apps.cam.ac.uk",
                    "display_name": "Michael Knight",
                    "cancelled": False,
                    "insts": ["BAR", "FOO"],
                },
                {
                    "id": "rjg21",
                    "email": "rjg21@gdev.apps.cam.ac.uk",
                    "display_name": "R. Goodall",
                    "cancelled": False,
                    "insts": ["BAZ", "TESTINST"],
                },
            ],
            None,
            "group1",
        )

        # First doesn't have a Google account
        mock_directory.return_value.user_has_google_account.side_effect = [False, True, True]

        # Mock user usages and shared drive cache returned by drive connection
        mock_drive.return_value.get_storage_quota_usage.side_effect = [54321, 12345]
        mock_drive.return_value.get_shared_drive_cache.return_value = bytes(
            datafile("shared_drive_list.yaml"), "utf-8"
        )

        report(CONFIGURATION, ReportType.GROUP, "GROUP1", cache_users=False, write_mode=True)

        # Report files to be written to local directory
        mock_write_report.assert_has_calls(
            [
                mock.call(
                    CONFIGURATION,
                    [
                        {
                            "id": "mk2155",
                            "display_name": "Michael Knight",
                            "cancelled": False,
                            "usage": 54321,
                            "insts": "BAR, FOO",
                        },
                        {
                            "id": "rjg21",
                            "display_name": "R. Goodall",
                            "cancelled": False,
                            "usage": 12345,
                            "insts": "BAZ, TESTINST",
                        },
                    ],
                    "group1-mydrive-20230101-013000.csv",
                    None,
                    True,
                ),
                mock.call(
                    CONFIGURATION,
                    [
                        {
                            "id": "drive1",
                            "name": "Drive with all types of permissions",
                            "managers": "mk2155",
                            "content_managers": "",
                            "insts": "BAR, FOO",
                            "files": 22,
                            "usage": 123456,
                            "as_of": datetime(2023, 5, 15, 11, 4, 40, 661098, tzinfo=timezone.utc),
                        },
                        {
                            "id": "drive2",
                            "name": "Empty Drive",
                            "managers": "",
                            "content_managers": "mk2155",
                            "insts": "BAR, FOO",
                            "files": 0,
                            "usage": 0,
                            "as_of": "Unknown",
                        },
                        {
                            "id": "drive3",
                            "name": "Unscanned Drive",
                            "managers": "rjg21",
                            "content_managers": "",
                            "insts": "BAZ, TESTINST",
                            "files": None,
                            "usage": None,
                            "as_of": "Unknown",
                        },
                    ],
                    "group1-shared-drive-20230101-013000.csv",
                    None,
                    True,
                ),
            ]
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.reporting.write_report")
    @mock.patch("gdrivemanager.manage.reporting.get_users_for_report")
    @mock.patch("gdrivemanager.manage.reporting.GoogleDriveConnection")
    @mock.patch("gdrivemanager.manage.reporting.GoogleDirectoryConnection")
    def test_report_for_inst(
        self,
        mock_directory,
        mock_drive,
        mock_get_users_for_report,
        mock_write_report,
    ):
        mock_get_users_for_report.return_value = (
            [
                {
                    "id": "abc123",
                    "email": "abc123@gdev.apps.cam.ac.uk",
                    "display_name": "A. Cole",
                    "cancelled": True,
                    "insts": ["QUX"],
                },
                {
                    "id": "mk2155",
                    "email": "mk2155@gdev.apps.cam.ac.uk",
                    "display_name": "Michael Knight",
                    "cancelled": False,
                    "insts": ["BAR", "FOO"],
                },
                {
                    "id": "rjg21",
                    "email": "rjg21@gdev.apps.cam.ac.uk",
                    "display_name": "R. Goodall",
                    "cancelled": False,
                    "insts": ["BAZ", "TESTINST"],
                },
            ],
            None,
            "TESTINST",
        )

        # First doesn't have a Google account
        mock_directory.return_value.user_has_google_account.side_effect = [False, True, True]

        # Mock user usages and shared drive cache returned by drive connection
        mock_drive.return_value.get_storage_quota_usage.side_effect = [54321, 12345]
        mock_drive.return_value.get_shared_drive_cache.return_value = bytes(
            datafile("shared_drive_list.yaml"), "utf-8"
        )

        report(
            CONFIGURATION, ReportType.INSTITUTION, "testinst", cache_users=False, write_mode=True
        )

        # Report files to be written to local directory
        mock_write_report.assert_has_calls(
            [
                mock.call(
                    CONFIGURATION,
                    [
                        {
                            "id": "mk2155",
                            "display_name": "Michael Knight",
                            "cancelled": False,
                            "usage": 54321,
                            "insts": "BAR, FOO",
                        },
                        {
                            "id": "rjg21",
                            "display_name": "R. Goodall",
                            "cancelled": False,
                            "usage": 12345,
                            "insts": "BAZ, TESTINST",
                        },
                    ],
                    "TESTINST-mydrive-20230101-013000.csv",
                    None,
                    True,
                ),
                mock.call(
                    CONFIGURATION,
                    [
                        {
                            "id": "drive1",
                            "name": "Drive with all types of permissions",
                            "managers": "mk2155",
                            "content_managers": "",
                            "insts": "BAR, FOO",
                            "files": 22,
                            "usage": 123456,
                            "as_of": datetime(2023, 5, 15, 11, 4, 40, 661098, tzinfo=timezone.utc),
                        },
                        {
                            "id": "drive2",
                            "name": "Empty Drive",
                            "managers": "",
                            "content_managers": "mk2155",
                            "insts": "BAR, FOO",
                            "files": 0,
                            "usage": 0,
                            "as_of": "Unknown",
                        },
                        {
                            "id": "drive3",
                            "name": "Unscanned Drive",
                            "managers": "rjg21",
                            "content_managers": "",
                            "insts": "BAZ, TESTINST",
                            "files": None,
                            "usage": None,
                            "as_of": "Unknown",
                        },
                    ],
                    "TESTINST-shared-drive-20230101-013000.csv",
                    None,
                    True,
                ),
            ]
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.reporting.os.remove")
    @mock.patch("gdrivemanager.manage.reporting.write_report")
    @mock.patch("gdrivemanager.manage.reporting.get_users_for_report")
    @mock.patch("gdrivemanager.manage.reporting.GoogleDriveConnection")
    @mock.patch("gdrivemanager.manage.reporting.GoogleDirectoryConnection")
    def test_report_using_request_file_no_output_location(
        self, mock_directory, mock_drive, mock_get_users_for_report, mock_write_report, mock_remove
    ):
        # Get report using request file without a output_location set
        self.assertIsNone(CONFIGURATION.report.output_location)

        # Mock that report request file "report-request-inst-testinst" was found which lead to the
        # following users being returned. test_get_users_for_report() tests the inner logic
        mock_get_users_for_report.return_value = (
            [
                {
                    "id": "abc123",
                    "email": "abc123@gdev.apps.cam.ac.uk",
                    "display_name": "A. Cole",
                    "cancelled": True,
                    "insts": ["QUX"],
                },
                {
                    "id": "mk2155",
                    "email": "mk2155@gdev.apps.cam.ac.uk",
                    "display_name": "Michael Knight",
                    "cancelled": False,
                    "insts": ["BAR", "FOO"],
                },
                {
                    "id": "rjg21",
                    "email": "rjg21@gdev.apps.cam.ac.uk",
                    "display_name": "R. Goodall",
                    "cancelled": False,
                    "insts": ["BAZ", "TESTINST"],
                },
            ],
            (None, "report-request-inst-testinst"),
            "TESTINST",
        )

        # First doesn't have a Google account
        mock_directory.return_value.user_has_google_account.side_effect = [False, True, True]

        # Mock user usages and shared drive cache returned by drive connection
        mock_drive.return_value.get_storage_quota_usage.side_effect = [54321, 12345]
        mock_drive.return_value.get_shared_drive_cache.return_value = bytes(
            datafile("shared_drive_list.yaml"), "utf-8"
        )

        report(CONFIGURATION, ReportType.REQUEST, None, cache_users=False, write_mode=True)

        # Report files to be written to local directory
        mock_write_report.assert_has_calls(
            [
                mock.call(
                    CONFIGURATION,
                    [
                        {
                            "id": "mk2155",
                            "display_name": "Michael Knight",
                            "cancelled": False,
                            "usage": 54321,
                            "insts": "BAR, FOO",
                        },
                        {
                            "id": "rjg21",
                            "display_name": "R. Goodall",
                            "cancelled": False,
                            "usage": 12345,
                            "insts": "BAZ, TESTINST",
                        },
                    ],
                    "TESTINST-mydrive-20230101-013000.csv",
                    None,
                    True,
                ),
                mock.call(
                    CONFIGURATION,
                    [
                        {
                            "id": "drive1",
                            "name": "Drive with all types of permissions",
                            "managers": "mk2155",
                            "content_managers": "",
                            "insts": "BAR, FOO",
                            "files": 22,
                            "usage": 123456,
                            "as_of": datetime(2023, 5, 15, 11, 4, 40, 661098, tzinfo=timezone.utc),
                        },
                        {
                            "id": "drive2",
                            "name": "Empty Drive",
                            "managers": "",
                            "content_managers": "mk2155",
                            "insts": "BAR, FOO",
                            "files": 0,
                            "usage": 0,
                            "as_of": "Unknown",
                        },
                        {
                            "id": "drive3",
                            "name": "Unscanned Drive",
                            "managers": "rjg21",
                            "content_managers": "",
                            "insts": "BAZ, TESTINST",
                            "files": None,
                            "usage": None,
                            "as_of": "Unknown",
                        },
                    ],
                    "TESTINST-shared-drive-20230101-013000.csv",
                    None,
                    True,
                ),
            ]
        )

        # And request file deleted
        mock_remove.assert_called_once_with("report-request-inst-testinst")

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.reporting.write_report")
    @mock.patch("gdrivemanager.manage.reporting.get_users_for_report")
    @mock.patch("gdrivemanager.manage.reporting.GoogleDriveConnection")
    @mock.patch("gdrivemanager.manage.reporting.GoogleDirectoryConnection")
    def test_report_using_request_file_with_output_location(
        self, mock_directory, mock_drive, mock_get_users_for_report, mock_write_report
    ):
        # Get report using request file using an output_location
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.report.output_location = "drive1"

        # Mock that report request file "report-request-inst-testinst" was found which lead to the
        # following users being returned. test_get_users_for_report() tests the inner logic
        mock_get_users_for_report.return_value = (
            [
                {
                    "id": "abc123",
                    "email": "abc123@gdev.apps.cam.ac.uk",
                    "display_name": "A. Cole",
                    "cancelled": True,
                    "insts": ["QUX"],
                },
                {
                    "id": "mk2155",
                    "email": "mk2155@gdev.apps.cam.ac.uk",
                    "display_name": "Michael Knight",
                    "cancelled": False,
                    "insts": ["BAR", "FOO"],
                },
                {
                    "id": "rjg21",
                    "email": "rjg21@gdev.apps.cam.ac.uk",
                    "display_name": "R. Goodall",
                    "cancelled": False,
                    "insts": ["BAZ", "TESTINST"],
                },
            ],
            ("file1", "report-request-inst-testinst"),
            "TESTINST",
        )

        # First doesn't have a Google account
        mock_directory.return_value.user_has_google_account.side_effect = [False, True, True]

        # Mock user usages and shared drive cache returned by drive connection
        mock_drive.return_value.get_storage_quota_usage.side_effect = [54321, 12345]
        mock_drive.return_value.get_shared_drive_cache.return_value = bytes(
            datafile("shared_drive_list.yaml"), "utf-8"
        )

        report(temp_configuration, ReportType.REQUEST, None, cache_users=False, write_mode=True)

        # Report files to be written to local directory
        mock_write_report.assert_has_calls(
            [
                mock.call(
                    temp_configuration,
                    [
                        {
                            "id": "mk2155",
                            "display_name": "Michael Knight",
                            "cancelled": False,
                            "usage": 54321,
                            "insts": "BAR, FOO",
                        },
                        {
                            "id": "rjg21",
                            "display_name": "R. Goodall",
                            "cancelled": False,
                            "usage": 12345,
                            "insts": "BAZ, TESTINST",
                        },
                    ],
                    "TESTINST-mydrive-20230101-013000.csv",
                    mock_drive.return_value,
                    True,
                ),
                mock.call(
                    temp_configuration,
                    [
                        {
                            "id": "drive1",
                            "name": "Drive with all types of permissions",
                            "managers": "mk2155",
                            "content_managers": "",
                            "insts": "BAR, FOO",
                            "files": 22,
                            "usage": 123456,
                            "as_of": datetime(2023, 5, 15, 11, 4, 40, 661098, tzinfo=timezone.utc),
                        },
                        {
                            "id": "drive2",
                            "name": "Empty Drive",
                            "managers": "",
                            "content_managers": "mk2155",
                            "insts": "BAR, FOO",
                            "files": 0,
                            "usage": 0,
                            "as_of": "Unknown",
                        },
                        {
                            "id": "drive3",
                            "name": "Unscanned Drive",
                            "managers": "rjg21",
                            "content_managers": "",
                            "insts": "BAZ, TESTINST",
                            "files": None,
                            "usage": None,
                            "as_of": "Unknown",
                        },
                    ],
                    "TESTINST-shared-drive-20230101-013000.csv",
                    mock_drive.return_value,
                    True,
                ),
            ]
        )

        # And request file deleted
        mock_drive.return_value.trash_file.assert_called_once_with(
            "file1", "report-request-inst-testinst"
        )

    @mock.patch("gdrivemanager.manage.reporting.find_report_request_file")
    @mock.patch("gdrivemanager.manage.reporting.LookupConnection")
    def test_get_users_for_report(self, mock_lookup, mock_find_report):
        TEST_USERS = [
            {
                "id": "abc123",
                "email": "abc123@gdev.apps.cam.ac.uk",
                "display_name": "A. Cole",
                "cancelled": False,
                "insts": ["FOO", "TESTINST"],
            },
            {
                "id": "uvw456",
                "email": "uvw456@gdev.apps.cam.ac.uk",
                "display_name": "U. Wheeler",
                "cancelled": False,
                "insts": ["FOO"],
            },
            {
                "id": "xyz789",
                "email": "xyz789@gdev.apps.cam.ac.uk",
                "display_name": "X. Zebra",
                "cancelled": True,
                "insts": ["BAR", "FOO"],
            },
        ]
        TEST_USERS_NO_EMAIL = [
            {k: v for k, v in user.items() if k != "email"} for user in TEST_USERS
        ]

        # Test get_users_for_report() with a user

        # single user returned from lookup
        mock_lookup.return_value.retrieve_user_by_crsid.return_value = TEST_USERS_NO_EMAIL[0]

        # test directly specifying a user (even in uppercase)
        users, file, entity_id = get_users_for_report(
            CONFIGURATION, ReportType.USER, "ABC123", False, None
        )
        self.assertEqual(users, TEST_USERS[:1])
        self.assertEqual(file, None)
        self.assertEqual(entity_id, "abc123")

        # test via report-request-user-* file (even with entity id in uppercase)
        mock_find_report.return_value = (
            ReportType.USER,
            "ABC123",
            None,
            "report-request-user-ABC123",
        )

        users, file, entity_id = get_users_for_report(
            CONFIGURATION, ReportType.REQUEST, None, False, None
        )
        self.assertEqual(users, TEST_USERS[:1])
        self.assertEqual(file, (None, "report-request-user-ABC123"))
        self.assertEqual(entity_id, "abc123")

        # Test get_users_for_report() with a group

        # first two test users returned from lookup
        mock_lookup.return_value.retrieve_users_by_group.return_value = TEST_USERS_NO_EMAIL[:2]

        # test directly specifying a group (even in uppercase)
        users, file, entity_id = get_users_for_report(
            CONFIGURATION, ReportType.GROUP, "GROUP1", False, None
        )
        self.assertEqual(users, TEST_USERS[:2])
        self.assertEqual(file, None)
        self.assertEqual(entity_id, "group1")

        # test via report-request-group-* file (even with entity id in uppercase)
        mock_find_report.return_value = (
            ReportType.GROUP,
            "GROUP1",
            None,
            "report-request-group-GROUP1",
        )

        users, file, entity_id = get_users_for_report(
            CONFIGURATION, ReportType.REQUEST, None, False, None
        )
        self.assertEqual(users, TEST_USERS[:2])
        self.assertEqual(file, (None, "report-request-group-GROUP1"))
        self.assertEqual(entity_id, "group1")

        # Test get_users_for_report() with an institution

        # all test users returned from lookup but as dict keyed on id
        mock_lookup.return_value.retrieve_users_by_instid.return_value = {
            user["id"]: user for user in TEST_USERS_NO_EMAIL
        }

        # test directly specifying an institution (even in lowercase)
        users, file, entity_id = get_users_for_report(
            CONFIGURATION, ReportType.INSTITUTION, "testinst", False, None
        )
        self.assertEqual(users, TEST_USERS)
        self.assertEqual(file, None)
        self.assertEqual(entity_id, "TESTINST")

        # test via report-request-inst-* file (even with entity id in lowercase)
        mock_find_report.return_value = (
            ReportType.INSTITUTION,
            "testinst",
            None,
            "report-request-inst-testinst",
        )

        users, file, entity_id = get_users_for_report(
            CONFIGURATION, ReportType.REQUEST, None, False, None
        )
        self.assertEqual(users, TEST_USERS)
        self.assertEqual(file, (None, "report-request-inst-testinst"))
        self.assertEqual(entity_id, "TESTINST")

    @mock.patch("gdrivemanager.manage.reporting.GoogleDirectoryConnection")
    def test_filter_users_by_google_account(self, mock_directory):
        # test users with and without Google accounts
        users = [
            {"id": "1", "email": "abc123@gdev.apps.cam.ac.uk", "_has_account": True},
            {"id": "2", "email": "xyz789@gdev.apps.cam.ac.uk", "_has_account": False},
            {"id": "3", "email": "uvz987@gdev.apps.cam.ac.uk", "_has_account": True},
        ]
        mock_directory.return_value.user_has_google_account.side_effect = [
            user["_has_account"] for user in users
        ]
        mock_directory.return_value.retrieve_users.return_value = [
            {
                "id": user["id"],
                "primaryEmail": user["email"],
            }
            for user in users
            if user["_has_account"]
        ]

        # filter called with no more than the limit of users, calls user_has_google_account once
        # per user and not retrieve_users at all
        filtered_users = filter_users_by_google_account(
            CONFIGURATION, mock_directory.return_value, users
        )
        mock_directory.return_value.user_has_google_account.has_calls(
            [mock.call(users[idx]["email"]) for idx in range(len(users))]
        )
        mock_directory.return_value.retrieve_users.assert_not_called()
        self.assertEqual(filtered_users, [user for user in users if user["_has_account"]])

        mock_directory.return_value.user_has_google_account.reset_mock()

        # filter called with more than the limit of users, calls retrieve_users once for all users
        # and not user_has_google_account at all
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.report.google_account_individual_check_limit = 2
        filtered_users = filter_users_by_google_account(
            temp_configuration, mock_directory.return_value, users
        )
        mock_directory.return_value.retrieve_users.assert_called_once_with(suspended_only=False)
        mock_directory.return_value.user_has_google_account.assert_not_called()
        self.assertEqual(filtered_users, [user for user in users if user["_has_account"]])

    @mock.patch("gdrivemanager.manage.reporting.read_csv")
    def test_get_user_usage_cache(self, mock_read):
        # Test with no cache file present
        mock_read.side_effect = FileNotFoundError
        users = get_user_usage_cache(CONFIGURATION)
        self.assertEqual(users, {})

        # Test with cache file present
        mock_read.side_effect = None
        mock_read.return_value = [
            {"id": "abc123", "usage": 12345},
            {"id": "xyz789", "usage": 67890},
        ]
        users = get_user_usage_cache(CONFIGURATION)
        self.assertEqual(users, {"abc123": 12345, "xyz789": 67890})

    @mock.patch("gdrivemanager.manage.reporting.write_csv")
    def test_update_user_usage_cache(self, mock_write):
        existing_cache = {"abc123": 12345, "xyz789": 67890}
        updated_data = [{"id": "xyz789", "usage": 99999}, {"id": "uvw456", "usage": 54321}]

        update_user_usage_cache(CONFIGURATION, existing_cache, updated_data, write_mode=True)

        expected_write = [
            {"id": "abc123", "usage": 12345},
            {"id": "xyz789", "usage": 99999},
            {"id": "uvw456", "usage": 54321},
        ]
        mock_write.assert_called_once_with(
            expected_write, CONFIGURATION.report.mydrive_user_usage_cache
        )

    @mock.patch("gdrivemanager.manage.reporting.os.scandir")
    def test_find_report_request_file_locally(self, mock_scandir):
        # Look for a report request file locally
        self.assertIsNone(CONFIGURATION.report.output_location)

        # Test with no local matching files
        mock_scandir.return_value.__enter__ = mock.Mock(return_value=[])
        report_type, id, file_id, file_name = find_report_request_file(CONFIGURATION, None)
        self.assertIsNone(report_type)

        # Test with matching and unmatching files found, first match is used
        mock_scandir.return_value.__enter__ = mock.Mock(
            return_value=[
                Mock_DirEntry("subfolder", is_dir=True),
                Mock_DirEntry("not-a-request"),
                Mock_DirEntry("report-request-badtype-abc123"),
                Mock_DirEntry("report-request-user-abc123"),
                Mock_DirEntry("report-request-inst-FOO"),
            ]
        )
        report_type, id, file_id, file_name = find_report_request_file(CONFIGURATION, None)
        self.assertEqual(report_type, ReportType.USER)
        self.assertEqual(id, "abc123")
        self.assertEqual(file_id, None)
        self.assertEqual(file_name, "report-request-user-abc123")

    @mock.patch("gdrivemanager.manage.reporting.GoogleDriveConnection")
    def test_find_report_request_file_in_drive(self, mock_drive):
        # Set output location so drive connection used
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.report.output_location = "driveid"

        # Test with no matching files found
        mock_drive.return_value.search_for_files.return_value = []
        report_type, id, file_id, file_name = find_report_request_file(
            temp_configuration, mock_drive.return_value
        )
        self.assertIsNone(report_type)

        # Test when single matching file found (user in this case)
        mock_drive.return_value.search_for_files.return_value = [
            {
                "id": "f1",
                "name": "report-request-user-abc123",
            }
        ]
        report_type, id, file_id, file_name = find_report_request_file(
            temp_configuration, mock_drive.return_value
        )
        self.assertEqual(report_type, ReportType.USER)
        self.assertEqual(id, "abc123")
        self.assertEqual(file_id, "f1")
        self.assertEqual(file_name, "report-request-user-abc123")

        # Test when multiple matching files found, first is used (institution in this case)
        mock_drive.return_value.search_for_files.return_value = [
            {
                "id": "f1",
                "name": "report-request-inst-FOO",
            },
            {
                "id": "f2",
                "name": "report-request-inst-BAR",
            },
        ]
        report_type, id, file_id, file_name = find_report_request_file(
            temp_configuration, mock_drive.return_value
        )
        self.assertEqual(report_type, ReportType.INSTITUTION)
        self.assertEqual(id, "foo")
        self.assertEqual(file_id, "f1")
        self.assertEqual(file_name, "report-request-inst-FOO")

        # Test when multiple matching files found, but first is bad
        mock_drive.return_value.search_for_files.return_value = [
            {
                "id": "f1",
                "name": "report-request-badtype-abc123",
            },
            {
                "id": "f2",
                "name": "report-request-inst-BAR",
            },
        ]
        report_type, id, file_id, file_name = find_report_request_file(
            temp_configuration, mock_drive.return_value
        )
        self.assertEqual(report_type, ReportType.INSTITUTION)
        self.assertEqual(id, "bar")
        self.assertEqual(file_id, "f2")
        self.assertEqual(file_name, "report-request-inst-BAR")

        # Test when multiple matching files found, but none are good
        mock_drive.return_value.search_for_files.return_value = [
            {
                "id": "f1",
                "name": "report-request-badtype-abc123",
            },
            {
                "id": "f2",
                "name": "report-request-too-many-hyphens",
            },
        ]
        report_type, id, file_id, file_name = find_report_request_file(
            temp_configuration, mock_drive.return_value
        )
        self.assertIsNone(report_type)

        # Test missing 'inst' type, is assumed
        mock_drive.return_value.search_for_files.return_value = [
            {
                "id": "f1",
                "name": "report-request-foo",
            }
        ]
        report_type, id, file_id, file_name = find_report_request_file(
            temp_configuration, mock_drive.return_value
        )
        self.assertEqual(report_type, ReportType.INSTITUTION)
        self.assertEqual(id, "foo")
        self.assertEqual(file_id, "f1")
        self.assertEqual(file_name, "report-request-foo")

        # Test group without hyphen
        mock_drive.return_value.search_for_files.return_value = [
            {
                "id": "f1",
                "name": "report-request-group-001234",
            }
        ]
        report_type, id, file_id, file_name = find_report_request_file(
            temp_configuration, mock_drive.return_value
        )
        self.assertEqual(report_type, ReportType.GROUP)
        self.assertEqual(id, "001234")
        self.assertEqual(file_id, "f1")
        self.assertEqual(file_name, "report-request-group-001234")

        # Test group with hyphen
        mock_drive.return_value.search_for_files.return_value = [
            {
                "id": "f1",
                "name": "report-request-group-uis-foobar",
            }
        ]
        report_type, id, file_id, file_name = find_report_request_file(
            temp_configuration, mock_drive.return_value
        )
        self.assertEqual(report_type, ReportType.GROUP)
        self.assertEqual(id, "uis-foobar")
        self.assertEqual(file_id, "f1")
        self.assertEqual(file_name, "report-request-group-uis-foobar")

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.reporting.GoogleDriveConnection")
    @mock.patch("gdrivemanager.manage.reporting.write_csv")
    def test_write_report(self, mock_write_csv, mock_drive):
        TEST_DATA = [
            {
                "field1": "value1",
                "field2": "value2",
            },
            {
                "field1": "value3",
                "field2": "value4",
            },
        ]
        TEST_FILENAME = "test.csv"

        # Test writing a report locally (read-only mode)
        write_report(CONFIGURATION, TEST_DATA, TEST_FILENAME, None, False)
        mock_write_csv.assert_not_called()

        # Test writing a report locally (write mode)
        write_report(CONFIGURATION, TEST_DATA, TEST_FILENAME, None, True)
        mock_write_csv.assert_called_once_with(TEST_DATA, TEST_FILENAME)

        # Test writing a report to a Google Drive location (read-only mode)
        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.report.output_location = "drive1"

        write_report(temp_configuration, TEST_DATA, TEST_FILENAME, mock_drive.return_value, False)
        mock_drive.return_value.write_file.assert_not_called()

        # Test writing a report to a Google Drive location (write mode)
        write_report(temp_configuration, TEST_DATA, TEST_FILENAME, mock_drive.return_value, True)
        mock_drive.return_value.write_file.assert_called_once_with(
            TEST_FILENAME, generate_csv(TEST_DATA), mimetype="text/csv", parents=["drive1"]
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.reporting.GoogleDriveConnection")
    @mock.patch("gdrivemanager.manage.reporting.LookupConnection")
    @mock.patch("gdrivemanager.manage.reporting.write_report")
    def test_shared_drive_report(self, mock_write_report, mock_lookup, mock_drive):
        # Use the shared drive list fixture
        mock_drive.return_value.get_shared_drive_cache.return_value = bytes(
            datafile("shared_drive_list.yaml"), "utf-8"
        )

        # Lookup returning a mixture of cancelled and active users
        mock_lookup.return_value.retrieve_users_from_list.return_value = [
            {
                "id": "amc203",
                "cancelled": False,
                "insts": ["FOO"],
            },
            {
                "id": "mk2155",
                "cancelled": True,
                "insts": ["BAR", "FOO"],
            },
            {
                "id": "abc123",
                "cancelled": False,
                "insts": ["BAR"],
            },
            {
                "id": "xyz123",
                "cancelled": True,
                "insts": ["BAZ", "FOO"],
            },
            # uvw321 intentionally missing from Lookup
            {
                "id": "rjg21",
                "cancelled": False,
                "insts": ["FOO", "QUX"],
            },
        ]

        shared_drive_report(CONFIGURATION, True)

        mock_drive.return_value.get_shared_drive_cache.assert_called_once()

        mock_lookup.return_value.retrieve_users_from_list.assert_called_once_with(
            {"amc203", "mk2155", "abc123", "xyz123", "uvw321", "rjg21"}
        )

        expected_write = [
            {
                "id": "drive1",
                "name": "Drive with all types of permissions",
                "active_cam_managers": "123456@groups.lookup.cam.ac.uk, amc203",
                "active_cam_others": "789012@groups.lookup.cam.ac.uk [R], abc123 [R]",
                "cancelled_cam_users": "mk2155 [M], uvw321 [R], xyz123 [W]",
                "other_users": "somebody@gmail.com [M]",
                "augmented_users": "",
                "files": 22,
                "usage": 123456,
                "as_of": "2023-05-15 11:04:40.661098+00:00",
                "institutions": "BAR, BAZ, FOO",
                "created": "2023-05-01 10:30:00.123456+00:00",
                "orgUnitId": "abc123",
                "last_removed": "Unknown",
                "last_restored": "Unknown",
            },
            {
                "id": "drive2",
                "name": "Empty Drive",
                "active_cam_managers": "",
                "active_cam_others": "",
                "cancelled_cam_users": "mk2155 [M]",
                "other_users": "",
                "augmented_users": "",
                "files": 0,
                "usage": 0,
                "as_of": "Unknown",
                "institutions": "BAR, FOO",
                "created": "Unknown",
                "orgUnitId": "Unknown",
                "last_removed": "Unknown",
                "last_restored": "Unknown",
            },
            # drive4 before drive3 as after usage sorting is by name
            {
                "id": "drive4",
                "name": "Drive with no managers",
                "active_cam_managers": "",
                "active_cam_others": "rjg21 [R]",
                "cancelled_cam_users": "",
                "other_users": "",
                "augmented_users": "",
                "files": "Unknown",
                "usage": "Unknown",
                "as_of": "Unknown",
                "institutions": "FOO, QUX",
                "created": "Unknown",
                "orgUnitId": "Unknown",
                "last_removed": "Unknown",
                "last_restored": "Unknown",
            },
            {
                "id": "drive3",
                "name": "Unscanned Drive",
                "active_cam_managers": "rjg21",
                "active_cam_others": "",
                "cancelled_cam_users": "",
                "other_users": "",
                "augmented_users": "anyoneWithLink [W], mk2155 [R]",
                "files": "Unknown",
                "usage": "Unknown",
                "as_of": "Unknown",
                "institutions": "FOO, QUX",
                "created": "Unknown",
                "orgUnitId": "Unknown",
                "last_removed": "Unknown",
                "last_restored": "Unknown",
            },
        ]
        mock_write_report.assert_called_once_with(
            CONFIGURATION,
            expected_write,
            "shared-drive-usage-20230101-013000.csv",
            None,
            True,
        )
