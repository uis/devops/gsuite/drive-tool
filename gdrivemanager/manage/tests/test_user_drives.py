import json
import logging
from copy import deepcopy
from datetime import datetime, timezone
from unittest import TestCase, mock

import pytest
import yaml
from freezegun import freeze_time

from gdrivemanager.gapi import ListTimeoutError
from gdrivemanager.manage.user_drives import (
    matching_permissions,
    recover,
    rescan_permissions,
    reset_manual_actions,
    scan,
)
from gdrivemanager.tests import CONFIGURATION, datafile
from gdrivemanager.utils import FOLDER_MIMETYPE


class ManageUserDrivesTests(TestCase):
    @pytest.fixture(autouse=True)
    def inject_caplog(self, caplog):
        self._caplog = caplog

    frozen_time = datetime(2023, 1, 1, 1, 30, tzinfo=timezone.utc)

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_scan_folders(self, mock_drive, mock_directory):
        mock_directory.return_value.retrieve_users.return_value = json.loads(
            datafile("directory-ordinary-response.json")
        )["users"]

        mock_drive.return_value.get_all_owned_files_metadata.side_effect = [
            json.loads(datafile("drive-ordinary-response-folders.json"))["files"],
            json.loads(datafile("drive-no-shared-response.json"))["files"],
        ]

        mock_drive.return_value.get_file_contents_by_name.side_effect = [
            (None, None),
            (None, None),
        ]

        scan(CONFIGURATION, write_mode=True)

        # Construct directory object once
        mock_directory.assert_called_once_with(CONFIGURATION, write_mode=True)
        # Retrieve users called
        mock_directory.return_value.retrieve_users.assert_called_once()
        # Construct shared drive connection, and the two users to scan (id 2 & 3) drive connection
        mock_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=True),
                mock.call(CONFIGURATION, "2", write_mode=True),
                mock.call(CONFIGURATION, "3", write_mode=True),
            ],
            any_order=True,
        )
        # For each user retrieve the list of folders only, then the list of all files.
        mock_drive.return_value.get_all_owned_files_metadata.assert_has_calls(
            [
                mock.call(query=f"mimeType = '{FOLDER_MIMETYPE}'"),
                mock.call(query=f"mimeType = '{FOLDER_MIMETYPE}'"),
            ]
        )
        mock_drive.return_value.get_file_contents_by_name.assert_has_calls(
            [
                mock.call("2", drive_id=CONFIGURATION.google.shared_storage_drive),
                mock.call("3", drive_id=CONFIGURATION.google.shared_storage_drive),
            ]
        )
        # Remove permissions from folders, then any remaining permissions.
        mock_drive.return_value.batch_remove_permissions.assert_has_calls(
            [
                # Folder permission p1 removed, also removes p1 from f4
                mock.call({"f5": ["p1"]}),
            ]
        )
        # Write file twice, once before removing folder permissions, then an updated
        # version when removing all other file permissions.
        mock_drive.return_value.write_file.assert_has_calls(
            [
                mock.call(
                    "bt234-2-shared-permissions-20230101-013000.yaml",
                    datafile("permissions-doc-folder.yaml"),
                    mimetype="text/yaml",
                    parents=[CONFIGURATION.google.shared_storage_drive],
                ),
            ]
        )
        # Update the ucam fields for each user.
        mock_directory.return_value.update_user_ucam_fields.assert_has_calls(
            [
                mock.call(
                    "2",
                    {
                        "mydrive-shared-result": "",
                        "mydrive-shared-action": "scan-files",
                        "mydrive-shared-filecount": 1,
                        "mydrive-shared-doc": "bt234-2-shared-permissions-20230101-013000.yaml",
                    },
                ),
                mock.call(
                    "3",
                    {
                        "mydrive-shared-result": "",
                        "mydrive-shared-action": "scan-files",
                        "mydrive-shared-filecount": 0,
                        "mydrive-shared-doc": "",
                    },
                ),
            ],
            any_order=True,
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_scan_folders_historic_document(self, mock_drive, mock_directory):
        mock_directory.return_value.retrieve_users.return_value = json.loads(
            datafile("directory-ordinary-response.json")
        )["users"]

        mock_drive.return_value.get_all_owned_files_metadata.side_effect = [
            json.loads(datafile("drive-ordinary-response-folders.json"))["files"],
            json.loads(datafile("drive-no-shared-response.json"))["files"],
        ]

        mock_drive.return_value.get_file_contents_by_name.side_effect = [
            ("HISTORIC-permissions-document.yaml", None),
            ("HISTORIC-permissions-document.yaml", None),
        ]

        scan(CONFIGURATION, write_mode=True)

        # Construct directory object once
        mock_directory.assert_called_once_with(CONFIGURATION, write_mode=True)
        # Retrieve users called
        mock_directory.return_value.retrieve_users.assert_called_once()
        # Construct shared drive connection, and the two users to scan (id 2 & 3) drive connection
        mock_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=True),
                mock.call(CONFIGURATION, "2", write_mode=True),
                mock.call(CONFIGURATION, "3", write_mode=True),
            ],
            any_order=True,
        )
        # For each user retrieve the list of folders only, then the list of all files.
        mock_drive.return_value.get_all_owned_files_metadata.assert_has_calls(
            [
                mock.call(query=f"mimeType = '{FOLDER_MIMETYPE}'"),
                mock.call(query=f"mimeType = '{FOLDER_MIMETYPE}'"),
            ]
        )
        mock_drive.return_value.get_file_contents_by_name.assert_has_calls(
            [
                mock.call("2", drive_id=CONFIGURATION.google.shared_storage_drive),
                mock.call("3", drive_id=CONFIGURATION.google.shared_storage_drive),
            ]
        )
        # Remove permissions from folders, then any remaining permissions.
        mock_drive.return_value.batch_remove_permissions.assert_has_calls(
            [
                # Remove permission p1 from folder f5.
                mock.call({"f5": ["p1"]}),
            ]
        )
        # Write file twice, once before removing folder permissions, then an updated
        # version when removing all other file permissions.
        mock_drive.return_value.write_file.assert_has_calls(
            [
                mock.call(
                    "bt234-2-shared-permissions-20230101-013000.yaml",
                    datafile("permissions-doc-folder.yaml"),
                    mimetype="text/yaml",
                    parents=[CONFIGURATION.google.shared_storage_drive],
                ),
            ]
        )
        # Update the ucam fields for each user.
        mock_directory.return_value.update_user_ucam_fields.assert_has_calls(
            [
                mock.call(
                    "2",
                    {
                        "mydrive-shared-result": "",
                        "mydrive-shared-action": "scan-files",
                        "mydrive-shared-filecount": 1,
                        "mydrive-shared-doc": "bt234-2-shared-permissions-20230101-013000.yaml",
                    },
                ),
                mock.call(
                    "3",
                    {
                        "mydrive-shared-result": "",
                        "mydrive-shared-action": "scan-files",
                        "mydrive-shared-filecount": 0,
                        "mydrive-shared-doc": "",
                    },
                ),
            ],
            any_order=True,
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_scan_folders_with_existing_document(self, mock_drive, mock_directory):
        mock_directory.return_value.retrieve_users.return_value = json.loads(
            datafile("directory-ordinary-response.json")
        )["users"]

        mock_drive.return_value.get_all_owned_files_metadata.side_effect = [
            json.loads(datafile("drive-ordinary-response-folders.json"))["files"],
            json.loads(datafile("drive-no-shared-response.json"))["files"],
        ]

        mock_drive.return_value.get_file_contents_by_name.side_effect = [
            (
                "existing-shared-files-1.yaml",
                bytes(datafile("permissions-doc-partial.yaml"), "utf-8"),
            ),
            (
                "existing-shared-files-2.yaml",
                bytes(datafile("permissions-doc-partial.yaml"), "utf-8"),
            ),
        ]

        scan(CONFIGURATION, write_mode=True)

        # Construct directory object once
        mock_directory.assert_called_once_with(CONFIGURATION, write_mode=True)
        # Retrieve users called
        mock_directory.return_value.retrieve_users.assert_called_once()
        # Construct shared drive connection, and the two users to scan (id 2 & 3) drive connection
        mock_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=True),
                mock.call(CONFIGURATION, "2", write_mode=True),
                mock.call(CONFIGURATION, "3", write_mode=True),
            ],
            any_order=True,
        )
        # For each user retrieve the list of folders only, then the list of all files.
        mock_drive.return_value.get_all_owned_files_metadata.assert_has_calls(
            [
                mock.call(query=f"mimeType = '{FOLDER_MIMETYPE}'"),
                mock.call(query=f"mimeType = '{FOLDER_MIMETYPE}'"),
            ]
        )
        mock_drive.return_value.get_file_contents_by_name.assert_has_calls(
            [
                mock.call("2", drive_id=CONFIGURATION.google.shared_storage_drive),
                mock.call("3", drive_id=CONFIGURATION.google.shared_storage_drive),
            ]
        )
        # Remove permissions from folders, then any remaining permissions.
        mock_drive.return_value.batch_remove_permissions.assert_has_calls(
            [
                # Folder permission p1 removed, also removes p1 from f4
                mock.call({"f5": ["p1"]}),
            ]
        )
        mock_drive.return_value.write_file.assert_has_calls(
            [
                # We only expect shared-permissions-1.yaml to be re-written, as no updates for
                # shared-permissions-2.yaml
                mock.call(
                    "existing-shared-files-1.yaml",
                    datafile("permissions-doc-merged.yaml"),
                    mimetype="text/yaml",
                    parents=[CONFIGURATION.google.shared_storage_drive],
                ),
            ]
        )
        # Update the ucam fields for each user.
        mock_directory.return_value.update_user_ucam_fields.assert_has_calls(
            [
                mock.call(
                    "2",
                    {
                        "mydrive-shared-result": "",
                        "mydrive-shared-action": "scan-files",
                        "mydrive-shared-filecount": 2,
                        "mydrive-shared-doc": "existing-shared-files-1.yaml",
                    },
                ),
                mock.call(
                    "3",
                    {
                        "mydrive-shared-result": "",
                        "mydrive-shared-action": "scan-files",
                        "mydrive-shared-filecount": 2,
                        "mydrive-shared-doc": "existing-shared-files-2.yaml",
                    },
                ),
            ],
            any_order=True,
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_scan_folders_with_invalid_document(self, mock_drive, mock_directory):
        mock_directory.return_value.retrieve_users.return_value = json.loads(
            datafile("directory-ordinary-response.json")
        )["users"]

        mock_drive.return_value.get_all_owned_files_metadata.side_effect = [
            json.loads(datafile("drive-ordinary-response-folders.json"))["files"],
            json.loads(datafile("drive-no-shared-response.json"))["files"],
        ]

        mock_drive.return_value.get_file_contents_by_name.side_effect = [
            (
                "existing-shared-files-1.yaml",
                bytes(datafile("invalid.yaml"), "utf-8"),
            ),
            (
                "existing-shared-files-2.yaml",
                bytes(datafile("permissions-doc-partial.yaml"), "utf-8"),
            ),
        ]

        scan(CONFIGURATION, write_mode=True)

        # Construct directory object once
        mock_directory.assert_called_once_with(CONFIGURATION, write_mode=True)
        # Retrieve users called
        mock_directory.return_value.retrieve_users.assert_called_once()
        # Construct shared drive connection, and the two users to scan (id 2 & 3) drive connection
        mock_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=True),
                mock.call(CONFIGURATION, "2", write_mode=True),
                mock.call(CONFIGURATION, "3", write_mode=True),
            ],
            any_order=True,
        )
        # For each user retrieve the list of folders only, then the list of all files.
        mock_drive.return_value.get_all_owned_files_metadata.assert_has_calls(
            [
                mock.call(query=f"mimeType = '{FOLDER_MIMETYPE}'"),
                mock.call(query=f"mimeType = '{FOLDER_MIMETYPE}'"),
            ]
        )
        mock_drive.return_value.get_file_contents_by_name.assert_has_calls(
            [
                mock.call("2", drive_id=CONFIGURATION.google.shared_storage_drive),
                mock.call("3", drive_id=CONFIGURATION.google.shared_storage_drive),
            ]
        )
        # Ensure user with invalid yaml document was skipped.
        mock_drive.return_value.batch_remove_permissions.assert_not_called()
        mock_drive.return_value.write_file.assert_not_called()
        # Update the ucam fields for each user.
        mock_directory.return_value.update_user_ucam_fields.assert_has_calls(
            [
                mock.call(
                    "3",
                    {
                        "mydrive-shared-result": "",
                        "mydrive-shared-action": "scan-files",
                        "mydrive-shared-filecount": 2,
                        "mydrive-shared-doc": "existing-shared-files-2.yaml",
                    },
                ),
            ],
            any_order=True,
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_scan_files_no_existing_permissions(self, mock_drive, mock_directory):
        mock_directory.return_value.retrieve_users.return_value = json.loads(
            datafile("directory-folders-scanned-no-shares-response.json")
        )["users"]

        mock_drive.return_value.get_all_owned_files_metadata.side_effect = [
            json.loads(datafile("drive-ordinary-response-cleaned-folders.json"))["files"],
            json.loads(datafile("drive-no-shared-response.json"))["files"],
        ]

        mock_drive.return_value.get_file_contents_by_name.side_effect = [
            (None, None),
            (None, None),
        ]

        scan(CONFIGURATION, write_mode=True)

        # Construct directory object once
        mock_directory.assert_called_once_with(CONFIGURATION, write_mode=True)
        # Retrieve users called
        mock_directory.return_value.retrieve_users.assert_called_once()
        # Construct shared drive connection, and the two users to scan (id 2 & 3) drive connection
        mock_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=True),
                mock.call(CONFIGURATION, "2", write_mode=True),
                mock.call(CONFIGURATION, "3", write_mode=True),
            ],
            any_order=True,
        )
        # For each user retrieve the list of folders only, then the list of all files.
        mock_drive.return_value.get_all_owned_files_metadata.assert_has_calls(
            [
                mock.call(),
                mock.call(),
            ]
        )
        mock_drive.return_value.get_file_contents_by_name.assert_has_calls(
            [
                mock.call("2", drive_id=CONFIGURATION.google.shared_storage_drive),
                mock.call("3", drive_id=CONFIGURATION.google.shared_storage_drive),
            ]
        )
        # Remove permissions from folders, then any remaining permissions.
        mock_drive.return_value.batch_remove_permissions.assert_has_calls(
            [
                mock.call({"f1": ["p1"], "f4": ["p3"]}),
            ]
        )
        mock_drive.return_value.write_file.assert_has_calls(
            [
                mock.call(
                    "bt234-2-shared-permissions-20230101-013000.yaml",
                    datafile("permissions-doc-file.yaml"),
                    mimetype="text/yaml",
                    parents=[CONFIGURATION.google.shared_storage_drive],
                ),
            ]
        )
        # Update the ucam fields for each user.
        mock_directory.return_value.update_user_ucam_fields.assert_has_calls(
            [
                mock.call(
                    "2",
                    {
                        "mydrive-shared-result": "permissions-removed",
                        "mydrive-shared-action": "",
                        "mydrive-shared-filecount": 2,
                        "mydrive-shared-doc": "bt234-2-shared-permissions-20230101-013000.yaml",
                    },
                ),
                mock.call(
                    "3",
                    {
                        "mydrive-shared-result": "permissions-none",
                        "mydrive-shared-action": "",
                        "mydrive-shared-filecount": 0,
                        "mydrive-shared-doc": "",
                    },
                ),
            ],
            any_order=True,
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_scan_files_with_existing_permissions(self, mock_drive, mock_directory):
        mock_directory.return_value.retrieve_users.return_value = json.loads(
            datafile("directory-folders-scanned-response.json")
        )["users"]

        mock_drive.return_value.get_all_owned_files_metadata.side_effect = [
            json.loads(datafile("drive-ordinary-response-cleaned-folders.json"))["files"],
            json.loads(datafile("drive-no-shared-response.json"))["files"],
        ]

        mock_drive.return_value.get_file_contents_by_name.side_effect = [
            (
                "existing-shared-files-1.yaml",
                bytes(datafile("permissions-doc-folder.yaml"), "utf-8"),
            ),
            (
                "existing-shared-files-2.yaml",
                bytes(datafile("permissions-doc-folder.yaml"), "utf-8"),
            ),
        ]

        scan(CONFIGURATION, write_mode=True)

        # Construct directory object once
        mock_directory.assert_called_once_with(CONFIGURATION, write_mode=True)
        # Retrieve users called
        mock_directory.return_value.retrieve_users.assert_called_once()
        # Construct shared drive connection, and the two users to scan (id 2 & 3) drive connection
        mock_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=True),
                mock.call(CONFIGURATION, "2", write_mode=True),
                mock.call(CONFIGURATION, "3", write_mode=True),
            ],
            any_order=True,
        )
        # For each user retrieve the list of folders only, then the list of all files.
        mock_drive.return_value.get_all_owned_files_metadata.assert_has_calls(
            [
                mock.call(),
                mock.call(),
            ]
        )
        mock_drive.return_value.get_file_contents_by_name.assert_has_calls(
            [
                mock.call("2", drive_id=CONFIGURATION.google.shared_storage_drive),
                mock.call("3", drive_id=CONFIGURATION.google.shared_storage_drive),
            ]
        )
        # Remove permissions from folders, then any remaining permissions.
        mock_drive.return_value.batch_remove_permissions.assert_has_calls(
            [
                mock.call({"f1": ["p1"], "f4": ["p3"]}),
            ]
        )
        mock_drive.return_value.write_file.assert_has_calls(
            [
                mock.call(
                    "existing-shared-files-1.yaml",
                    datafile("permissions-doc.yaml"),
                    mimetype="text/yaml",
                    parents=[CONFIGURATION.google.shared_storage_drive],
                ),
            ]
        )
        # Update the ucam fields for each user.
        mock_directory.return_value.update_user_ucam_fields.assert_has_calls(
            [
                mock.call(
                    "2",
                    {
                        "mydrive-shared-result": "permissions-removed",
                        "mydrive-shared-action": "",
                        "mydrive-shared-filecount": 3,
                        "mydrive-shared-doc": "existing-shared-files-1.yaml",
                    },
                ),
                mock.call(
                    "3",
                    {
                        "mydrive-shared-result": "permissions-removed",
                        "mydrive-shared-action": "",
                        "mydrive-shared-filecount": 1,
                        "mydrive-shared-doc": "existing-shared-files-2.yaml",
                    },
                ),
            ],
            any_order=True,
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_max_users_exceeded_scan(self, mock_drive, mock_directory):
        mock_directory.return_value.retrieve_users.return_value = json.loads(
            datafile("directory-ordinary-response.json")
        )["users"]

        mock_drive.return_value.get_all_owned_files_metadata.side_effect = [
            json.loads(datafile("drive-ordinary-response-folders.json"))["files"],
            json.loads(datafile("drive-ordinary-response-cleaned-folders.json"))["files"],
            json.loads(datafile("drive-no-shared-response.json"))["files"],
            json.loads(datafile("drive-no-shared-response.json"))["files"],
        ]

        mock_drive.return_value.get_file_contents_by_name.side_effect = [(None, None)]

        temp_configuration = deepcopy(CONFIGURATION)
        temp_configuration.limits.max_scanned_users = 1
        scan(temp_configuration, write_mode=True)

        # Construct directory object once
        mock_directory.assert_called_once_with(temp_configuration, write_mode=True)
        # Retrieve users called
        mock_directory.return_value.retrieve_users.assert_called_once()
        # Construct shared drive connection, and the single user to scan (id 2) drive connection
        mock_drive.assert_has_calls(
            [
                mock.call(temp_configuration, write_mode=True),
                mock.call(temp_configuration, "2", write_mode=True),
            ],
            any_order=True,
        )
        mock_drive.return_value.get_all_owned_files_metadata.assert_has_calls(
            [
                mock.call(query=f"mimeType = '{FOLDER_MIMETYPE}'"),
            ]
        )
        mock_drive.return_value.get_file_contents_by_name.assert_has_calls(
            [
                mock.call("2", drive_id=CONFIGURATION.google.shared_storage_drive),
            ]
        )
        mock_drive.return_value.batch_remove_permissions.assert_has_calls(
            [
                # Folder permission p1 removed, also removes p1 from f4
                mock.call({"f5": ["p1"]}),
            ]
        )
        mock_drive.return_value.write_file.assert_has_calls(
            [
                mock.call(
                    "bt234-2-shared-permissions-20230101-013000.yaml",
                    datafile("permissions-doc-folder.yaml"),
                    mimetype="text/yaml",
                    parents=[CONFIGURATION.google.shared_storage_drive],
                ),
            ]
        )
        mock_directory.return_value.update_user_ucam_fields.assert_called_once_with(
            "2",
            {
                "mydrive-shared-result": "",
                "mydrive-shared-action": "scan-files",
                "mydrive-shared-filecount": 1,
                "mydrive-shared-doc": "bt234-2-shared-permissions-20230101-013000.yaml",
            },
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_manual_scan_set_on_timeout(self, mock_drive, mock_directory):
        mock_directory.return_value.retrieve_users.return_value = json.loads(
            datafile("directory-ordinary-response.json")
        )["users"]

        # First user scan times out, second user scan completes
        mock_drive.return_value.get_all_owned_files_metadata.side_effect = [
            ListTimeoutError(),
            json.loads(datafile("drive-ordinary-response-folders.json"))["files"],
        ]

        mock_drive.return_value.get_file_contents_by_name.side_effect = [(None, None)]

        with self.assertLogs(level=logging.ERROR) as cm:
            # RuntimeError is raised at the end of the scanning
            with self.assertRaises(RuntimeError):
                scan(CONFIGURATION, write_mode=True)
        # Error log contains notice of users that have manual action required set
        self.assertTrue(
            any(
                "The following users had manual action required set: bt234@gdev.apps.cam.ac.uk"
                in err
                for err in cm.output
            )
        )

        # Construct directory object once
        mock_directory.assert_called_once_with(CONFIGURATION, write_mode=True)
        # Retrieve users called
        mock_directory.return_value.retrieve_users.assert_called_once()
        # Construct shared drive connection, and the two users to scan (id 2 & 3) drive connection
        mock_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=True),
                mock.call(CONFIGURATION, "2", write_mode=True),
                mock.call(CONFIGURATION, "3", write_mode=True),
            ],
            any_order=True,
        )
        # For each user retrieve the list of folders only
        mock_drive.return_value.get_all_owned_files_metadata.assert_has_calls(
            [
                mock.call(query=f"mimeType = '{FOLDER_MIMETYPE}'"),
                mock.call(query=f"mimeType = '{FOLDER_MIMETYPE}'"),
            ]
        )
        # Only second user has permissions removed so an existing document is retrieved
        mock_drive.return_value.get_file_contents_by_name.assert_has_calls(
            [
                mock.call("3", drive_id=CONFIGURATION.google.shared_storage_drive),
            ]
        )
        # Only permissions from the second users folders are removed, and then written to a
        # recovery file
        mock_drive.return_value.batch_remove_permissions.assert_has_calls(
            [
                mock.call({"f5": ["p1"]}),
            ]
        )
        mock_drive.return_value.write_file.assert_has_calls(
            [
                mock.call(
                    "cn345-3-shared-permissions-20230101-013000.yaml",
                    datafile("permissions-doc-folder.yaml"),
                    mimetype="text/yaml",
                    parents=[CONFIGURATION.google.shared_storage_drive],
                ),
            ]
        )
        # First user action set to "manual-scan-folders" as scan timed out but second user action
        # still set to "scan-files" and the recovery file referenced
        mock_directory.return_value.update_user_ucam_fields.assert_has_calls(
            [
                mock.call(
                    "2",
                    {
                        "mydrive-shared-action": "manual-scan-folders",
                    },
                ),
                mock.call(
                    "3",
                    {
                        "mydrive-shared-result": "",
                        "mydrive-shared-action": "scan-files",
                        "mydrive-shared-filecount": 1,
                        "mydrive-shared-doc": "cn345-3-shared-permissions-20230101-013000.yaml",
                    },
                ),
            ],
            any_order=True,
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_recover(self, mock_drive, mock_directory):
        mock_directory.return_value.retrieve_users.return_value = json.loads(
            datafile("directory-ordinary-response.json")
        )["users"]
        mock_drive.return_value.get_file_contents_by_name.side_effect = [
            ("shared-permissions-file.yaml", bytes(datafile("permissions-doc.yaml"), "utf-8")),
            (None, None),
        ]

        recover(CONFIGURATION, write_mode=True)

        # Construct directory object once
        mock_directory.assert_called_once_with(CONFIGURATION, write_mode=True)
        # Retrieve users called
        mock_directory.return_value.retrieve_users.assert_called_once()
        # Construct shared drive connection, and the single user to scan (id 1) drive connection
        mock_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=True),
                mock.call(CONFIGURATION, "1", write_mode=True),
            ],
            any_order=True,
        )
        # Get the permissions document
        mock_drive.return_value.get_file_contents_by_name.assert_has_calls(
            [
                mock.call("1", drive_id=CONFIGURATION.google.shared_storage_drive),
                mock.call("4", drive_id=CONFIGURATION.google.shared_storage_drive),
            ]
        )
        # Loaded permissions doc used to create shared permissions
        mock_drive.return_value.batch_create_shared_permissions.assert_called_once_with(
            yaml.safe_load(datafile("permissions-doc.yaml"))
        )
        mock_drive.return_value.update_file_metadata.assert_called_once_with(
            "shared-permissions-file.yaml",
            {"name": "HISTORIC-shared-permissions-file.yaml"},
            drive_id=CONFIGURATION.google.shared_storage_drive,
        )

        # Update fields, for user 1 mark as recovered and clear action, for user 4 no shared
        # permissions to recover so clear action only.
        mock_directory.return_value.batch_update_user_ucam_fields.assert_called_once_with(
            {
                "1": {
                    "mydrive-shared-result": "permissions-recovered",
                    "mydrive-shared-action": "",
                },
                "4": {
                    "mydrive-shared-action": "",
                },
            }
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_recover_partial(self, mock_drive, mock_directory):
        mock_directory.return_value.retrieve_users.return_value = json.loads(
            datafile("directory-ordinary-response.json")
        )["users"]
        mock_drive.return_value.get_file_contents_by_name.side_effect = [
            ("shared-permissions-file.yaml", bytes(datafile("permissions-doc.yaml"), "utf-8")),
            (None, None),
        ]

        recover(CONFIGURATION, write_mode=True)

        # Construct directory object once
        mock_directory.assert_called_once_with(CONFIGURATION, write_mode=True)
        # Retrieve users called
        mock_directory.return_value.retrieve_users.assert_called_once()
        # Construct shared drive connection, and the single user to scan (id 1) drive connection
        mock_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=True),
                mock.call(CONFIGURATION, "1", write_mode=True),
            ],
            any_order=True,
        )
        # Get the permissions document
        mock_drive.return_value.get_file_contents_by_name.assert_has_calls(
            [
                mock.call("1", drive_id=CONFIGURATION.google.shared_storage_drive),
                mock.call("4", drive_id=CONFIGURATION.google.shared_storage_drive),
            ]
        )
        # Loaded permissions doc used to create shared permissions
        mock_drive.return_value.batch_create_shared_permissions.assert_called_once_with(
            yaml.safe_load(datafile("permissions-doc.yaml"))
        )
        mock_drive.return_value.update_file_metadata.assert_called_once_with(
            "shared-permissions-file.yaml",
            {"name": "HISTORIC-shared-permissions-file.yaml"},
            drive_id=CONFIGURATION.google.shared_storage_drive,
        )

        # Update fields, for user 1 mark as recovered and clear action, for user 4 no shared
        # permissions to recover so clear action only.
        mock_directory.return_value.batch_update_user_ucam_fields.assert_called_once_with(
            {
                "1": {
                    "mydrive-shared-result": "permissions-recovered",
                    "mydrive-shared-action": "",
                },
                "4": {
                    "mydrive-shared-action": "",
                },
            }
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_recover_marked_historic(self, mock_drive, mock_directory):
        self._caplog.set_level(logging.DEBUG)
        mock_directory.return_value.retrieve_users.return_value = json.loads(
            datafile("directory-ordinary-response.json")
        )["users"]
        mock_drive.return_value.get_file_contents_by_name.side_effect = [
            (
                "HISTORIC-shared-permissions-file-1.yaml",
                bytes(datafile("permissions-doc.yaml"), "utf-8"),
            ),
            (
                "HISTORIC-shared-permissions-file-2.yaml",
                bytes(datafile("permissions-doc.yaml"), "utf-8"),
            ),
        ]

        recover(CONFIGURATION, write_mode=True)

        # Construct directory object once
        mock_directory.assert_called_once_with(CONFIGURATION, write_mode=True)
        # Retrieve users called
        mock_directory.return_value.retrieve_users.assert_called_once()
        # Construct shared drive connection
        mock_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=True),
            ],
            any_order=True,
        )
        # Get the permissions document
        mock_drive.return_value.get_file_contents_by_name.assert_has_calls(
            [
                mock.call("1", drive_id=CONFIGURATION.google.shared_storage_drive),
                mock.call("4", drive_id=CONFIGURATION.google.shared_storage_drive),
            ]
        )
        # No permissions should be restored if the permissions file is marked as HISTORIC
        mock_drive.return_value.batch_create_shared_permissions.assert_not_called()

        # Batch update called with empty dictionary, no fields to update
        mock_directory.return_value.batch_update_user_ucam_fields.assert_called_once_with({})

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_recover_with_user(self, mock_drive, mock_directory):
        mock_directory.return_value.retrieve_user.return_value = json.loads(
            datafile("directory-ordinary-response.json")
        )["users"][0]
        mock_drive.return_value.get_file_contents_by_name.side_effect = [
            (
                "shared-permissions-file.yaml",
                bytes(datafile("permissions-doc.yaml"), "utf-8"),
            )
        ]

        recover(CONFIGURATION, user_id="af123", write_mode=True)

        # Construct directory object once
        mock_directory.assert_called_once_with(CONFIGURATION, write_mode=True)
        # Retrieve users called
        mock_directory.return_value.retrieve_user.assert_called_once_with(
            "af123@gdev.apps.cam.ac.uk"
        )
        # Construct shared drive connection, and the single user to scan (id 1) drive connection
        mock_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=True),
                mock.call(CONFIGURATION, "1", write_mode=True),
            ],
            any_order=True,
        )
        # Get the permissions document
        mock_drive.return_value.get_file_contents_by_name.assert_called_once_with(
            "1", drive_id=CONFIGURATION.google.shared_storage_drive
        )
        # Loaded permissions doc used to create shared permissions
        mock_drive.return_value.batch_create_shared_permissions.assert_called_once_with(
            yaml.safe_load(datafile("permissions-doc.yaml"))
        )
        mock_drive.return_value.update_file_metadata.assert_called_once_with(
            "shared-permissions-file.yaml",
            {"name": "HISTORIC-shared-permissions-file.yaml"},
            drive_id=CONFIGURATION.google.shared_storage_drive,
        )

        # Update fields, for user 1 mark as recovered and clear action, for user 4 no shared
        # permissions to recover so clear action only.
        mock_directory.return_value.batch_update_user_ucam_fields.assert_called_once_with(
            {
                "1": {
                    "mydrive-shared-result": "permissions-recovered",
                    "mydrive-shared-action": "",
                },
            }
        )

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_recover_with_missing_user(self, mock_drive, mock_directory):
        mock_directory.return_value.retrieve_user.return_value = None

        recover(CONFIGURATION, user_id="af123", write_mode=True)

        # Construct directory object once
        mock_directory.assert_called_once_with(CONFIGURATION, write_mode=True)
        # Retrieve users called
        mock_directory.return_value.retrieve_user.assert_called_once_with(
            "af123@gdev.apps.cam.ac.uk"
        )
        # Shared drive connection never constructed
        mock_drive.assert_not_called()

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_recover_with_user_not_marked(self, mock_drive, mock_directory):
        user = json.loads(datafile("directory-ordinary-response.json"))["users"][0]
        user["customSchemas"]["UCam"]["mydrive-shared-action"] = ""
        mock_directory.return_value.retrieve_user.return_value = user

        recover(CONFIGURATION, user_id="af123", write_mode=True)

        # Construct directory object once
        mock_directory.assert_called_once_with(CONFIGURATION, write_mode=True)
        # Retrieve users called
        mock_directory.return_value.retrieve_user.assert_called_once_with(
            "af123@gdev.apps.cam.ac.uk"
        )
        # Shared drive connection never constructed
        mock_drive.assert_not_called()

    @freeze_time(frozen_time)
    @mock.patch("gdrivemanager.manage.user_drives.rescan_permissions")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_recover_with_rescan(self, mock_drive, mock_directory, mock_rescan):
        user = json.loads(datafile("directory-ordinary-response.json"))["users"][0]
        mock_directory.return_value.retrieve_user.return_value = user
        mock_drive.return_value.get_file_contents_by_name.side_effect = [
            (
                "shared-permissions-file.yaml",
                bytes(datafile("permissions-doc.yaml"), "utf-8"),
            )
        ]
        # One file removed from the permissions document
        original_doc = yaml.safe_load(datafile("permissions-doc.yaml"))
        filtered_doc = {k: v for k, v in original_doc.items() if k != "f1"}
        mock_rescan.return_value = filtered_doc

        recover(CONFIGURATION, user_id="af123", rescan=True, write_mode=True)

        # Construct directory object once
        mock_directory.assert_called_once_with(CONFIGURATION, write_mode=True)
        # Retrieve users called
        mock_directory.return_value.retrieve_user.assert_called_once_with(
            "af123@gdev.apps.cam.ac.uk"
        )
        # Construct shared drive connection, and the single user to scan (id 1) drive connection
        mock_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=True),
                mock.call(CONFIGURATION, "1", write_mode=True),
            ],
            any_order=True,
        )
        # Get the permissions document
        mock_drive.return_value.get_file_contents_by_name.assert_called_once_with(
            "1", drive_id=CONFIGURATION.google.shared_storage_drive
        )
        # Rescan called with original document
        mock_rescan.assert_called_once_with(user, CONFIGURATION, original_doc)

        # Filtered by rescan permissions doc used to create shared permissions
        mock_drive.return_value.batch_create_shared_permissions.assert_called_once_with(
            filtered_doc
        )
        mock_drive.return_value.update_file_metadata.assert_called_once_with(
            "shared-permissions-file.yaml",
            {"name": "HISTORIC-shared-permissions-file.yaml"},
            drive_id=CONFIGURATION.google.shared_storage_drive,
        )

        # Update fields, for user 1 mark as recovered and clear action, for user 4 no shared
        # permissions to recover so clear action only.
        mock_directory.return_value.batch_update_user_ucam_fields.assert_called_once_with(
            {
                "1": {
                    "mydrive-shared-result": "permissions-recovered",
                    "mydrive-shared-action": "",
                },
            }
        )

    @mock.patch("gdrivemanager.manage.user_drives.GoogleDriveConnection")
    def test_rescan_permissions(self, mock_drive):
        original_doc = yaml.safe_load(datafile("permissions-doc.yaml"))

        # Mock that files f1 and f4 already exist, are shared and have permissions though f4's
        # permissions are slightly different
        existing_files = {
            k: {**v, "shared": True} for k, v in original_doc.items() if k in ["f1", "f4"]
        }
        existing_files["f4"]["permissions"] = [
            {
                "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
                "id": "p9",  # was p3 but ignored in comparison anyway
                "role": "viewer",  # was "editor"
                "type": "user",
            },
            {
                "emailAddress": "mk2155@gdev.apps.cam.ac.uk",
                "id": "p2",
                "role": "owner",
                "type": "user",
            },
        ]
        mock_drive.return_value.get_all_owned_files_metadata.return_value = [
            {"id": k, **v} for k, v in existing_files.items()
        ]
        user = json.loads(datafile("directory-ordinary-response.json"))["users"][0]

        resulting_doc = rescan_permissions(user, CONFIGURATION, original_doc)

        # Construct connection single user to scan (id 1) drive connection
        mock_drive.assert_called_once_with(CONFIGURATION, "1", write_mode=False)

        mock_drive.return_value.get_all_owned_files_metadata.assert_called_once_with()

        # f1 should be removed from the resulting document as it no longer exists
        self.assertNotIn("f1", resulting_doc)
        # f2 should be present and with the original permissions
        self.assertIn("f4", resulting_doc)
        self.assertEqual(resulting_doc["f4"]["permissions"], original_doc["f4"]["permissions"])
        # f5 shouldn't have been removed as it didn't exist
        self.assertIn("f5", resulting_doc)

    def test_matching_permissions(self):
        writer = {
            "id": "p1",
            "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
            "role": "writer",
            "type": "user",
        }
        reader = {
            "id": "p2",
            "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
            "role": "reader",
            "type": "user",
        }
        domain_reader = {
            "id": "p3",
            "domain": "gdev.apps.cam.ac.uk",
            "role": "reader",
            "type": "domain",
        }
        anyone_writer = {
            "id": "anyoneWithLink",
            "role": "writer",
            "type": "anyone",
        }

        # different lengths don't match
        self.assertFalse(matching_permissions([writer], [writer, reader]))
        # different roles don't match
        self.assertFalse(matching_permissions([writer], [reader]))
        # different types don't match
        self.assertFalse(matching_permissions([reader], [domain_reader]))
        self.assertFalse(matching_permissions([writer], [anyone_writer]))

        # same permissions match
        self.assertTrue(
            matching_permissions(
                [reader, writer, domain_reader, anyone_writer],
                [reader, writer, domain_reader, anyone_writer],
            )
        )
        # ordering doesn't matter
        self.assertTrue(
            matching_permissions(
                [reader, writer, domain_reader, anyone_writer],
                [reader, domain_reader, anyone_writer, writer],
            )
        )
        # different ids are ignored
        similar_writer = {k: (v if k != "id" else "p9") for k, v in writer.items()}
        self.assertTrue(matching_permissions([writer], [similar_writer]))

    @mock.patch("gdrivemanager.manage.user_drives.GoogleDirectoryConnection")
    def test_manual_reset(self, mock_directory):
        mock_directory.return_value.retrieve_users.return_value = json.loads(
            datafile("directory-ordinary-response.json")
        )["users"]

        reset_manual_actions(CONFIGURATION, write_mode=True)

        # Update the ucam fields for user with "manual-" prefix.
        mock_directory.return_value.update_user_ucam_fields.assert_called_once_with(
            "7",
            {
                "mydrive-shared-action": "scan-files",
            },
        )
