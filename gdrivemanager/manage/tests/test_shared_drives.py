import logging
from datetime import datetime, timedelta, timezone
from unittest import TestCase, mock

import yaml
from freezegun import freeze_time
from googleapiclient.errors import HttpError

from gdrivemanager.manage.shared_drives import (
    connect_as_manager,
    flush_shared_drive_cache,
    rebuild_shared_drive_cache,
    remove_shared_drive_permissions,
    restore_shared_drive_permissions,
    scan_shared_drives,
    update_drive_in_cache,
    write_shared_drive_recovery_file,
)
from gdrivemanager.tests import CONFIGURATION, datafile


class ManageSharedDrivesTests(TestCase):
    shared_drive_list_within_limit = datetime(2023, 5, 17, 10, 30, tzinfo=timezone.utc)
    shared_drive_list_out_of_date = datetime(2023, 5, 30, 10, 30, tzinfo=timezone.utc)

    @freeze_time(shared_drive_list_out_of_date)
    @mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection")
    def test_shared_drives(self, mock_drive):
        # Multiple calls to GoogleDriveConnection are made during scan_shared_drives() so have a
        # separate mock for each
        cache_drive_connection = mock.Mock(name="cache")
        shared_drive_connection = mock.Mock(name="shared_drive")
        # Unscanned drives first starting with drive3
        drive3_manager_connection = mock.Mock(name="drive3")
        # Next unscanned drive drive4 has no manager so a connection to add one is made
        add_manager_connection = mock.Mock(name="add_manager")
        # Drive1 last_updated is older than drive2 so will be created first
        drive1_manager_connection = mock.Mock(name="drive1")
        drive2_manager_connection = mock.Mock(name="drive2")
        mock_list = [
            cache_drive_connection,
            shared_drive_connection,
            drive3_manager_connection,
            add_manager_connection,
            drive1_manager_connection,
            drive2_manager_connection,
        ]
        drive_mocks_created = len(mock_list)

        def mock_construct(configuration, impersonate=None, write_mode=False):
            nonlocal mock_list
            # Keep track of which mock was constructed so we can assert on it later
            next_mock = mock_list.pop(0)
            next_mock.constructed_with = (configuration, impersonate, write_mode)
            return next_mock

        mock_drive.side_effect = mock_construct

        # Initial mocked read of the cache file is the same as the test data.
        test_data_doc = bytes(datafile("shared_drive_list.yaml"), "utf-8")
        # The second read of the cache file is the same as the first but the last_updated has been
        # updated to the current time by the rebuild process
        doc_as_dict = yaml.load(test_data_doc, Loader=yaml.CLoader)
        doc_as_dict["last_updated"] = datetime.now(timezone.utc)
        post_rebuild_doc = yaml.dump(doc_as_dict, Dumper=yaml.CDumper).encode("utf-8")

        cache_drive_connection.get_shared_drive_cache.side_effect = [
            test_data_doc,
            post_rebuild_doc,
        ]

        test_data_drives = yaml.safe_load(test_data_doc)["shared_drives"]
        # Retrieve shared drives list is unchanged from the test data
        retrieved_fields = ["id", "name", "createdTime", "orgUnitId"]
        shared_drive_connection.retrieve_shared_drives.return_value = [
            {k: d.get(k) for k in retrieved_fields} for d in test_data_drives
        ]
        shared_drive_connection.read_shared_drive_permissions.side_effect = lambda id: next(
            d["permissions"] for d in test_data_drives if d["id"] == id
        )

        # Quota usage, new for drive3, same for drive1 and updated for drive2
        drive3_manager_connection.get_shared_drive_usage_and_augmented_perms.return_value = (
            10,
            12345678,
            {"f3"},
        )
        drive1_manager_connection.get_shared_drive_usage_and_augmented_perms.return_value = (
            22,
            123456,
            set(),
        )
        drive2_manager_connection.get_shared_drive_usage_and_augmented_perms.return_value = (
            1,
            98765,
            {"f1", "f2"},
        )

        # Augmented permissions for each drive (though drive1 has none)
        DRIVE2_AUGMENTED_PERMISSIONS = [
            {
                "file_id": "f1",
                "permissions": [
                    {
                        "id": "p50",
                        "type": "user",
                        "role": "reader",
                        "emailAddress": "mk2155@gdev.apps.cam.ac.uk",
                    }
                ],
            },
            {
                "file_id": "f2",
                "permissions": [
                    {
                        "id": "p60",
                        "type": "user",
                        "role": "writer",
                        "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
                    }
                ],
            },
        ]
        DRIVE3_AUGMENTED_PERMISSIONS = [
            {
                "file_id": "f3",
                "permissions": [
                    {
                        "id": "p40",
                        "type": "user",
                        "role": "reader",
                        "emailAddress": "mk2155@gdev.apps.cam.ac.uk",
                    }
                ],
            }
        ]
        drive1_manager_connection.get_shared_drive_augmented_permissions.return_value = {}
        drive2_manager_connection.get_shared_drive_augmented_permissions.return_value = (
            DRIVE2_AUGMENTED_PERMISSIONS
        )
        drive3_manager_connection.get_shared_drive_augmented_permissions.return_value = (
            DRIVE3_AUGMENTED_PERMISSIONS
        )

        scan_shared_drives(CONFIGURATION, write_mode=True)

        # GoogleDriveConnection called as many times as we created mocks
        self.assertEqual(mock_drive.call_count, drive_mocks_created)

        # Get Shared Drive cache called twice - once for initial read and once just before write
        self.assertEqual(cache_drive_connection.get_shared_drive_cache.call_count, 2)
        self.assertEqual(cache_drive_connection.constructed_with, (CONFIGURATION, None, True))

        # Retrieve shared drives called once
        self.assertEqual(
            shared_drive_connection.constructed_with,
            (CONFIGURATION, CONFIGURATION.google.admin_user, False),
        )
        shared_drive_connection.retrieve_shared_drives.assert_called_once()

        # Read permissions for each shared drive called for each in need of scanning order
        shared_drive_connection.read_shared_drive_permissions.assert_has_calls(
            [mock.call(id) for id in ["drive3", "drive4", "drive1", "drive2"]]
        )

        # Connect as only manager of drive3 and get usage first (as it's unscanned)
        self.assertEqual(
            drive3_manager_connection.constructed_with,
            (CONFIGURATION, "rjg21@gdev.apps.cam.ac.uk", False),
        )
        drive3_manager_connection.get_shared_drive_usage_and_augmented_perms.assert_called_once_with(  # noqa: E501
            "drive3"
        )
        drive3_manager_connection.get_shared_drive_augmented_permissions.assert_called_once_with(
            {"f3"}, {"p20", "p50"}
        )

        # Connect as manager of drive4 will fail so another drive connection (impersonating admin)
        # is made to add the management_user
        self.assertEqual(
            add_manager_connection.constructed_with,
            (CONFIGURATION, CONFIGURATION.google.admin_user, True),
        )
        add_manager_connection.add_manager_to_shared_drive.assert_called_once_with(
            "drive4", CONFIGURATION.google.management_user
        )

        # Connect as first manager of drive1 and get usage next (as it was scanned longest ago)
        self.assertEqual(
            drive1_manager_connection.constructed_with,
            (CONFIGURATION, "amc203@gdev.apps.cam.ac.uk", False),
        )
        drive1_manager_connection.get_shared_drive_usage_and_augmented_perms.assert_called_once_with(  # noqa: E501
            "drive1"
        )
        drive1_manager_connection.get_shared_drive_augmented_permissions.assert_not_called()

        # Connect as only manager of drive2 and get usage last (as it was scanned most recently)
        self.assertEqual(
            drive2_manager_connection.constructed_with,
            (CONFIGURATION, "mk2155@gdev.apps.cam.ac.uk", False),
        )
        drive2_manager_connection.get_shared_drive_usage_and_augmented_perms.assert_called_once_with(  # noqa: E501
            "drive2"
        )
        drive2_manager_connection.get_shared_drive_augmented_permissions.assert_called_once_with(
            {"f1", "f2"}, {"p10"}
        )

        # Shared Drive cache rewritten 2 times - firstly when the list was rebuilt and then after
        # all drives scanned (as they all took less than the 5 minutes update frequency as time
        # was frozen)
        self.assertEqual(cache_drive_connection.write_file.call_count, 2)

        # Resulting shared drive cache file is as expected as per test data with updates to files,
        # usage and last_updated
        final_write = cache_drive_connection.write_file.call_args.args[1]
        # Need UnsafeLoader to parse freezegun timestamps
        final_shared_drives_cache = yaml.load(final_write, Loader=yaml.UnsafeLoader)

        # last updated is current (frozen) time
        self.assertEqual(
            str(final_shared_drives_cache["last_updated"]), "2023-05-30 10:30:00+00:00"
        )
        # all four drives are present
        self.assertEqual(len(final_shared_drives_cache["shared_drives"]), 4)

        # drive1 files and usage is unchanged but last_updated, last_scanned and last_scan_success
        # are all updated
        drive_data = next(
            d for d in final_shared_drives_cache["shared_drives"] if d["id"] == "drive1"
        )
        self.assertEqual(drive_data["files"], 22)
        self.assertEqual(drive_data["usage"], 123456)
        self.assertEqual(str(drive_data["last_updated"]), "2023-05-30 10:30:00+00:00")
        self.assertEqual(str(drive_data["last_scanned"]), "2023-05-30 10:30:00+00:00")
        self.assertTrue(drive_data["last_scan_success"])
        self.assertIsNone(drive_data.get("augmented_permissions"))

        # drive2 files and usage is completely updated
        drive_data = next(
            d for d in final_shared_drives_cache["shared_drives"] if d["id"] == "drive2"
        )
        self.assertEqual(drive_data["files"], 1)
        self.assertEqual(drive_data["usage"], 98765)
        self.assertEqual(str(drive_data["last_updated"]), "2023-05-30 10:30:00+00:00")
        self.assertEqual(str(drive_data["last_scanned"]), "2023-05-30 10:30:00+00:00")
        self.assertTrue(drive_data["last_scan_success"])
        self.assertEqual(drive_data.get("augmented_permissions"), DRIVE2_AUGMENTED_PERMISSIONS)

        # drive3 files and usage is new (hadn't previously been scanned)
        drive_data = next(
            d for d in final_shared_drives_cache["shared_drives"] if d["id"] == "drive3"
        )
        self.assertEqual(drive_data["files"], 10)
        self.assertEqual(drive_data["usage"], 12345678)
        self.assertEqual(str(drive_data["last_updated"]), "2023-05-30 10:30:00+00:00")
        self.assertEqual(str(drive_data["last_scanned"]), "2023-05-30 10:30:00+00:00")
        self.assertTrue(drive_data["last_scan_success"])
        self.assertEqual(drive_data.get("augmented_permissions"), DRIVE3_AUGMENTED_PERMISSIONS)

        # drive4 files and usage are still unknown and scan was a failure
        drive_data = next(
            d for d in final_shared_drives_cache["shared_drives"] if d["id"] == "drive4"
        )
        self.assertEqual(drive_data["files"], None)
        self.assertEqual(drive_data["usage"], None)
        self.assertIsNone(drive_data.get("last_updated"))
        self.assertEqual(str(drive_data["last_scanned"]), "2023-05-30 10:30:00+00:00")
        self.assertFalse(drive_data["last_scan_success"])
        self.assertIsNone(drive_data.get("augmented_permissions"))

    @freeze_time(shared_drive_list_out_of_date)
    @mock.patch("gdrivemanager.manage.shared_drives.rebuild_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.read_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.get_shared_drives_to_update")
    @mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection")
    def test_skip_rebuild_shared_drives(self, mock_drive, mock_to_update, mock_read, mock_rebuild):
        # Skip usage updating part of test
        mock_to_update.return_value = []

        # Cache file exists (and even out of date)
        mock_read.return_value = yaml.safe_load(bytes(datafile("shared_drive_list.yaml"), "utf-8"))

        with self.assertLogs(level=logging.WARNING) as cm:
            scan_shared_drives(CONFIGURATION, skip_rebuild=True, write_mode=True)

        # cache_drive_connection and shared_drive_connection created, though the latter isn't used
        mock_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=True),
                mock.call(CONFIGURATION, CONFIGURATION.google.admin_user, write_mode=False),
            ]
        )
        # Rebuild not called even though cache is out of date due to skip_rebuild
        mock_rebuild.assert_not_called()

        # But warning was logged
        self.assertTrue(
            any(
                "Shared drive list cache is out of date but --skip-rebuild is set" in err
                for err in cm.output
            )
        )

    @freeze_time(shared_drive_list_within_limit)
    @mock.patch("gdrivemanager.manage.shared_drives.rebuild_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.write_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.read_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.get_shared_drives_to_update")
    @mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection")
    def test_no_cache_shared_drives(
        self, mock_drive, mock_to_update, mock_read, mock_write, mock_rebuild
    ):
        # Skip usage updating part of test
        mock_to_update.return_value = []

        # No existing cache file
        mock_read.return_value = None

        scan_shared_drives(CONFIGURATION, write_mode=True)

        # cache_drive_connection and shared_drive_connection created
        mock_drive.assert_has_calls(
            [
                mock.call(CONFIGURATION, write_mode=True),
                mock.call(CONFIGURATION, CONFIGURATION.google.admin_user, write_mode=False),
            ]
        )
        # Rebuild called (as no skip_rebuild) and write called to save new cache
        mock_rebuild.assert_called_once_with(
            CONFIGURATION, mock_drive.return_value, {"last_updated": None, "shared_drives": []}
        )
        mock_write.assert_called_once()

    @freeze_time(shared_drive_list_within_limit)
    @mock.patch("gdrivemanager.manage.shared_drives.rebuild_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.write_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.read_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.get_shared_drives_to_update")
    @mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection")
    def test_single_shared_drive(
        self, mock_drive, mock_to_update, mock_read, mock_write, mock_rebuild
    ):
        # Multiple calls to GoogleDriveConnection are made during scan_shared_drives() so have a
        # separate mock for each
        cache_drive_connection = mock.Mock(name="cache")
        shared_drive_connection = mock.Mock(name="shared_drive")
        # Single specified drive connection as its manager
        drive_manager_connection = mock.Mock(name="drive")

        mock_list = [
            cache_drive_connection,
            shared_drive_connection,
            drive_manager_connection,
        ]
        drive_mocks_created = len(mock_list)

        def mock_construct(configuration, impersonate=None, write_mode=False):
            nonlocal mock_list
            # Keep track of which mock was constructed so we can assert on it later
            next_mock = mock_list.pop(0)
            next_mock.constructed_with = (configuration, impersonate, write_mode)
            return next_mock

        mock_drive.side_effect = mock_construct

        # Cache file exists (and freeze_time is within limit)
        original_cache = yaml.safe_load(bytes(datafile("shared_drive_list.yaml"), "utf-8"))
        mock_read.return_value = original_cache

        updated_drive_permissions = [
            {
                "id": "p20",
                "type": "user",
                "role": "organizer",
                "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
            },
            {
                "id": "p99",
                "type": "user",
                "role": "reader",
                "emailAddress": "mk2155@gdev.apps.cam.ac.uk",
            },
        ]
        shared_drive_connection.read_shared_drive_permissions.return_value = (
            updated_drive_permissions
        )

        # Quota usage and no augmented files found for specified drive
        drive_manager_connection.get_shared_drive_usage_and_augmented_perms.return_value = (
            10,
            12345678,
            set(),
        )

        # Skipping rebuild to focus test on specifying a single drive
        scan_shared_drives(CONFIGURATION, skip_rebuild=True, drive_id="drive3", write_mode=True)

        # GoogleDriveConnection called as many times as we created mocks
        self.assertEqual(mock_drive.call_count, drive_mocks_created)

        # Cache drive connection created
        self.assertEqual(cache_drive_connection.constructed_with, (CONFIGURATION, None, True))

        # Shared drive connection created
        self.assertEqual(
            shared_drive_connection.constructed_with,
            (CONFIGURATION, CONFIGURATION.google.admin_user, False),
        )

        # Get Shared Drive cache called twice - once for initial read and once just before write
        self.assertEqual(mock_read.call_count, 2)

        # Rebuild share drive list not called due to skip_rebuild=True
        mock_rebuild.assert_not_called()

        # get_shared_drives_to_update not called
        mock_to_update.assert_not_called()

        # Drive connection as manager created and usage checked
        self.assertEqual(
            drive_manager_connection.constructed_with,
            (CONFIGURATION, "rjg21@gdev.apps.cam.ac.uk", False),
        )
        drive_manager_connection.get_shared_drive_usage_and_augmented_perms.assert_called_once_with(  # noqa: E501
            "drive3"
        )

        # Shared Drive cache rewritten
        expected_cache = original_cache
        drive_record = next(d for d in expected_cache["shared_drives"] if d["id"] == "drive3")
        drive_record.update(
            {
                "permissions": updated_drive_permissions,
                "files": 10,
                "usage": 12345678,
                "last_updated": datetime.now(timezone.utc),
                "last_scanned": datetime.now(timezone.utc),
                "last_scan_success": True,
            }
        )
        # Called once (once when the data is flushed)
        mock_write.assert_called_once_with(CONFIGURATION, cache_drive_connection, expected_cache)

    @mock.patch("gdrivemanager.manage.shared_drives.write_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.read_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection")
    def test_update_drive_in_cache(self, mock_drive, mock_read, mock_write):
        existing_cache = {
            "last_updated": datetime(2023, 5, 23, 10, 30, tzinfo=timezone.utc),
            "shared_drives": [
                {"id": "drive1", "name": "Drive 1"},
            ],
        }
        mock_read.return_value = existing_cache

        with freeze_time("2023-05-23 10:30:00") as frozen_time:
            # First call doesn't write cache
            self._clear_static_vars()
            update_drive_in_cache(
                CONFIGURATION,
                mock_drive.return_value,
                {"id": "drive1", "name": "Drive 1 New Name"},
            )
            mock_write.assert_not_called()

            # Write still not called if not enough time hasn't passed
            frozen_time.tick(delta=timedelta(minutes=1))
            update_drive_in_cache(
                CONFIGURATION, mock_drive.return_value, {"id": "drive2", "name": "Drive 2"}
            )
            mock_write.assert_not_called()

            # Only when enough time has passed does the write happen
            frozen_time.tick(delta=timedelta(minutes=10))
            update_drive_in_cache(
                CONFIGURATION, mock_drive.return_value, {"id": "drive3", "name": "Drive 3"}
            )
            mock_write.assert_called_once_with(
                CONFIGURATION,
                mock_drive.return_value,
                {
                    "last_updated": datetime(2023, 5, 23, 10, 30, tzinfo=timezone.utc),
                    "shared_drives": [
                        {"id": "drive1", "name": "Drive 1 New Name"},
                        {"id": "drive2", "name": "Drive 2"},
                        {"id": "drive3", "name": "Drive 3"},
                    ],
                },
            )

    @mock.patch("gdrivemanager.manage.shared_drives.write_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.read_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection")
    def test_update_drive_in_cache_force(self, mock_drive, mock_read, mock_write):
        existing_cache = {
            "last_updated": datetime(2023, 5, 23, 10, 30, tzinfo=timezone.utc),
            "shared_drives": [
                {"id": "drive1", "name": "Drive 1"},
            ],
        }
        mock_read.return_value = existing_cache

        with freeze_time("2023-05-23 10:30:00") as frozen_time:
            # Force causes write to happen on first call
            self._clear_static_vars()
            update_drive_in_cache(
                CONFIGURATION,
                mock_drive.return_value,
                {"id": "drive1", "name": "Drive 1 New Name"},
                force=True,
            )
            mock_write.assert_called_once_with(
                CONFIGURATION,
                mock_drive.return_value,
                {
                    "last_updated": datetime(2023, 5, 23, 10, 30, tzinfo=timezone.utc),
                    "shared_drives": [
                        {"id": "drive1", "name": "Drive 1 New Name"},
                    ],
                },
            )
            mock_write.reset_mock()

            # Adding another drive will set the next update to 5 minutes from now
            frozen_time.tick(delta=timedelta(minutes=1))
            update_drive_in_cache(
                CONFIGURATION, mock_drive.return_value, {"id": "drive2", "name": "Drive 2"}
            )
            mock_write.assert_not_called()

            # Force causes write to happen even if not enough time hasn't passed
            frozen_time.tick(delta=timedelta(minutes=1))
            update_drive_in_cache(
                CONFIGURATION,
                mock_drive.return_value,
                {"id": "drive3", "name": "Drive 3"},
                force=True,
            )
            mock_write.assert_called_once_with(
                CONFIGURATION,
                mock_drive.return_value,
                {
                    "last_updated": datetime(2023, 5, 23, 10, 30, tzinfo=timezone.utc),
                    "shared_drives": [
                        {"id": "drive1", "name": "Drive 1 New Name"},
                        {"id": "drive2", "name": "Drive 2"},
                        {"id": "drive3", "name": "Drive 3"},
                    ],
                },
            )

    @mock.patch("gdrivemanager.manage.shared_drives.write_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.read_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection")
    def test_flush_shared_drive_cache(self, mock_drive, mock_read, mock_write):
        existing_cache = {
            "last_updated": datetime(2023, 5, 23, 10, 30, tzinfo=timezone.utc),
            "shared_drives": [
                {"id": "drive1", "name": "Drive 1"},
            ],
        }
        mock_read.return_value = existing_cache
        with freeze_time("2023-05-23 10:30:00") as frozen_time:
            # Flushing before first call to update_drive_in_cache does nothing
            self._clear_static_vars()
            flush_shared_drive_cache(CONFIGURATION, mock_drive.return_value)
            mock_write.assert_not_called()

            # Flushing when there is a pending update, writes even if not enough time has passed
            update_drive_in_cache(
                CONFIGURATION,
                mock_drive.return_value,
                {"id": "drive1", "name": "Drive 1 New Name"},
            )
            frozen_time.tick(delta=timedelta(minutes=1))
            flush_shared_drive_cache(CONFIGURATION, mock_drive.return_value)
            mock_write.assert_called_once_with(
                CONFIGURATION,
                mock_drive.return_value,
                {
                    "last_updated": datetime(2023, 5, 23, 10, 30, tzinfo=timezone.utc),
                    "shared_drives": [
                        {"id": "drive1", "name": "Drive 1 New Name"},
                    ],
                },
            )
            mock_write.reset_mock()

            # Flushing having just written, doesn't write again even if enough time has
            # passed
            frozen_time.tick(delta=timedelta(minutes=10))
            flush_shared_drive_cache(CONFIGURATION, mock_drive.return_value)
            mock_write.assert_not_called()

    def _clear_static_vars(self):
        try:
            delattr(update_drive_in_cache, "drive_updates")
            delattr(update_drive_in_cache, "next_update")
        except AttributeError:
            pass

    @mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection")
    def test_connect_as_manager(self, mock_drive):
        drive = {
            "id": "testdrive",
            "name": "Test Drive",
            "permissions": [
                {
                    "id": "p1",
                    "type": "user",
                    "role": "reader",
                    "emailAddress": "uvw456@gdev.apps.cam.ac.uk",
                },
                {
                    "id": "p2",
                    "type": "user",
                    "role": "organizer",
                    "emailAddress": "abc123@gdev.apps.cam.ac.uk",
                },
                {
                    "id": "p3",
                    "type": "user",
                    "role": "fileOrganizer",
                    "emailAddress": "xyz789@gdev.apps.cam.ac.uk",
                },
            ],
        }

        with mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection") as mock_drive:
            # With no existing manager, connect as first organizer or fileOrganizer
            manager, connection = connect_as_manager(CONFIGURATION, drive, None, None)
            self.assertEqual(manager, "abc123@gdev.apps.cam.ac.uk")
            self.assertEqual(connection, mock_drive.return_value)
            mock_drive.assert_called_once_with(
                CONFIGURATION, "abc123@gdev.apps.cam.ac.uk", write_mode=False
            )

        with mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection") as mock_drive:
            # With existing manager that doesn't have permission, connect as first organizer
            # or fileOrganizer
            current_connection = mock.Mock()
            manager, connection = connect_as_manager(
                CONFIGURATION, drive, "def123@gdev.apps.cam.ac.uk", current_connection
            )
            self.assertEqual(manager, "abc123@gdev.apps.cam.ac.uk")
            self.assertEqual(connection, mock_drive.return_value)
            mock_drive.assert_called_once_with(
                CONFIGURATION, "abc123@gdev.apps.cam.ac.uk", write_mode=False
            )

        with mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection") as mock_drive:
            # With existing manager that has permission, maintain existing manager and connection
            current_connection = mock.Mock()
            manager, connection = connect_as_manager(
                CONFIGURATION, drive, "xyz789@gdev.apps.cam.ac.uk", current_connection
            )
            self.assertEqual(manager, "xyz789@gdev.apps.cam.ac.uk")
            self.assertEqual(connection, current_connection)
            mock_drive.assert_not_called()

        with mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection") as mock_drive:
            resp = mock.Mock()
            resp.status = 403
            resp.reason = "Fake Error"
            mock_drive.side_effect = HttpError(resp=resp, content=b"")
            # If unable to connect as any listed manager, return no manager but keep existing
            # connection
            current_connection = mock.Mock()
            manager, connection = connect_as_manager(
                CONFIGURATION, drive, "def123@gdev.apps.cam.ac.uk", current_connection
            )
            self.assertIsNone(manager)
            self.assertEqual(connection, current_connection)
            mock_drive.assert_has_calls(
                [
                    mock.call(CONFIGURATION, "abc123@gdev.apps.cam.ac.uk", write_mode=False),
                    mock.call(CONFIGURATION, "xyz789@gdev.apps.cam.ac.uk", write_mode=False),
                ]
            )

        # Strip manager permissions
        drive["permissions"] = drive["permissions"][:1]

        with mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection") as mock_drive:
            # Without any managers return no manager but keep existing connection
            current_connection = mock.Mock()
            manager, connection = connect_as_manager(
                CONFIGURATION, drive, "xyz789@gdev.apps.cam.ac.uk", current_connection
            )
            self.assertIsNone(manager)
            self.assertEqual(connection, current_connection)
            mock_drive.assert_not_called()

    @freeze_time(shared_drive_list_within_limit)
    def test_rebuild_shared_drive_cache(self):
        drive_connection = mock.Mock()

        cache = {"last_updated": None, "shared_drives": []}
        # Add single drive on empty cache
        drive_connection.retrieve_shared_drives.return_value = [
            {
                "id": "drive1",
                "name": "Drive 1",
                "createdTime": "2023-05-01T12:00:00.000Z",
                "orgUnitId": "ou1",
            }
        ]

        cache = rebuild_shared_drive_cache(CONFIGURATION, drive_connection, cache)

        self.assertEqual(
            cache,
            {
                "last_updated": datetime.now(timezone.utc),
                "shared_drives": [
                    {
                        "id": "drive1",
                        "name": "Drive 1",
                        "createdTime": "2023-05-01T12:00:00.000Z",
                        "orgUnitId": "ou1",
                    }
                ],
            },
        )

        # Simulate a scanning of the same drive
        cache["shared_drives"][0]["usage"] = 1234
        cache["shared_drives"][0]["files"] = 99
        cache["shared_drives"][0]["last_updated"] = datetime.now(timezone.utc)
        cache["shared_drives"][0]["last_scanned"] = datetime.now(timezone.utc)
        cache["shared_drives"][0]["last_scan_success"] = True
        cache["shared_drives"][0]["permissions"] = [
            {
                "id": "p1",
                "type": "user",
                "role": "organiser",
                "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
            }
        ]

        # Add another drive and update existing one
        drive_connection.retrieve_shared_drives.return_value = [
            {
                "id": "drive1",
                "name": "Drive 1 New Name",
                "createdTime": "2023-05-01T12:00:00.000Z",
                "orgUnitId": "ou2",
            },
            {
                "id": "drive2",
                "name": "Drive 2",
                "createdTime": "2023-06-01T12:00:00.000Z",
                "orgUnitId": "ou1",
            },
        ]

        cache = rebuild_shared_drive_cache(CONFIGURATION, drive_connection, cache)

        self.assertEqual(
            cache,
            {
                "last_updated": datetime.now(timezone.utc),
                "shared_drives": [
                    {
                        "id": "drive1",
                        "name": "Drive 1 New Name",
                        "createdTime": "2023-05-01T12:00:00.000Z",
                        "orgUnitId": "ou2",
                        "usage": 1234,
                        "files": 99,
                        "last_updated": datetime.now(timezone.utc),
                        "last_scanned": datetime.now(timezone.utc),
                        "last_scan_success": True,
                        "permissions": [
                            {
                                "id": "p1",
                                "type": "user",
                                "role": "organiser",
                                "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
                            }
                        ],
                    },
                    {
                        "id": "drive2",
                        "name": "Drive 2",
                        "createdTime": "2023-06-01T12:00:00.000Z",
                        "orgUnitId": "ou1",
                    },
                ],
            },
        )

    @freeze_time(shared_drive_list_within_limit)
    @mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection")
    def test_remove_shared_drive_permissions(self, mock_drive):
        # Multiple calls to GoogleDriveConnection are made during remove_shared_drive_permissions()
        # so have a separate mock for each

        # Connection for reading/writing the shared drive cache and recovery files
        cache_rec_drive_connection = mock.Mock(name="cache_rec")
        # Connection as admin user for adding/removing from the shared drive's permissions
        admin_drive_connection = mock.Mock(name="admin")
        # Connection as management user for removing augmented permissions
        management_drive_connection = mock.Mock(name="management")

        mock_list = [
            cache_rec_drive_connection,
            admin_drive_connection,
            management_drive_connection,
        ]
        drive_mocks_created = len(mock_list)

        def mock_construct(configuration, impersonate=None, write_mode=False):
            nonlocal mock_list
            # Keep track of which mock was constructed so we can assert on it later
            next_mock = mock_list.pop(0)
            next_mock.constructed_with = (configuration, impersonate, write_mode)
            return next_mock

        mock_drive.side_effect = mock_construct

        # Use standard fixture for shared drive cache but add augmented permissions to drive1
        test_data_doc = bytes(datafile("shared_drive_list.yaml"), "utf-8")
        test_data = yaml.load(test_data_doc.decode("utf-8"), Loader=yaml.CLoader)
        drive1_idx = next(
            i for i, d in enumerate(test_data["shared_drives"]) if d["id"] == "drive1"
        )
        test_data["shared_drives"][drive1_idx]["augmented_permissions"] = [
            {
                "file_id": "f1",
                "permissions": [
                    {
                        "id": "p10",
                        "type": "user",
                        "role": "reader",
                        "emailAddress": "mk2155@gdev.apps.cam.ac.uk",
                    }
                ],
            },
            {
                "file_id": "f2",
                "permissions": [
                    {
                        "id": "anyoneWithLink",
                        "type": "anyone",
                        "role": "writer",
                    }
                ],
            },
        ]
        test_data_doc = yaml.dump(test_data).encode("utf-8")

        cache_rec_drive_connection.get_shared_drive_cache.return_value = test_data_doc

        # No existing recovery file
        cache_rec_drive_connection.get_file_contents_by_name.return_value = (None, None)

        # Remove permissions for drive1
        remove_shared_drive_permissions(CONFIGURATION, "drive1", write_mode=True)

        # GoogleDriveConnection called as many times as we created mocks
        self.assertEqual(mock_drive.call_count, drive_mocks_created)

        # Cache and recovery drive connection created
        self.assertEqual(cache_rec_drive_connection.constructed_with, (CONFIGURATION, None, True))
        # Get Shared Drive cache called twice - once for initial read and once just before write
        self.assertEqual(cache_rec_drive_connection.get_shared_drive_cache.call_count, 2)
        # Existing recovery file checked for once
        cache_rec_drive_connection.get_file_contents_by_name.assert_called_once_with(
            "shared-drive-drive1",
            CONFIGURATION.google.shared_storage_drive,
            expected=False,
        )
        # Recovery file written then shared drive cache rewritten
        self.assertEqual(cache_rec_drive_connection.write_file.call_count, 2)
        (recovery_write_call, cache_write_call) = cache_rec_drive_connection.write_file.mock_calls

        expected_recovery_file_content = {
            "id": "drive1",
            "name": test_data["shared_drives"][drive1_idx]["name"],
            "drive_permissions": {
                p["id"]: p for p in test_data["shared_drives"][drive1_idx]["permissions"]
            },
            "augmented_permissions": {
                f["file_id"]: f["permissions"]
                for f in test_data["shared_drives"][drive1_idx]["augmented_permissions"]
            },
        }
        self.assertEqual(
            recovery_write_call.args[0], "shared-drive-drive1-permissions-20230517-103000.yaml"
        )
        self.assertEqual(
            recovery_write_call.kwargs["parents"], [CONFIGURATION.google.shared_storage_drive]
        )
        # Need UnsafeLoader to parse freezegun timestamps
        recovery_write_content = yaml.load(recovery_write_call.args[1], Loader=yaml.UnsafeLoader)
        self.assertEqual(recovery_write_content, expected_recovery_file_content)

        expected_updated_cache = test_data
        expected_updated_cache["shared_drives"][drive1_idx].update(
            {
                "last_removed": datetime.now(timezone.utc),
                "last_scanned": None,
                "last_updated": None,
            }
        )
        self.assertEqual(cache_write_call.args[0], "shared_drive_list.yaml")
        self.assertEqual(
            cache_write_call.kwargs["parents"], [CONFIGURATION.google.shared_storage_drive]
        )
        cache_write_content = yaml.load(cache_write_call.args[1], Loader=yaml.UnsafeLoader)
        self.assertEqual(cache_write_content, expected_updated_cache)

        # Admin drive connection created impersonating admin user
        self.assertEqual(
            admin_drive_connection.constructed_with,
            (CONFIGURATION, CONFIGURATION.google.admin_user, True),
        )
        # Drive1 has no management user so one gets added
        admin_drive_connection.add_manager_to_shared_drive.assert_called_once_with(
            "drive1", CONFIGURATION.google.management_user
        )
        # Drive1 permissions are removed
        admin_drive_connection.batch_remove_drive_permissions.assert_called_once_with(
            "drive1", [p["id"] for p in test_data["shared_drives"][drive1_idx]["permissions"]]
        )

        # Management drive connection created impersonating management user
        self.assertEqual(
            management_drive_connection.constructed_with,
            (CONFIGURATION, CONFIGURATION.google.management_user, True),
        )
        # Drive1 augmented permissions are removed
        management_drive_connection.batch_remove_permissions.assert_called_once_with(
            {
                f["file_id"]: [p["id"] for p in f["permissions"]]
                for f in test_data["shared_drives"][drive1_idx]["augmented_permissions"]
            },
            is_shared_drive=True,
        )

    @freeze_time("2024-03-15 10:00:00")
    @mock.patch("gdrivemanager.manage.shared_drives.write_shared_drive_recovery_file")
    @mock.patch("gdrivemanager.manage.shared_drives.write_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.read_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection")
    def test_remove_shared_drive_permissions_not_exist(
        self, mock_drive, mock_read, mock_write, mock_write_recovery
    ):
        existing_cache = {
            "last_updated": datetime(2024, 3, 9, 10, 30, tzinfo=timezone.utc),
            "shared_drives": [
                {
                    "id": "drive1",
                    "name": "Drive 1",
                    "last_updated": datetime(2024, 3, 14, 10, 0, tzinfo=timezone.utc),
                },
            ],
        }
        mock_read.return_value = existing_cache

        # Remove permissions for drive2
        with self.assertLogs(level=logging.ERROR) as cm:
            remove_shared_drive_permissions(CONFIGURATION, "drive2", write_mode=True)

        # Shared drive cache was read
        mock_read.assert_called_once()

        # Does not exist error was logged
        self.assertTrue(
            any("Shared drive 'drive2' not found in shared drive list" in err for err in cm.output)
        )

        # Recovery file was not written
        mock_write_recovery.assert_not_called()

        # No update to shared drive cache
        mock_write.assert_not_called()

    @freeze_time("2024-03-15 10:00:00")
    @mock.patch("gdrivemanager.manage.shared_drives.write_shared_drive_recovery_file")
    @mock.patch("gdrivemanager.manage.shared_drives.write_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.read_shared_drive_cache")
    @mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection")
    def test_remove_shared_drive_permissions_not_recent(
        self, mock_drive, mock_read, mock_write, mock_write_recovery
    ):
        # Drive1 last updated more than 3 days ago
        existing_cache = {
            "last_updated": datetime(2024, 3, 9, 10, 30, tzinfo=timezone.utc),
            "shared_drives": [
                {
                    "id": "drive1",
                    "name": "Drive 1",
                    "last_updated": datetime(2024, 3, 10, 10, 0, tzinfo=timezone.utc),
                },
            ],
        }
        mock_read.return_value = existing_cache

        # Remove permissions for drive1
        with self.assertLogs(level=logging.ERROR) as cm:
            remove_shared_drive_permissions(CONFIGURATION, "drive1", write_mode=True)

        # Shared drive cache was read
        mock_read.assert_called_once()

        # Not recent enough error was logged
        self.assertTrue(
            any(
                "Shared drive 'drive1' - 'Drive 1' has not been recently scanned" in err
                for err in cm.output
            )
        )

        # Recovery file was not written
        mock_write_recovery.assert_not_called()

        # No update to shared drive cache
        mock_write.assert_not_called()

    @freeze_time("2024-03-15 10:00:00")
    def test_write_shared_drive_recovery_file_no_existing_file(self):
        # No existing recovery file
        drive_connection = mock.Mock()
        drive_connection.get_file_contents_by_name.return_value = (None, None)

        permissions_data = {
            "id": "drive1",
            "name": "Drive 1",
            "drive_permissions": {
                "p1": {
                    "id": "p1",
                    "type": "user",
                    "role": "organiser",
                    "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
                },
            },
            "augmented_permissions": {
                "f1": [
                    {
                        "id": "p10",
                        "type": "user",
                        "role": "reader",
                        "emailAddress": "mk2155@gdev.apps.cam.ac.uk",
                    }
                ],
            },
        }

        write_shared_drive_recovery_file(CONFIGURATION, drive_connection, permissions_data)

        # Existing file is checked for
        drive_connection.get_file_contents_by_name.assert_called_once()

        # Written file is unchanged permissions_data
        yaml_content = yaml.dump(permissions_data, Dumper=yaml.CDumper)
        drive_connection.write_file.assert_called_once_with(
            "shared-drive-drive1-permissions-20240315-100000.yaml",
            yaml_content,
            mimetype="text/yaml",
            parents=[CONFIGURATION.google.shared_storage_drive],
        )

    @freeze_time("2024-03-15 10:00:00")
    def test_write_shared_drive_recovery_file_existing_file(self):
        # Existing recovery file
        drive_connection = mock.Mock()
        existing_permissions_data = {
            "id": "drive1",
            "name": "Drive 1",
            "drive_permissions": {
                "p1": {
                    "id": "p1",
                    "type": "user",
                    "role": "organiser",
                    "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
                },
                "p9": {
                    "id": "p9",
                    "type": "user",
                    "role": "reader",
                    "emailAddress": "xyz789@gdev.apps.cam.ac.uk",
                },
            },
            "augmented_permissions": {
                "f1": [
                    {
                        "id": "p10",
                        "type": "user",
                        "role": "reader",
                        "emailAddress": "mk2155@gdev.apps.cam.ac.uk",
                    },
                    {
                        "id": "p20",
                        "type": "user",
                        "role": "writer",
                        "emailAddress": "xyz789@gdev.apps.cam.ac.uk",
                    },
                ],
                "f99": [
                    {
                        "id": "p99",
                        "type": "user",
                        "role": "writer",
                        "emailAddress": "xyz789@gdev.apps.cam.ac.uk",
                    }
                ],
            },
        }

        drive_connection.get_file_contents_by_name.return_value = (
            "shared-drive-drive1-permissions-20240312-100000.yaml",
            yaml.dump(existing_permissions_data, Dumper=yaml.CDumper).encode("utf-8"),
        )

        permissions_data = {
            "id": "drive1",
            "name": "Drive 1",
            "drive_permissions": {
                "p1": {
                    "id": "p1",
                    "type": "user",
                    "role": "fileOrganiser",  # Role has changed for same permission
                    "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
                },
                # New permission
                "p2": {
                    "id": "p2",
                    "type": "user",
                    "role": "reader",
                    "emailAddress": "abc123@gdev.apps.cam.ac.uk",
                },
            },
            "augmented_permissions": {
                "f1": [
                    {
                        "id": "p10",
                        "type": "user",
                        "role": "writer",  # Role has changed for same permission
                        "emailAddress": "mk2155@gdev.apps.cam.ac.uk",
                    },
                    # New augmented permission for file "f1"
                    {
                        "id": "anyoneWithLink",
                        "type": "anyone",
                        "role": "reader",
                    },
                ],
                # New file with augmented permissions
                "f2": [
                    {
                        "id": "p30",
                        "type": "user",
                        "role": "reader",
                        "emailAddress": "def456@gdev.apps.cam.ac.uk",
                    }
                ],
            },
        }

        write_shared_drive_recovery_file(CONFIGURATION, drive_connection, permissions_data)

        # Existing file is checked for
        drive_connection.get_file_contents_by_name.assert_called_once()

        expected_merged_data = {
            "id": "drive1",
            "name": "Drive 1",
            "drive_permissions": {
                "p1": {
                    "id": "p1",
                    "type": "user",
                    "role": "fileOrganiser",  # Updated role from permissions_data
                    "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
                },
                # New permission from permissions_data
                "p2": {
                    "id": "p2",
                    "type": "user",
                    "role": "reader",
                    "emailAddress": "abc123@gdev.apps.cam.ac.uk",
                },
                # Existing permission from recovery file
                "p9": {
                    "id": "p9",
                    "type": "user",
                    "role": "reader",
                    "emailAddress": "xyz789@gdev.apps.cam.ac.uk",
                },
            },
            "augmented_permissions": {
                "f1": [
                    # New augmented permission for file "f1"
                    {
                        "id": "anyoneWithLink",
                        "type": "anyone",
                        "role": "reader",
                    },
                    {
                        "id": "p10",
                        "type": "user",
                        "role": "writer",  # Updated role from permissions_data
                        "emailAddress": "mk2155@gdev.apps.cam.ac.uk",
                    },
                    # Existing augmented permission for "f1" from recovery file
                    {
                        "id": "p20",
                        "type": "user",
                        "role": "writer",
                        "emailAddress": "xyz789@gdev.apps.cam.ac.uk",
                    },
                ],
                # New file with augmented permissions from permissions_data
                "f2": [
                    {
                        "id": "p30",
                        "type": "user",
                        "role": "reader",
                        "emailAddress": "def456@gdev.apps.cam.ac.uk",
                    }
                ],
                # Existing file with augmented permissions from recovery file
                "f99": [
                    {
                        "id": "p99",
                        "type": "user",
                        "role": "writer",
                        "emailAddress": "xyz789@gdev.apps.cam.ac.uk",
                    }
                ],
            },
        }

        # Written file is expected merged permissions_data
        yaml_content = yaml.dump(expected_merged_data, Dumper=yaml.CDumper)
        drive_connection.write_file.assert_called_once_with(
            "shared-drive-drive1-permissions-20240315-100000.yaml",
            yaml_content,
            mimetype="text/yaml",
            parents=[CONFIGURATION.google.shared_storage_drive],
        )

    @freeze_time(shared_drive_list_within_limit)
    @mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection")
    def test_restore_shared_drive_permissions(self, mock_drive):
        # Multiple calls to GoogleDriveConnection are made during the call to
        # restore_shared_drive_permissions() so have a separate mock for each

        # Connection for reading/writing the shared drive cache and reading recovery files
        cache_rec_drive_connection = mock.Mock(name="cache_rec")
        # Connection as admin user for adding/removing from the shared drive's permissions
        # and adding a management user
        admin_drive_connection = mock.Mock(name="admin")
        # Connection as management user for adding augmented permissions
        management_drive_connection = mock.Mock(name="management")

        mock_list = [
            cache_rec_drive_connection,
            admin_drive_connection,
            management_drive_connection,
        ]
        drive_mocks_created = len(mock_list)

        def mock_construct(configuration, impersonate=None, write_mode=False):
            nonlocal mock_list
            # Keep track of which mock was constructed so we can assert on it later
            next_mock = mock_list.pop(0)
            next_mock.constructed_with = (configuration, impersonate, write_mode)
            return next_mock

        mock_drive.side_effect = mock_construct

        # Use standard fixture for shared drive cache (only used to check if drive exists)
        test_data_doc = bytes(datafile("shared_drive_list.yaml"), "utf-8")
        test_data = yaml.load(test_data_doc.decode("utf-8"), Loader=yaml.CLoader)
        cache_rec_drive_connection.get_shared_drive_cache.return_value = test_data_doc

        # Recovery file exists with these permissions (sorted for comparison with actual calls)
        existing_permissions_data = {
            "id": "drive1",
            "name": "Drive 1",
            "drive_permissions": {
                "anyoneWithLink": {
                    "id": "anyoneWithLink",
                    "role": "reader",
                    "type": "anyone",
                },
                "p1": {
                    "emailAddress": "rjg21@gdev.apps.cam.ac.uk",
                    "id": "p1",
                    "role": "organiser",
                    "type": "user",
                },
                "p3": {
                    "emailAddress": "123456@groups.lookup.gdev.apps.cam.ac.uk",
                    "id": "p3",
                    "role": "writer",
                    "type": "group",
                },
            },
            "augmented_permissions": {
                "f1": [
                    {
                        "id": "p10",
                        "role": "reader",
                        "type": "user",
                        "emailAddress": "mk2155@gdev.apps.cam.ac.uk",
                    },
                    {
                        "id": "p11",
                        "role": "writer",
                        "type": "user",
                        "emailAddress": "xyz789@gdev.apps.cam.ac.uk",
                    },
                ],
                "f2": [
                    {
                        "emailAddress": "xyz789@gdev.apps.cam.ac.uk",
                        "id": "p20",
                        "role": "writer",
                        "type": "user",
                    },
                    {
                        "id": "anyoneWithLink",
                        "role": "reader",
                        "type": "anyone",
                    },
                ],
            },
        }
        cache_rec_drive_connection.get_file_contents_by_name.return_value = (
            "shared-drive-drive1-permissions-20230517-103000.yaml",
            yaml.dump(existing_permissions_data, Dumper=yaml.CDumper).encode("utf-8"),
        )

        # Restore permissions to drive1
        restore_shared_drive_permissions(CONFIGURATION, "drive1", write_mode=True)

        # GoogleDriveConnection called as many times as we created mocks
        self.assertEqual(mock_drive.call_count, drive_mocks_created)

        # Cache and recovery drive connection created
        self.assertEqual(cache_rec_drive_connection.constructed_with, (CONFIGURATION, None, True))
        # Get Shared Drive cache called twice - once for initial read and once just before write
        self.assertEqual(cache_rec_drive_connection.get_shared_drive_cache.call_count, 2)
        # Existing recovery file checked for once
        cache_rec_drive_connection.get_file_contents_by_name.assert_called_once_with(
            "shared-drive-drive1",
            CONFIGURATION.google.shared_storage_drive,
            expected=True,
        )
        # Shared drive cache rewritten with last_restored, etc updated
        expected_updated_cache = test_data
        drive1_idx = next(
            i for i, d in enumerate(expected_updated_cache["shared_drives"]) if d["id"] == "drive1"
        )
        expected_updated_cache["shared_drives"][drive1_idx].update(
            {
                "last_restored": datetime.now(timezone.utc),
                "last_scanned": None,
                "last_updated": None,
            }
        )
        cache_rec_drive_connection.write_file.assert_called_once_with(
            "shared_drive_list.yaml",
            yaml.dump(expected_updated_cache, Dumper=yaml.CDumper),
            mimetype="text/yaml",
            parents=[CONFIGURATION.google.shared_storage_drive],
        )

        # Admin drive connection created impersonating admin user
        self.assertEqual(
            admin_drive_connection.constructed_with,
            (CONFIGURATION, CONFIGURATION.google.admin_user, True),
        )
        # Drive1 has no management user so one gets added
        admin_drive_connection.add_manager_to_shared_drive.assert_called_once_with(
            "drive1", CONFIGURATION.google.management_user
        )
        # Drive1 permissions are restored
        admin_drive_connection.batch_create_drive_permissions.assert_called_once_with(
            "drive1", list(existing_permissions_data["drive_permissions"].values())
        )

        # Management drive connection created impersonating management user
        self.assertEqual(
            management_drive_connection.constructed_with,
            (CONFIGURATION, CONFIGURATION.google.management_user, True),
        )
        # Drive1 augmented permissions are restored
        # batch_create_shared_permissions expects an extra object with permissions key
        augmented_permissions = {
            k: {"permissions": v}
            for k, v in existing_permissions_data["augmented_permissions"].items()
        }
        management_drive_connection.batch_create_shared_permissions.assert_called_once_with(
            augmented_permissions,
            is_shared_drive=True,
        )

    @mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection")
    def test_restore_shared_drive_permissions_no_drive(self, mock_drive):
        # Shared drive cache contains just drive1
        shared_drive_cache = {
            "last_updated": datetime(2024, 3, 9, 10, 30, tzinfo=timezone.utc),
            "shared_drives": [
                {
                    "id": "drive1",
                    "name": "Drive 1",
                    "last_updated": datetime(2024, 3, 14, 10, 0, tzinfo=timezone.utc),
                },
            ],
        }
        mock_drive.return_value.get_shared_drive_cache.return_value = yaml.dump(
            shared_drive_cache, Dumper=yaml.CDumper
        ).encode("utf-8")

        # Restore permissions to drive2 (doesn't exist)
        with self.assertLogs(level=logging.ERROR) as cm:
            restore_shared_drive_permissions(CONFIGURATION, "drive2", write_mode=True)

        # Shared drive cache was read
        mock_drive.return_value.get_shared_drive_cache.assert_called_once()

        # Does not exist error was logged
        self.assertTrue(
            any("Shared drive 'drive2' not found in shared drive list" in err for err in cm.output)
        )

        # No permissions were restored
        mock_drive.return_value.batch_create_drive_permissions.assert_not_called()
        mock_drive.return_value.batch_create_shared_permissions.assert_not_called()

        # Shared drive cache wasn't updated
        mock_drive.return_value.write_file.assert_not_called()

    @mock.patch("gdrivemanager.manage.shared_drives.GoogleDriveConnection")
    def test_restore_shared_drive_permissions_no_doc(self, mock_drive):
        # Shared drive cache contains drive1
        shared_drive_cache = {
            "last_updated": datetime(2024, 3, 9, 10, 30, tzinfo=timezone.utc),
            "shared_drives": [
                {
                    "id": "drive1",
                    "name": "Drive 1",
                    "last_updated": datetime(2024, 3, 14, 10, 0, tzinfo=timezone.utc),
                },
            ],
        }
        mock_drive.return_value.get_shared_drive_cache.return_value = yaml.dump(
            shared_drive_cache, Dumper=yaml.CDumper
        ).encode("utf-8")

        # No existing recovery file
        mock_drive.return_value.get_file_contents_by_name.return_value = (None, None)

        # Restore permissions to drive1
        with self.assertLogs(level=logging.ERROR) as cm:
            restore_shared_drive_permissions(CONFIGURATION, "drive1", write_mode=True)

        # Shared drive cache was read
        mock_drive.return_value.get_shared_drive_cache.assert_called_once()

        # Does not exist error was logged
        self.assertTrue(
            any("No recovery file found for shared drive 'drive1'" in err for err in cm.output)
        )

        # No permissions were restored
        mock_drive.return_value.batch_create_drive_permissions.assert_not_called()
        mock_drive.return_value.batch_create_shared_permissions.assert_not_called()

        # Shared drive cache wasn't updated
        mock_drive.return_value.write_file.assert_not_called()
