import json
from datetime import datetime, timezone
from unittest import TestCase, mock

from gdrivemanager.manage.cleanup import clean_recovery_drive
from gdrivemanager.tests import CONFIGURATION, datafile


class ManageCleanupTests(TestCase):
    frozen_time = datetime(2023, 1, 1, 1, 30, tzinfo=timezone.utc)

    @mock.patch("gdrivemanager.manage.cleanup.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.cleanup.GoogleDriveConnection")
    def test_clean_recovery_drive_user_not_suspended(self, mock_drive, mock_directory):
        mock_drive.return_value.get_all_files_in_drive_metadata.side_effect = [
            json.loads(datafile("drive-permissions-documents.json"))["files"],
        ]
        # In both cases return user who is not suspended.
        mock_directory.return_value.retrieve_users.side_effect = [
            json.loads(datafile("directory-mixed-response.json"))["users"],
            json.loads(datafile("directory-mixed-response.json"))["users"],
        ]
        # Merging 'permissions-doc-folder.yaml' and 'permissions-doc-file.yaml' should result in
        # 'permissions-doc.yaml'
        mock_drive.return_value.get_file_contents_by_id.side_effect = [
            bytes(datafile("permissions-doc-folder.yaml"), "utf-8"),
            # One other blank (for simplicity).
            bytes("{}", "utf-8"),
            bytes(datafile("permissions-doc-file.yaml"), "utf-8"),
        ]

        clean_recovery_drive(CONFIGURATION)

        # Get list of oldest metadata files to clean
        mock_drive.return_value.get_all_files_in_drive_metadata.assert_called_once_with(
            CONFIGURATION.google.shared_storage_drive,
        )
        # Search for user CRSids from 'drive-permissions-documents.json'
        mock_directory.return_value.retrieve_users.assert_has_calls(
            [
                mock.call(suspended_only=False),
                mock.call(show_deleted=True, suspended_only=False),
            ]
        )
        mock_drive.return_value.update_file_metadata.assert_has_calls(
            [
                # File name is changed to HISTORIC
                mock.call(
                    "af123-300100-shared-permissions-20230101-010000.yaml",
                    {"name": "HISTORIC-af123-300100-shared-permissions-20230101-010000.yaml"},
                    CONFIGURATION.google.shared_storage_drive,
                ),
            ]
        )
        # Get contents of oldest document being processed - f3 - and then the contents of all the
        # other document ids
        mock_drive.return_value.get_file_contents_by_id.assert_has_calls(
            [
                mock.call("f3", drive_id=CONFIGURATION.google.shared_storage_drive),
                mock.call("f1", drive_id=CONFIGURATION.google.shared_storage_drive),
                mock.call("f2", drive_id=CONFIGURATION.google.shared_storage_drive),
            ]
        )
        # Write merged file
        mock_drive.return_value.write_file.assert_called_once_with(
            "HISTORIC-af123-300100-shared-permissions-20230101-010000.yaml",
            datafile("permissions-doc.yaml"),
            mimetype="text/yaml",
            parents=[CONFIGURATION.google.shared_storage_drive],
        )
        # Trash other files
        mock_drive.return_value.trash_file.assert_has_calls(
            [
                mock.call("f1", "af123-100100-shared-permissions-20230101-010000.yaml"),
                mock.call("f2", "HISTORIC-af123-200100-shared-permissions-20230101-010000.yaml"),
            ]
        )

    @mock.patch("gdrivemanager.manage.cleanup.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.cleanup.GoogleDriveConnection")
    def test_clean_recovery_drive_user_not_suspended_single_file(self, mock_drive, mock_directory):
        mock_drive.return_value.get_all_files_in_drive_metadata.side_effect = [
            json.loads(datafile("drive-permissions-documents-two.json"))["files"],
        ]
        # In both cases return user who is not suspended.
        mock_directory.return_value.retrieve_users.side_effect = [
            json.loads(datafile("directory-mixed-response.json"))["users"],
            json.loads(datafile("directory-mixed-response.json"))["users"],
        ]

        clean_recovery_drive(CONFIGURATION)

        # Get list of oldest metadata files to clean
        mock_drive.return_value.get_all_files_in_drive_metadata.assert_called_once_with(
            CONFIGURATION.google.shared_storage_drive,
        )
        # Both files should be unchanged, but an update metadata call is made to bump the
        # modifiedAt time
        mock_drive.return_value.update_file_metadata.assert_has_calls(
            [
                # File name is changed to HISTORIC
                mock.call(
                    "HISTORIC-af123-200100-shared-permissions-20230101-010000.yaml",
                    {"name": "HISTORIC-af123-200100-shared-permissions-20230101-010000.yaml"},
                    CONFIGURATION.google.shared_storage_drive,
                ),
            ]
        )
        # Trash other files
        mock_drive.return_value.trash_file.assert_has_calls(
            [
                mock.call("f3", "xy123-300100-shared-permissions-20230101-010000.yaml"),
                mock.call("f1", "xy123-100100-shared-permissions-20230101-010000.yaml"),
            ]
        )

    @mock.patch("gdrivemanager.manage.cleanup.GoogleDirectoryConnection")
    @mock.patch("gdrivemanager.manage.cleanup.GoogleDriveConnection")
    def test_clean_recovery_drive_user_suspended(self, mock_drive, mock_directory):
        mock_drive.return_value.get_all_files_in_drive_metadata.side_effect = [
            json.loads(datafile("drive-permissions-documents.json"))["files"],
        ]
        mock_directory.return_value.retrieve_users.side_effect = [
            json.loads(datafile("directory-ordinary-response.json"))["users"],
            json.loads(datafile("directory-ordinary-response.json"))["users"],
        ]
        # Merging 'permissions-doc-folder.yaml' and 'permissions-doc-file.yaml' should result in
        # 'permissions-doc.yaml'
        mock_drive.return_value.get_file_contents_by_id.side_effect = [
            # First three returns are for first file - f1 - and its other associated live files
            # - f3 & f4
            bytes(datafile("permissions-doc-folder.yaml"), "utf-8"),
            bytes(datafile("permissions-doc-file.yaml"), "utf-8"),
        ]

        clean_recovery_drive(CONFIGURATION)

        # Get list of oldest metadata files to clean
        mock_drive.return_value.get_all_files_in_drive_metadata.assert_called_once_with(
            CONFIGURATION.google.shared_storage_drive,
        )
        mock_directory.return_value.retrieve_users.assert_has_calls(
            [
                mock.call(suspended_only=False),
                mock.call(show_deleted=True, suspended_only=False),
            ]
        )
        mock_drive.return_value.update_file_metadata.assert_has_calls(
            [
                mock.call(
                    "HISTORIC-af123-200100-shared-permissions-20230101-010000.yaml",
                    {"name": "HISTORIC-af123-200100-shared-permissions-20230101-010000.yaml"},
                    CONFIGURATION.google.shared_storage_drive,
                ),
            ]
        )
        # Get contents of f1, then related f3 & f4. Then get contents of f2 and related f5 & f6.
        mock_drive.return_value.get_file_contents_by_id.assert_has_calls(
            [
                mock.call("f3", drive_id=CONFIGURATION.google.shared_storage_drive),
                mock.call("f1", drive_id=CONFIGURATION.google.shared_storage_drive),
            ]
        )
        # Write both merged files
        mock_drive.return_value.write_file.assert_called_once_with(
            "af123-300100-shared-permissions-20230101-010000.yaml",
            datafile("permissions-doc.yaml"),
            mimetype="text/yaml",
            parents=[CONFIGURATION.google.shared_storage_drive],
        )
        # Trash other files
        mock_drive.return_value.trash_file.assert_has_calls(
            [
                mock.call("f1", "af123-100100-shared-permissions-20230101-010000.yaml"),
            ]
        )
