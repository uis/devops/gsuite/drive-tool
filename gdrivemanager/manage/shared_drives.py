import logging
from datetime import datetime, timedelta, timezone

import yaml
from google.auth.exceptions import RefreshError
from googleapiclient.errors import HttpError

from ..gapi import GoogleDriveConnection
from ..utils import human_size

LOG = logging.getLogger(__name__)


# Management Command Function - `gdrivemanager shared-drive-usage`


def scan_shared_drives(configuration, skip_rebuild=False, drive_id=None, write_mode=False):
    LOG.info(
        f'Running shared drive usage scanning in {"WRITE" if write_mode else "READ ONLY"} mode.'
    )

    # Max duration of the operation includes potential time spent rebuilding list of shared drives
    max_duration_mins = configuration.limits.max_shared_drive_usage_duration
    max_duration = datetime.now(timezone.utc) + timedelta(minutes=max_duration_mins)
    LOG.info(f"Max duration {max_duration_mins} mins - {max_duration}")

    # To read/write the cached shared drive list we need to connect as just the
    # service account
    cache_drive_connection = GoogleDriveConnection(configuration, write_mode=write_mode)

    # We need to connect as admin account to read shared drives and their permissions, and
    # to potentially add a manager to a shared drive
    if configuration.google.admin_user is None:
        LOG.error(
            "No google.admin_email set in configuration, cannot list shared drives without "
            "impersonating a domain admin"
        )
        return
    shared_drive_connection = GoogleDriveConnection(
        configuration, configuration.google.admin_user, write_mode=False
    )

    # Get the cached shared drive list and check if it needs updating
    shared_drives_cache = read_shared_drive_cache(cache_drive_connection)
    if shared_drives_cache is None:
        LOG.warning("No shared drive list cache found, starting with empty list!")
        shared_drives_cache = {"last_updated": None, "shared_drives": []}

    cache_out_of_date = False
    last_updated = shared_drives_cache.get("last_updated")
    if last_updated is None or last_updated < datetime.now(timezone.utc) - timedelta(
        days=configuration.google.shared_drive_list_cache_days
    ):
        LOG.warning(f"Shared drive list cache is out of date (last updated {last_updated})")
        cache_out_of_date = True

    # Rebuild the cached shared drive list unless --skip-rebuild is set
    if skip_rebuild:
        if cache_out_of_date:
            LOG.warning(
                "Shared drive list cache is out of date but --skip-rebuild is set, "
                "scanning may attempt to use stale data."
            )
    else:
        shared_drives_cache = rebuild_shared_drive_cache(
            configuration, shared_drive_connection, shared_drives_cache
        )
        # Write of the cache as we've just spent time rebuilding the list
        write_shared_drive_cache(configuration, cache_drive_connection, shared_drives_cache)

    if drive_id is not None:
        # If a specific drive_id is provided, only update that drive
        shared_drives_to_update = [
            drive for drive in shared_drives_cache["shared_drives"] if drive["id"] == drive_id
        ]
        if len(shared_drives_to_update) == 0:
            LOG.error(f"Shared drive {drive_id!r} not found in shared drive list")
            return
        LOG.info(
            f"Updating usage for shared drive {drive_id!r} "
            f"({shared_drives_to_update[0]['name']!r})"
        )
    else:
        # Get the list of shared drives that need their usage updating
        shared_drives_to_update = get_shared_drives_to_update(configuration, shared_drives_cache)
        if len(shared_drives_to_update) == 0:
            LOG.info("No shared drives need their usage updating")
            return
        LOG.info(f"Updating usage for {len(shared_drives_to_update)} shared drives")

    # Add usage to each shared drive, updating last_scanned, last_scan_success and if successful,
    # last_updated
    drive_count = 0
    manager = None
    manager_drive_connection = None
    for drive in shared_drives_to_update:
        # Abort if we've hit the maximum duration for this operation
        if datetime.now(timezone.utc) > max_duration:
            LOG.warning(f"Max scan duration of {max_duration_mins} mins reached.")
            break
        drive_count += 1

        # First try and update the root shared drive permissions
        try:
            drive["permissions"] = shared_drive_connection.read_shared_drive_permissions(
                drive["id"]
            )
        except HttpError:
            LOG.error(
                f"Unable to read permissions for shared drive {drive['id']!r} - {drive['name']!r}"
            )
            # Don't update the last_updated time for this shared drive and record that the last
            # scan was a failure
            drive["last_scan_success"] = False
            drive["last_scanned"] = datetime.now(timezone.utc)
            update_drive_in_cache(configuration, cache_drive_connection, drive)
            continue

        # To list the files in a shared drive we need to connect as a user with
        # access to it.
        new_manager, manager_drive_connection = connect_as_manager(
            configuration, drive, manager, manager_drive_connection
        )

        if new_manager is not None:
            manager = new_manager
            LOG.info(
                f"Getting usage for shared drive {drive['name']!r} as {manager.split('@')[0]!r}"
            )
            num_files, usage, augmented_file_ids = (
                manager_drive_connection.get_shared_drive_usage_and_augmented_perms(drive["id"])
            )
            LOG.info(
                f"Found {num_files} files using {human_size(usage)}, {len(augmented_file_ids)} "
                "with augmented permissions."
            )

            drive["usage"] = usage
            drive["files"] = num_files

            if augmented_file_ids:
                # permissions ids of the permissions directly on the drive so we can ignore them
                # when getting the file augmented permissions
                drive_permission_ids = {p["id"] for p in drive.get("permissions", [])}

                drive["augmented_permissions"] = (
                    manager_drive_connection.get_shared_drive_augmented_permissions(
                        augmented_file_ids, drive_permission_ids
                    )
                )
                LOG.info(
                    f"Found {len(drive['augmented_permissions'])} files with non-inherited "
                    "augmented permissions."
                )
            else:
                # Clear any previous augmented permissions
                drive["augmented_permissions"] = None

            # Update the last_updated time for this shared drive as we successfully scanned it
            # and record that the last scan was a success
            drive["last_updated"] = datetime.now(timezone.utc)
            drive["last_scan_success"] = True
        else:
            # We've been unable to connect as a manager so if we have one, add the management_user
            # to the shared drive. This isn't instant so we'll still fail the scan and hope for
            # better luck next time.
            # Cannot reuse shared_drive_connection as it's always read-only
            if configuration.google.management_user is not None:
                add_manager_drive_connection = GoogleDriveConnection(
                    configuration, configuration.google.admin_user, write_mode=write_mode
                )
                try:
                    add_manager_drive_connection.add_manager_to_shared_drive(
                        drive["id"], configuration.google.management_user
                    )
                except HttpError:
                    # If we fail to add the management_user, log the error and but continue
                    LOG.error(
                        f"Unable to add management_user {configuration.google.management_user} to "
                        f"shared drive {drive['id']!r} - {drive['name']!r}"
                    )

            # Don't update the last_updated time for this shared drive and record that the last
            # scan was a failure
            drive["last_scan_success"] = False

        # Even if we failed, update it's last_scanned so we don't keep trying to update this drive.
        drive["last_scanned"] = datetime.now(timezone.utc)

        # Save to cache as we go as there is the potential for drop out part way through scanning
        # a large number of files in a shared drive.
        update_drive_in_cache(configuration, cache_drive_connection, drive)

    # Flush any pending saves of the cache
    flush_shared_drive_cache(configuration, cache_drive_connection)

    if drive_count == len(shared_drives_to_update):
        LOG.info(f"Updated usage for {len(shared_drives_to_update)} shared drives")
    else:
        LOG.info(
            f"Updated usage for {drive_count} out of {len(shared_drives_to_update)} shared drives"
        )


def read_shared_drive_cache(drive_connection):
    # Find cached shared drive list in shared_storage_drive
    shared_drive_doc = drive_connection.get_shared_drive_cache()
    if shared_drive_doc is None:
        return None
    return yaml.load(shared_drive_doc.decode("utf-8"), Loader=yaml.CLoader)


def rebuild_shared_drive_cache(configuration, drive_connection, existing_cache):
    LOG.info("Rebuilding shared drive list")
    start_time = datetime.now(timezone.utc)
    shared_drive_list = drive_connection.retrieve_shared_drives()
    cache_by_drive_id = {drive["id"]: drive for drive in existing_cache.get("shared_drives", [])}
    for idx, drive in enumerate(shared_drive_list):
        # Add previous permissions, usage, last_updated, etc (anything already in the cache entry)
        # to each shared drive in the new list (with new data taking precedence)
        if drive["id"] in cache_by_drive_id:
            drive_cache = cache_by_drive_id[drive["id"]]
            shared_drive_list[idx] = {**drive_cache, **drive}
    # Save new cached list (with previous usage and last_updated maintained)
    shared_drives_cache = {
        "last_updated": start_time,
        "shared_drives": shared_drive_list,
    }
    return shared_drives_cache


def update_drive_in_cache(configuration, drive_connection, drive=None, force=False):
    # Uses "static" variables: `drive_updates` being a dict of drive entries to be added/updated in
    # the shared drive cache "shared_drives" list, and `next_update` being the time after which the
    # updates are to be written to the cache (e.g write cache timeout).

    # Initialise the static variables if they doesn't exist.
    # Also, set the next update time if the list of updates is empty.
    if not hasattr(update_drive_in_cache, "drive_updates"):
        update_drive_in_cache.drive_updates = {}
    if (
        not hasattr(update_drive_in_cache, "next_update")
        or not update_drive_in_cache.drive_updates
    ):
        update_drive_in_cache.next_update = datetime.now(timezone.utc) + timedelta(
            minutes=configuration.google.shared_drive_usage_cache_update_frequency
        )

    # Add the drive to the list of updates (if given)
    if drive is not None:
        update_drive_in_cache.drive_updates[drive["id"]] = drive

    if force or (update_drive_in_cache.next_update < datetime.now(timezone.utc)):
        LOG.info(f"Writing {len(update_drive_in_cache.drive_updates)} drive updates to cache")
        # Read the current cache, update it and write
        shared_drives_cache = read_shared_drive_cache(drive_connection)
        if shared_drives_cache is None:
            raise RuntimeError("No shared drive list cache found, aborting update!")
        for idx, drive in enumerate(shared_drives_cache["shared_drives"]):
            drive_id = drive["id"]
            if drive_id in update_drive_in_cache.drive_updates:
                # Update the existing drives in the cache
                shared_drives_cache["shared_drives"][idx] = update_drive_in_cache.drive_updates[
                    drive_id
                ]
                # Remove it from the updates list
                del update_drive_in_cache.drive_updates[drive_id]
        # Remaining drive updates are new drives to add to the cache (shouldn't really happen)
        if update_drive_in_cache.drive_updates:
            shared_drives_cache["shared_drives"].extend(
                update_drive_in_cache.drive_updates.values()
            )
        update_drive_in_cache.drive_updates = {}
        # Write the updated cache
        write_shared_drive_cache(configuration, drive_connection, shared_drives_cache)


def write_shared_drive_cache(configuration, drive_connection, shared_drives_cache):
    drive_connection.write_file(
        configuration.google.shared_drive_list_file,
        yaml.dump(shared_drives_cache, Dumper=yaml.CDumper),
        mimetype="text/yaml",
        parents=[configuration.google.shared_storage_drive],
    )


def flush_shared_drive_cache(configuration, drive_connection):
    # Flush any pending drive updates to the shared drive cache
    if (
        hasattr(update_drive_in_cache, "drive_updates")
        and len(update_drive_in_cache.drive_updates) > 0
    ):
        update_drive_in_cache(configuration, drive_connection, drive=None, force=True)


def get_shared_drives_to_update(configuration, shared_drives_cache):
    # Determine which shared drive "usage"s need updating
    cache_cutoff = datetime.now(timezone.utc) - timedelta(
        days=configuration.google.shared_drive_usage_cache_days
    )

    def should_be_scanned(drive):
        """Determines if a drive should be scanned based on its last scanned and
        last updated timestamps compared to a cache cutoff. For compatibility with
        versions prior to v1.1.4."""

        last_scanned = drive.get("last_scanned")
        last_updated = drive.get("last_updated")

        # Scan if never scanned and either never updated or updated before cutoff
        if last_scanned is None:
            return (last_updated is None) or (last_updated < cache_cutoff)

        # If scanned, rescan if last scan was before the cache cutoff
        return last_scanned < cache_cutoff

    shared_drives_to_update = [
        drive for drive in shared_drives_cache["shared_drives"] if should_be_scanned(drive)
    ]
    if len(shared_drives_to_update) == 0:
        return []
    # sort oldest first (None is considered older than any specified date) and then by id (for
    # consistency in tests)
    min_date = datetime.min.replace(tzinfo=timezone.utc)
    shared_drives_to_update.sort(
        key=lambda x: (x.get("last_scanned") or x.get("last_updated") or min_date, x["id"])
    )
    return shared_drives_to_update


def connect_as_manager(configuration, drive, current_manager, current_connection):
    # Find a manager or content manager of the shared drive (with domain email address) or use the
    # management_user if set
    managers = [
        permission["emailAddress"]
        for permission in drive["permissions"]
        if permission["role"] in ["organizer", "fileOrganizer"]
        and (
            permission["emailAddress"].endswith("@" + configuration.google.domain_name)
            or (
                configuration.google.management_user is not None
                and permission["emailAddress"] == configuration.google.management_user
            )
        )
    ]
    if len(managers) == 0:
        LOG.error(f"Unable to find a manager of shared drive {drive['id']!r} - {drive['name']!r}")
        return None, current_connection

    # If we are already impersonating one of them, continue to use it
    if current_manager is not None and current_manager in managers:
        LOG.info(f"Reusing existing connection as {current_manager}")
        return current_manager, current_connection

    # Try to connect as each manager in turn, stopping when we find one that works.
    manager = None
    for m in managers:
        try:
            manager_drive_connection = GoogleDriveConnection(configuration, m, write_mode=False)
            # We need to actually use the connection to test if it works and catch a failure.
            manager_drive_connection.check_drive_access(drive["id"])
            manager = m
            break
        except (RefreshError, HttpError):
            LOG.warning(
                f"Unable to connect as {m} to get usage for shared drive "
                f"{drive['id']!r} - {drive['name']!r}"
            )
    if manager is None:
        LOG.error(
            f"Unable to connect as any of the managers of shared drive "
            f"{drive['id']!r} - {drive['name']!r}"
        )
        return None, current_connection
    # New manager and connection established
    return manager, manager_drive_connection


# Management Command Function - `gdrivemanager shared-drive-permissions remove`


def remove_shared_drive_permissions(configuration, drive_id, write_mode=False):
    # To read/write the cached shared drive list and recovery documents we need to connect as just
    # the service account
    cache_rec_drive_connection = GoogleDriveConnection(configuration, write_mode=write_mode)

    # Get the cached shared drive list and check if the drive specified is in it
    shared_drives_cache = read_shared_drive_cache(cache_rec_drive_connection)
    shared_drives = shared_drives_cache.get("shared_drives", [])
    shared_drive = next((drive for drive in shared_drives if drive["id"] == drive_id), None)
    if shared_drive is None:
        LOG.error(f"Shared drive {drive_id!r} not found in shared drive list")
        return
    LOG.info(f"Removing permissions from shared drive {drive_id!r} - {shared_drive['name']!r}")

    # Safety check to ensure we don't remove permissions from a drive that hasn't been recently
    # scanned and successfully updated
    cut_off = datetime.now(timezone.utc) - timedelta(
        days=configuration.limits.min_shared_drive_update_days
    )
    if shared_drive.get("last_updated") is None or shared_drive["last_updated"] < cut_off:
        LOG.error(
            f"Shared drive {drive_id!r} - {shared_drive['name']!r} has not been recently scanned. "
            "Consider running `shared-drive-usage` (targeting this drive) first."
        )
        return

    # To change the permissions on the shared drive we need to connect as admin account
    if configuration.google.admin_user is None:
        LOG.error(
            "No google.admin_email set in configuration, cannot add the management user to or "
            "remove permissions from the shared drive without impersonating a domain admin"
        )
        return
    admin_drive_connection = GoogleDriveConnection(
        configuration, configuration.google.admin_user, write_mode=write_mode
    )

    # Get permissions to remove after storing them in a recovery file
    permissions_data = {
        "id": drive_id,
        "name": shared_drive["name"],
        "drive_permissions": {p["id"]: p for p in (shared_drive.get("permissions") or [])},
        "augmented_permissions": {
            f["file_id"]: f["permissions"]
            for f in (shared_drive.get("augmented_permissions") or [])
        },
    }
    LOG.info(
        f"Found {len(permissions_data['drive_permissions'])} drive permissions and "
        f"{len(permissions_data['augmented_permissions'])} augmented files with permissions"
    )

    if permissions_data["augmented_permissions"]:
        if configuration.google.management_user is None:
            LOG.error(
                "No google.management_user set in configuration, this is needed for shared drive "
                "augmented file permission handling"
            )
            return

        # To remove augmented permissions we need to add a management user to the shared drive,
        # if we haven't already
        has_management_user = any(
            [
                p
                for p in permissions_data["drive_permissions"].values()
                if p.get("emailAddress") == configuration.google.management_user
            ]
        )
        if not has_management_user:
            admin_drive_connection.add_manager_to_shared_drive(
                drive_id, configuration.google.management_user
            )

    # Ensure the shared drive has a recovery file before removing permissions
    write_shared_drive_recovery_file(configuration, cache_rec_drive_connection, permissions_data)

    # Remove augmented permissions from the shared drive
    if permissions_data["augmented_permissions"]:
        management_drive_connection = GoogleDriveConnection(
            configuration, configuration.google.management_user, write_mode=write_mode
        )
        # batch_remove_permissions expects a dict of format:
        # {file_id: [permission_id, ...]} so create and pass that
        management_drive_connection.batch_remove_permissions(
            {
                f: [p["id"] for p in ps]
                for f, ps in permissions_data["augmented_permissions"].items()
            },
            is_shared_drive=True,
        )
    else:
        LOG.info("No augmented file permissions to remove from shared drive")

    # Remove drive permissions from the shared drive (except for the management user)
    if permissions_data["drive_permissions"]:
        permissions_ids_to_remove = [
            pid
            for pid, p in permissions_data["drive_permissions"].items()
            if p.get("emailAddress") != configuration.google.management_user
        ]
        admin_drive_connection.batch_remove_drive_permissions(drive_id, permissions_ids_to_remove)
    else:
        LOG.info("No drive permissions to remove from shared drive")

    # Mark the drive as having had its permissions removed (set `last_removed`) and make it
    # be scanned for usage next (clear both `last_scanned` and `last_updated`)
    shared_drive["last_removed"] = datetime.now(timezone.utc)
    shared_drive["last_scanned"] = None
    shared_drive["last_updated"] = None
    update_drive_in_cache(configuration, cache_rec_drive_connection, shared_drive, force=True)
    LOG.info(f"Permissions removed from shared drive {drive_id!r} - {shared_drive['name']!r}")


def write_shared_drive_recovery_file(configuration, drive_connection, permissions_data):
    # Look for a existing recovery file for the shared drive
    doc_name, raw_doc = drive_connection.get_file_contents_by_name(
        f"shared-drive-{permissions_data['id']}",
        configuration.google.shared_storage_drive,
        expected=False,
    )
    if doc_name is not None:
        LOG.info(f"Existing recovery file found {doc_name!r} - merging")
        try:
            recovery_doc = yaml.load(raw_doc.decode("utf-8"), Loader=yaml.CLoader)
            if recovery_doc["id"] != permissions_data["id"]:
                raise RuntimeError(
                    f"Recovery document {doc_name!r} has a different shared drive id "
                    f"({recovery_doc['id']!r})"
                )
            LOG.info(
                f"Merging {len(recovery_doc['drive_permissions'])} drive permissions and "
                f"{len(recovery_doc['augmented_permissions'])} augmented files with permissions"
            )
            # Drive permissions a single depth dictionary so can just be updated (new permissions
            # will overwrite those in recovery file)
            recovery_doc["drive_permissions"].update(permissions_data["drive_permissions"])
            permissions_data["drive_permissions"] = recovery_doc["drive_permissions"]

            # Augmented permissions is a dictionary of lists so need to be merged carefully
            # e.g. augmented files in the recovery file but not in permissions_data are added,
            # and if the file is in both then only permissions not already in permissions_data are
            # appended.
            for file_id, file_permissions in recovery_doc["augmented_permissions"].items():
                if file_id in permissions_data["augmented_permissions"]:
                    permissions_ids = {
                        p["id"] for p in permissions_data["augmented_permissions"][file_id]
                    }
                    for permission in file_permissions:
                        if permission["id"] not in permissions_ids:
                            permissions_data["augmented_permissions"][file_id].append(permission)
                else:
                    permissions_data["augmented_permissions"][file_id] = file_permissions
        except yaml.YAMLError:
            raise RuntimeError("Previously recorded permissions document was not valid YAML.")
        except KeyError:
            raise RuntimeError(
                "Previously recorded permissions document did not contain the expected data."
            )

        LOG.info(
            f"Merge resulted in {len(permissions_data['drive_permissions'])} drive permissions and"
            f" {len(permissions_data['augmented_permissions'])} augmented files with permissions"
        )

    # Order the permissions data (for consistency in tests)
    permissions_data["drive_permissions"] = {
        k: v for k, v in sorted(permissions_data["drive_permissions"].items())
    }
    permissions_data["augmented_permissions"] = {
        k: sorted(v, key=lambda p: p["id"])
        for k, v in sorted(permissions_data["augmented_permissions"].items())
    }

    # Write the recovery file
    recovery_file_name = (
        f"shared-drive-{permissions_data['id']}-permissions-"
        f"{datetime.now().strftime('%Y%m%d-%H%M%S')}.yaml"
    )
    drive_connection.write_file(
        recovery_file_name,
        yaml.dump(permissions_data, Dumper=yaml.CDumper),
        mimetype="text/yaml",
        parents=[configuration.google.shared_storage_drive],
    )


# Management Command Function - `gdrivemanager shared-drive-permissions restore`


def restore_shared_drive_permissions(configuration, drive_id, write_mode=False):
    # To read/write the cached shared drive list and recovery documents we need to connect as just
    # the service account
    cache_rec_drive_connection = GoogleDriveConnection(configuration, write_mode=write_mode)

    # Get the cached shared drive list and check if the drive specified is in it
    shared_drives_cache = read_shared_drive_cache(cache_rec_drive_connection)
    shared_drives = shared_drives_cache.get("shared_drives", [])
    shared_drive = next((drive for drive in shared_drives if drive["id"] == drive_id), None)
    if shared_drive is None:
        LOG.error(f"Shared drive {drive_id!r} not found in shared drive list")
        return

    # Look for a existing recovery file for the shared drive
    doc_name, raw_doc = cache_rec_drive_connection.get_file_contents_by_name(
        f"shared-drive-{drive_id}",
        configuration.google.shared_storage_drive,
        expected=True,
    )
    if doc_name is None:
        LOG.error(f"No recovery file found for shared drive {drive_id!r}")
        return

    recovery_data = {
        "drive_permissions": {},
        "augmented_permissions": {},
    }
    LOG.info(f"Existing recovery file found {doc_name!r} - checking")
    try:
        recovery_doc = yaml.load(raw_doc.decode("utf-8"), Loader=yaml.CLoader)
        if recovery_doc["id"] != drive_id:
            raise RuntimeError(
                f"Recovery document {doc_name!r} has a different shared drive id "
                f"({recovery_doc['id']!r})"
            )
        recovery_data["drive_permissions"].update(recovery_doc["drive_permissions"])
        recovery_data["augmented_permissions"].update(recovery_doc["augmented_permissions"])
    except yaml.YAMLError:
        raise RuntimeError("Previously recorded permissions document was not valid YAML.")
    except KeyError:
        raise RuntimeError(
            "Previously recorded permissions document did not contain the expected data."
        )

    LOG.info(
        f"Found {len(recovery_data['drive_permissions'])} drive permissions and "
        f"{len(recovery_data['augmented_permissions'])} augmented files with permissions"
    )

    LOG.info(f"Restoring permissions from shared drive {drive_id!r} - {shared_drive['name']!r}")

    # To change the permissions on the shared drive we need to connect as admin account
    if configuration.google.admin_user is None:
        LOG.error(
            "No google.admin_email set in configuration, cannot add the management user to or "
            "remove permissions from the shared drive without impersonating a domain admin"
        )
        return
    admin_drive_connection = GoogleDriveConnection(
        configuration, configuration.google.admin_user, write_mode=write_mode
    )

    if recovery_doc["augmented_permissions"]:
        if configuration.google.management_user is None:
            LOG.error(
                "No google.management_user set in configuration, this is needed for shared drive "
                "augmented file permission handling"
            )
            return

        # To restore augmented permissions we need to add a management user to the shared drive,
        # if we haven't already
        has_management_user = any(
            [
                p
                for p in recovery_doc["drive_permissions"].values()
                if p.get("emailAddress") == configuration.google.management_user
            ]
        )
        if not has_management_user:
            admin_drive_connection.add_manager_to_shared_drive(
                drive_id, configuration.google.management_user
            )

    # Add top level permissions to the shared drive
    if recovery_data["drive_permissions"]:
        admin_drive_connection.batch_create_drive_permissions(
            drive_id, list(recovery_data["drive_permissions"].values())
        )
    else:
        LOG.info("No drive permissions to restore to shared drive")

    # Add augmented permissions to the shared drive (as management user)
    if recovery_data["augmented_permissions"]:
        management_drive_connection = GoogleDriveConnection(
            configuration, configuration.google.management_user, write_mode=write_mode
        )
        # batch_create_shared_permissions expects a dict of format:
        # {file_id: {"permissions": [...]}} so add that extra layer
        management_drive_connection.batch_create_shared_permissions(
            {f: {"permissions": ps} for f, ps in recovery_data["augmented_permissions"].items()},
            is_shared_drive=True,
        )
    else:
        LOG.info("No augmented file permissions to restore to shared drive")

    # Mark the drive as having had its permissions restored (set `last_restored`) and make it
    # be scanned for usage next (clear both `last_scanned` and `last_updated`)
    shared_drive["last_restored"] = datetime.now(timezone.utc)
    shared_drive["last_scanned"] = None
    shared_drive["last_updated"] = None
    update_drive_in_cache(configuration, cache_rec_drive_connection, shared_drive, force=True)
    LOG.info(f"Permissions restored to shared drive {drive_id!r} - {shared_drive['name']!r}")
