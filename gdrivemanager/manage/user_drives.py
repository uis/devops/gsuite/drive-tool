import logging
from datetime import datetime, timedelta, timezone
from pprint import pformat

import yaml
from googleapiclient.errors import HttpError

from ..gapi import GoogleDirectoryConnection, GoogleDriveConnection, ListTimeoutError
from ..utils import (
    FOLDER_MIMETYPE,
    UcamField,
    file_is_shared,
    sort_files_for_permission_removal,
    user_email_is_crsid,
    user_marked_for_manual_attention,
    user_marked_for_recovery,
    user_marked_for_scan,
)

LOG = logging.getLogger(__name__)


class UserProcessingError(Exception):
    """
    Exception class for an error when processing a single user. Expect that general processing
    can continue.
    """

    pass


# Management Command Function - `gdrivemanager scan`


def scan(configuration, write_mode=False):
    """
    Command function to go through all google users, for each valid user who is suspended and has
    been marked as needing to be scanned:
    - Print the user to the command line.

    """
    LOG.info(f'Running SCAN in {"WRITE" if write_mode else "READ ONLY"} mode.')

    max_users = configuration.limits.max_scanned_users
    directory_connection = GoogleDirectoryConnection(configuration, write_mode=write_mode)
    all_google_users = directory_connection.retrieve_users()

    LOG.info(f"TOTAL users fetched: {len(all_google_users)}")

    filtered_users = [
        user
        for user in all_google_users
        if all(
            [
                user_email_is_crsid(user),
                user_marked_for_scan(user),
            ]
        )
    ]

    LOG.info(f"TOTAL users marked for scan: {len(filtered_users)}.")
    LOG.info(f"Maximum users to scan this run: {max_users}.")
    shared_drive_connection = GoogleDriveConnection(configuration, write_mode=write_mode)
    shared_drive_id = configuration.google.shared_storage_drive

    max_duration_mins = configuration.limits.max_total_scan_duration
    max_duration = datetime.now(timezone.utc) + timedelta(minutes=max_duration_mins)
    LOG.info(f"Max duration {max_duration_mins} mins - {max_duration}")

    user_count = 0
    user_total = max_users if max_users < len(filtered_users) else len(filtered_users)
    manual_action_users = set()
    for user in filtered_users[:max_users]:
        if datetime.now(timezone.utc) > max_duration:
            LOG.warning(
                f"Max scan duration of {max_duration_mins} mins reached. "
                f"{user_count} out of {user_total} users scanned."
            )
            break
        user_count += 1
        try:
            user_patch_data, manual_action_set = scan_remove_users_shared_permissions(
                user, shared_drive_connection, shared_drive_id, configuration, write_mode
            )
            if manual_action_set:
                manual_action_users.add(user.get("primaryEmail", f"Unknown ({user['id']})"))
            directory_connection.update_user_ucam_fields(user["id"], user_patch_data)
        except (HttpError, UserProcessingError):
            # Logged already in called function, continue processing next user
            continue

    if manual_action_users:
        LOG.error(
            "The following users had manual action required set: " + ", ".join(manual_action_users)
        )
        raise RuntimeError("Manual action required was set for some users")

    return filtered_users


def remove_permissions_from(
    file_list,
    user,
    user_drive_connection,
    shared_drive_connection,
    shared_drive_id,
    permissions_file_name,
    permissions_document,
):
    # Sort files so that parent folders are first
    file_list = sort_files_for_permission_removal(file_list)
    LOG.debug("Shared files to remove: \n" + pformat(file_list))

    # Remove shared file permissions
    permissions_to_remove = {}
    for shared_file in file_list:
        # Store whether it is a folder and what parents each file/folder has, along with
        # permissions in permissions document
        permissions_document[shared_file["id"]] = {
            "isFolder": shared_file["isFolder"],
            "parents": shared_file.get("parents", []),
            "permissions": shared_file["permissions"],
        }
        file_permissions_to_remove = [
            permission["id"]
            for permission in shared_file["permissions"]
            if permission["role"] != "owner"
        ]
        LOG.debug(
            f"File {shared_file['id']} has permissions for removal: \n"
            + pformat(file_permissions_to_remove)
        )
        if file_permissions_to_remove:
            permissions_to_remove[shared_file["id"]] = file_permissions_to_remove
    # Write file first, if this fails we should cancel out without removing the file
    # permissions
    LOG.debug("Current permissions document: \n" + pformat(permissions_document))
    LOG.debug("Permission to remove in batch: \n" + pformat(permissions_to_remove))
    if permissions_to_remove:
        if not permissions_file_name:
            user_readable_id = user.get("primaryEmail", "").split("@")[0]
            permissions_file_name = (
                f"{user_readable_id}-{user['id']}-shared-permissions-"
                f"{datetime.now().strftime('%Y%m%d-%H%M%S')}.yaml"
            )
        shared_drive_connection.write_file(
            permissions_file_name,
            yaml.dump(permissions_document, Dumper=yaml.CDumper),
            mimetype="text/yaml",
            parents=[shared_drive_id],
        )
        user_drive_connection.batch_remove_permissions(permissions_to_remove)
    return (
        len(permissions_document),
        permissions_file_name,
    )


def find_permissions_doc(user, shared_drive_connection, shared_drive_id):
    doc_name, raw_doc = shared_drive_connection.get_file_contents_by_name(
        user["id"], drive_id=shared_drive_id
    )
    if not raw_doc or (doc_name and doc_name.startswith("HISTORIC")):
        return "", {}
    else:
        try:
            permissions_recovery_doc = yaml.load(raw_doc.decode("utf-8"), Loader=yaml.CLoader)
            LOG.debug(
                f"Existing permissions document {doc_name}:\n" + pformat(permissions_recovery_doc)
            )
            return doc_name, permissions_recovery_doc
        except yaml.YAMLError:
            LOG.error(
                f"Previously recorded permissions document {doc_name} was not valid "
                "YAML. If it has been modified the user scan may end up in an invalid state."
            )
            raise UserProcessingError()


def scan_remove_users_folders(
    user, shared_drive_connection, shared_drive_id, user_drive_connection
):
    ucam_fields = {
        UcamField.RESULT: "",
        UcamField.ACTION: "scan-files",
        UcamField.FILECOUNT: 0,
        UcamField.DOC: "",
    }
    LOG.info(f"User {user.get('primaryEmail', 'Unknown')} was marked for FOLDER scan.")

    all_user_folders = user_drive_connection.get_all_owned_files_metadata(
        query=f"mimeType = '{FOLDER_MIMETYPE}'"
    )
    LOG.info(f"User {user.get('primaryEmail')}, has {len(all_user_folders)} folders.")
    user_shared_folders = [folder for folder in all_user_folders if file_is_shared(folder)]
    LOG.info(f"User {user.get('primaryEmail')}, has {len(user_shared_folders)} shared folders.")

    permissions_file_name, permissions_recovery_doc = find_permissions_doc(
        user, shared_drive_connection, shared_drive_id
    )
    if permissions_file_name:
        LOG.info(
            f"User {user.get('primaryEmail')} has existing permissions document "
            f"{permissions_file_name}, but was marked for folder scan. Existing document will be "
            "merged with newly removed permissions if applicable."
        )

    # If a permissions document was found it will be merged and rewritten with the new
    # removed permissions. If one was not found a new one will be created.
    if len(user_shared_folders):
        folders_removed, permissions_file_name = remove_permissions_from(
            user_shared_folders,
            user,
            user_drive_connection,
            shared_drive_connection,
            shared_drive_id,
            permissions_file_name,
            permissions_recovery_doc,
        )
    else:
        folders_removed = len(permissions_recovery_doc)
    ucam_fields[UcamField.FILECOUNT] = folders_removed
    # If an existing permissions document was found, the existing name will be used here, if an
    # existing permissions document was not found and no shared folders were found then this is
    # blank.
    ucam_fields[UcamField.DOC] = permissions_file_name
    return ucam_fields


def scan_remove_users_files(user, shared_drive_connection, shared_drive_id, user_drive_connection):
    ucam_fields = {
        UcamField.RESULT: "",
        UcamField.ACTION: "",
        UcamField.FILECOUNT: 0,
        UcamField.DOC: "",
    }

    LOG.info(f"User {user.get('primaryEmail', 'Unknown')} was marked for FILE scan.")

    all_user_files = user_drive_connection.get_all_owned_files_metadata()
    LOG.info(f"User {user.get('primaryEmail')}, has {len(all_user_files)} files.")
    user_shared_files = [file for file in all_user_files if file_is_shared(file)]
    LOG.info(f"User {user.get('primaryEmail')}, has {len(user_shared_files)} shared files.")

    permissions_file_name, permissions_document = find_permissions_doc(
        user, shared_drive_connection, shared_drive_id
    )
    if permissions_file_name:
        LOG.info(
            f"User {user.get('primaryEmail')} has existing permissions document "
            f"{permissions_file_name}."
        )
    else:
        LOG.info(f"User {user.get('primaryEmail')} does not have existing permissions document.")

    # Folder scan done, permissions file found and user has shared files:
    # - Update the permissions document with file removals.
    # - Write the permissions document using the existing name
    # - Remove the file permissions.
    # - Set UCam fields:
    #   - action = ""
    #   - filecount = folders_removed + files_removed
    #   - result = "permissions-removed"
    #   - doc = existing document name or newly generated name
    if len(user_shared_files):
        files_removed, permissions_file_name = remove_permissions_from(
            user_shared_files,
            user,
            user_drive_connection,
            shared_drive_connection,
            shared_drive_id,
            permissions_file_name,
            permissions_document,
        )
        ucam_fields[UcamField.FILECOUNT] = files_removed
        ucam_fields[UcamField.RESULT] = "permissions-removed"
    # Folder scan done, permissions file found and user has no shared files:
    # - Set UCam fields:
    #   - action = ""
    #   - filecount = folders_removed
    #   - result = "permissions-removed"
    #   - doc = existing document name
    elif not len(user_shared_files) and permissions_file_name:
        ucam_fields[UcamField.FILECOUNT] = len(permissions_document)
        ucam_fields[UcamField.RESULT] = "permissions-removed"
    # Folder scan done, permissions file not found and user has no shared files:
    # - Set UCam fields:
    #   - action = ""
    #   - filecount = folders_removed
    #   - result = "permissions-none"
    #   - doc = ""
    else:
        ucam_fields[UcamField.RESULT] = "permissions-none"
    ucam_fields[UcamField.DOC] = permissions_file_name
    return ucam_fields


def scan_remove_users_shared_permissions(
    user, shared_drive_connection, shared_drive_id, configuration, write_mode
):
    LOG.info(f"Scanning user {user.get('primaryEmail', 'Unknown')}, who has id {user['id']}.")
    action = user.get("customSchemas", {}).get("UCam", {}).get("mydrive-shared-action", None)
    user_drive_connection = GoogleDriveConnection(configuration, user["id"], write_mode=write_mode)

    manual_action_set = False
    try:
        if action == "scan-files":
            ucam_fields = scan_remove_users_files(
                user, shared_drive_connection, shared_drive_id, user_drive_connection
            )
        else:
            ucam_fields = scan_remove_users_folders(
                user, shared_drive_connection, shared_drive_id, user_drive_connection
            )
    except ListTimeoutError:
        manual_action = f"manual-{action}"
        LOG.warning(
            f"Timeout scanning {user.get('primaryEmail', 'Unknown')} "
            f"setting action to {manual_action}"
        )
        manual_action_set = True
        ucam_fields = {
            UcamField.ACTION: manual_action,
        }
    LOG.debug(
        f"Write user {user.get('primaryEmail', 'Unknown')} UCam fields:\n" + pformat(ucam_fields)
    )
    return ucam_fields, manual_action_set


# Management Command Function - `gdrivemanager recover`


def recover(configuration, user_id=None, rescan=False, write_mode=False):
    LOG.info(f'Running RECOVER in {"WRITE" if write_mode else "READ ONLY"} mode.')

    max_users = configuration.limits.max_recovered_users
    directory_connection = GoogleDirectoryConnection(configuration, write_mode=write_mode)

    if user_id:
        user_email = f"{user_id.lower()}@{configuration.google.domain_name}"
        LOG.info(f"Finding user {user_email}")
        user = directory_connection.retrieve_user(user_email)
        if not user:
            LOG.error(f"User {user_email} not found.")
            return
        if not user_marked_for_recovery(user):
            LOG.error(f"User {user_email} not marked for recovery.")
            return
        LOG.info(f"User found with id {user['id']}")
        filtered_users = [user]
    else:
        all_google_users = directory_connection.retrieve_users()

        LOG.info(f"TOTAL users fetched: {len(all_google_users)}")

        filtered_users = [
            user
            for user in all_google_users
            if all(
                [
                    user_email_is_crsid(user),
                    user_marked_for_recovery(user),
                ]
            )
        ]

        LOG.info(f"TOTAL users marked for recovery: {len(filtered_users)}.")
        LOG.info(f"Maximum users to recover this run: {max_users}.")

    shared_drive_connection = GoogleDriveConnection(configuration, write_mode=write_mode)
    shared_drive_id = configuration.google.shared_storage_drive
    users_patch_data = {}

    for user in filtered_users[:max_users]:
        try:
            users_patch_data[user["id"]] = recover_users_shared_permissions(
                user,
                shared_drive_connection,
                shared_drive_id,
                configuration,
                rescan=rescan,
                write_mode=write_mode,
            )
        except (HttpError, UserProcessingError):
            # Logged already in called function, continue processing
            continue

    # Remove None entries from the batch update.
    directory_connection.batch_update_user_ucam_fields(
        {k: v for k, v in users_patch_data.items() if v}
    )

    return filtered_users


def recover_users_shared_permissions(
    user, shared_drive_connection, shared_drive_id, configuration, rescan=False, write_mode=False
):
    LOG.info(f"Recovering user {user.get('primaryEmail', 'Unknown')}, who has id {user['id']}.")
    result_of_scan = user.get("customSchemas", {}).get("UCam", {}).get(UcamField.RESULT, "")
    if result_of_scan:
        permissions_doc_name, raw_doc = shared_drive_connection.get_file_contents_by_name(
            user["id"], drive_id=shared_drive_id
        )
        if raw_doc:
            if permissions_doc_name.startswith("HISTORIC"):
                LOG.error(
                    f"User {user.get('primaryEmail', 'Unknown')} had HISTORIC permissions "
                    "document but was marked for recovery. Manual intervention is required."
                )
                raise UserProcessingError()
            LOG.info(
                f"User {user.get('primaryEmail', 'Unknown')} has permissions recovery "
                f"document {permissions_doc_name}."
            )
            if result_of_scan == "permissions-none" or result_of_scan == "permissions-recovered":
                LOG.warning(
                    f"User {user.get('primaryEmail', 'Unknown')} was marked for recovery and had "
                    "shared permissions despite their mydrive-shared-result being "
                    f"{result_of_scan}. These permissions will be recovered, overwriting any "
                    "current permissions."
                )
            permissions_recovery_doc = yaml.load(raw_doc.decode("utf-8"), Loader=yaml.CLoader)
            LOG.debug("Permissions document contents:\n" + pformat(permissions_recovery_doc))

            if rescan:
                permissions_recovery_doc = rescan_permissions(
                    user, configuration, permissions_recovery_doc
                )

            user_drive_connection = GoogleDriveConnection(
                configuration, user["id"], write_mode=write_mode
            )
            user_drive_connection.batch_create_shared_permissions(permissions_recovery_doc)
            shared_drive_connection.update_file_metadata(
                permissions_doc_name,
                {"name": f"HISTORIC-{permissions_doc_name}"},
                drive_id=shared_drive_id,
            )
            return {
                UcamField.RESULT: "permissions-recovered",
                UcamField.ACTION: "",
            }
        else:
            if result_of_scan == "permissions-removed":
                LOG.warning(
                    f"User {user.get('primaryEmail', 'Unknown')} was marked for recovery but had "
                    f"no shared permissions backup stored in shared drive id {shared_drive_id} "
                    f"despite their mydrive-shared-result being {result_of_scan}. No permissions "
                    "can be recovered."
                )
            else:
                LOG.info(
                    f"User {user.get('primaryEmail', 'Unknown')} did not have shared permissions, "
                    "and does not need to be recovered."
                )
            # Clear action, leave other fields unchanged.
            return {UcamField.ACTION: ""}
    else:
        LOG.warning(
            f"User {user.get('primaryEmail', 'Unknown')} did not have a mydrive-shared-result set "
            "and cannot be recovered."
        )
        return None


def rescan_permissions(user, configuration, permissions_recovery_doc):
    LOG.info("Rescanning for existing permissions...")
    user_drive_connection = GoogleDriveConnection(configuration, user["id"], write_mode=False)
    all_user_items = user_drive_connection.get_all_owned_files_metadata()
    LOG.info(f"- has {len(all_user_items)} items.")
    user_shared_items = {item["id"]: item for item in all_user_items if file_is_shared(item)}
    LOG.info(f"- has {len(user_shared_items)} shared items.")

    # Remove items from recovery doc if in existing shared items (and permissions match)
    items_to_remove = []
    for item_id, item_details in permissions_recovery_doc.items():
        if item_id in user_shared_items:
            if matching_permissions(
                item_details["permissions"], user_shared_items[item_id]["permissions"]
            ):
                LOG.info(f"- removing {item_id} from permissions recovery document.")
                items_to_remove.append(item_id)
    LOG.info(f"- removing {len(items_to_remove)} items from permissions recovery document.")
    return {
        item_id: item_details
        for item_id, item_details in permissions_recovery_doc.items()
        if item_id not in items_to_remove
    }


def matching_permissions(p1, p2):
    LOG.debug(f"Comparing permissions:\n{pformat(p1)}\n{pformat(p2)}")
    if len(p1) != len(p2):
        return False
    # Compare permissions excluding id and sorted by (emailAddress, domain)
    c1 = sorted(
        [{k: v for k, v in p.items() if k != "id"} for p in p1],
        key=lambda p: (p.get("emailAddress", ""), p.get("domain", "")),
    )
    c2 = sorted(
        [{k: v for k, v in p.items() if k != "id"} for p in p2],
        key=lambda p: (p.get("emailAddress", ""), p.get("domain", "")),
    )
    for i in range(len(c1)):
        if c1[i] != c2[i]:
            return False
    return True


# Management Command Function - `gdrivemanager reset-manual`


def reset_manual_actions(configuration, write_mode=False):
    LOG.info(f'Running RESET MANUAL in {"WRITE" if write_mode else "READ ONLY"} mode.')

    directory_connection = GoogleDirectoryConnection(configuration, write_mode=write_mode)
    all_google_users = directory_connection.retrieve_users()

    LOG.info(f"TOTAL users fetched: {len(all_google_users)}")

    filtered_users = [
        user
        for user in all_google_users
        if all(
            [
                user_email_is_crsid(user),
                user_marked_for_manual_attention(user),
            ]
        )
    ]

    LOG.info(f"TOTAL users marked for manual attention: {len(filtered_users)}")

    user_total = len(filtered_users)
    for idx, user in enumerate(filtered_users):
        new_action = user["customSchemas"]["UCam"][UcamField.ACTION].replace("manual-", "")
        LOG.info(
            f"Updating user {user.get('primaryEmail', 'Unknown')} action to "
            f"{new_action} - {idx+1}/{user_total}"
        )
        try:
            directory_connection.update_user_ucam_fields(
                user["id"],
                {
                    UcamField.ACTION: new_action,
                },
            )
        except (HttpError, UserProcessingError):
            # Logged already in called function, continue processing next user
            continue
