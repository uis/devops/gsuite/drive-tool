import logging
import os
from datetime import datetime
from enum import Enum

from ..gapi import GoogleDirectoryConnection, GoogleDriveConnection
from ..lookup import LookupConnection
from ..utils import generate_csv, read_csv, user_email_is_crsid, write_csv
from .shared_drives import read_shared_drive_cache

LOG = logging.getLogger(__name__)

ReportType = Enum("ReportType", ["INSTITUTION", "GROUP", "USER", "REQUEST"])


# Management Command Function - `gdrivemanager report`


def report(
    configuration,
    report_type: ReportType,
    entity_id=None,
    include_children=False,
    cache_users=False,
    write_mode=False,
):
    # If outputting report to Google Drive, we need a drive connection to it
    if configuration.report.output_location is not None:
        output_drive_connection = GoogleDriveConnection(configuration, write_mode=write_mode)
    else:
        output_drive_connection = None
    # Gather list of users to report on
    users, request_file, entity_id = get_users_for_report(
        configuration, report_type, entity_id, include_children, output_drive_connection
    )
    if len(users) == 0:
        return

    LOG.info("Filtering users to only those with a Google account")
    directory_connection = GoogleDirectoryConnection(configuration, write_mode=False)
    filtered_users = filter_users_by_google_account(configuration, directory_connection, users)
    LOG.info(f"Found {len(filtered_users)} users with a Google account")
    if len(filtered_users) == 0:
        return

    # Load the user quota cache if using it
    user_quota_cache = get_user_usage_cache(configuration) if cache_users else []

    # Get MyDrive usage for each user
    mydrive_usage = get_user_usage_for_report(configuration, filtered_users, user_quota_cache)

    # Write to CSV
    report_filename = configuration.report.mydrive_filename.format(
        id=entity_id, timestamp=datetime.now().strftime("%Y%m%d-%H%M%S")
    )
    write_report(
        configuration, mydrive_usage, report_filename, output_drive_connection, write_mode
    )

    # Update user usage cache file if using it
    if cache_users:
        update_user_usage_cache(configuration, user_quota_cache, mydrive_usage, write_mode)

    # Find cached shared drive list in shared_storage_drive
    shared_drive_connection = GoogleDriveConnection(configuration, write_mode=False)
    shared_drives_cache = read_shared_drive_cache(shared_drive_connection)
    if shared_drives_cache is None:
        LOG.error("No shared drive list found, cannot report shared drives without it")
        return
    shared_drives = shared_drives_cache.get("shared_drives", [])

    # Get shared drive usage for drives with these users as managers
    shared_drives_output = get_shared_drive_usage_for_report(shared_drives, filtered_users)

    if len(shared_drives_output) > 0:
        # Write to CSV
        report_filename = configuration.report.shared_drive_filename.format(
            id=entity_id, timestamp=datetime.now().strftime("%Y%m%d-%H%M%S")
        )
        write_report(
            configuration,
            shared_drives_output,
            report_filename,
            output_drive_connection,
            write_mode,
        )

    # Delete report request file, if applicable
    if request_file is not None:
        LOG.info("Cleaning up request file")
        if output_drive_connection is not None:
            output_drive_connection.trash_file(request_file[0], request_file[1])
        elif not write_mode:
            LOG.info(f"Skipping deleting request file {request_file[1]} in READ ONLY mode")
        else:
            os.remove(request_file[1])


def get_users_for_report(
    configuration, report_type, entity_id, include_children, output_drive_connection
):
    # We'll need to return whether this request came from a report request file
    request_file = None

    # Search for a report request file
    if report_type == ReportType.REQUEST:
        report_type, entity_id, file_id, file_name = find_report_request_file(
            configuration, output_drive_connection
        )
        if report_type is None:
            LOG.info("No report request file found")
            return ([], None, entity_id)
        request_file = (file_id, file_name)
        # All institution requests via this method include child institutions
        if report_type == ReportType.INSTITUTION:
            include_children = True

    # Get appropriate list of users from Lookup
    lookup_connection = LookupConnection(configuration)
    if report_type == ReportType.INSTITUTION:
        # Lookup requires uppercase instid
        entity_id = entity_id.upper()
        LOG.info(
            f"Generating report for institution {entity_id}"
            + (" (and children)" if include_children else "")
        )
        users = sorted(
            lookup_connection.retrieve_users_by_instid(
                entity_id, include_children=include_children
            ).values(),
            key=lambda x: x["id"],
        )
        cancelled_users = [u for u in users if u["cancelled"]]
        LOG.info(f"Found {len(users)} users ({len(cancelled_users)} cancelled)")
    elif report_type == ReportType.USER:
        # CRSids are lower case in Lookup
        entity_id = entity_id.lower()
        LOG.info(f"Generating report for user {entity_id}")
        users = [lookup_connection.retrieve_user_by_crsid(entity_id)]
        LOG.info(
            f"{users[0]['display_name']} is {'not ' if not users[0]['cancelled'] else ''}"
            "cancelled"
        )
    elif report_type == ReportType.GROUP:
        # Lookup group names are lowercase
        entity_id = entity_id.lower()
        LOG.info(f"Generating report for group {entity_id}")
        users = lookup_connection.retrieve_users_by_group(entity_id)
        cancelled_users = [u for u in users if u["cancelled"]]
        LOG.info(f"Found {len(users)} users ({len(cancelled_users)} cancelled)")
    else:
        LOG.error(f"Invalid report type {report_type}")
        return ([], None, entity_id)

    # Add email address to each user (all with default domain name)
    users = [
        {**user, "email": f"{user['id']}@{configuration.google.domain_name}"} for user in users
    ]
    return (users, request_file, entity_id)


def filter_users_by_google_account(configuration, directory_connection, users):
    if len(users) <= configuration.report.google_account_individual_check_limit:
        return [
            user for user in users if directory_connection.user_has_google_account(user["email"])
        ]

    LOG.info("Getting full list of Google users to compare against")
    all_google_users = directory_connection.retrieve_users(suspended_only=False)
    all_google_emails = {user["primaryEmail"] for user in all_google_users}
    return [user for user in users if user["email"] in all_google_emails]


def get_user_usage_for_report(configuration, filtered_users, user_quota_cache):
    # Get MyDrive usage for each user (using cache if applicable and available)
    mydrive_usage = []
    for user in filtered_users:
        if user["id"] in user_quota_cache:
            usage = user_quota_cache[user["id"]]
        else:
            user_drive_connection = GoogleDriveConnection(
                configuration, user["email"], write_mode=False
            )
            usage = user_drive_connection.get_storage_quota_usage()

        mydrive_usage.append(
            {
                **{k: v for k, v in user.items() if k not in ("email", "insts")},
                "usage": usage,
                "insts": ", ".join(sorted(user["insts"])),
            }
        )
    return mydrive_usage


def get_shared_drive_usage_for_report(shared_drives, filtered_users):
    # Look for those drives with a member of the users list as a manager or contentManager
    # (organizer or fileOrganiser in the API response)
    matching_emails = {user["email"] for user in filtered_users}
    shared_drives_with_user_as_manager = [
        drive
        for drive in shared_drives
        if any(
            [
                True
                for permission in drive["permissions"]
                if permission["role"] in ["organizer", "fileOrganizer"]
                and permission["emailAddress"] in matching_emails
            ]
        )
    ]
    LOG.info(
        f"Found {len(shared_drives_with_user_as_manager)} shared drives with matching managers"
    )
    # Add managers and content managers CRSids to each shared drive and institutions as those of
    # any managers or content managers, and format for CSV output
    filtered_users_by_id = {user["id"]: user for user in filtered_users}
    shared_drives_output = []
    for drive in shared_drives_with_user_as_manager:
        managers = {
            permission["emailAddress"].split("@")[0]
            for permission in drive["permissions"]
            if permission["role"] == "organizer" and permission["emailAddress"] in matching_emails
        }
        content_managers = {
            permission["emailAddress"].split("@")[0]
            for permission in drive["permissions"]
            if permission["role"] == "fileOrganizer"
            and permission["emailAddress"] in matching_emails
        }
        institutions = {
            inst
            for manager in (managers | content_managers)
            for inst in filtered_users_by_id[manager]["insts"]
        }
        shared_drives_output.append(
            {
                "id": drive["id"],
                "name": drive["name"],
                "managers": ", ".join(managers),
                "content_managers": ", ".join(content_managers),
                "insts": ", ".join(sorted(institutions)),
                "files": drive.get("files", "Unknown"),
                "usage": drive.get("usage", "Unknown"),
                "as_of": drive.get("last_updated") or "Unknown",
            }
        )
    return shared_drives_output


def write_report(configuration, report_data, report_filename, drive_connection, write_mode=False):
    # Write CSV report output to file, either locally or in Google Drive
    if not write_mode:
        LOG.info(f"Skipping writing report {report_filename!r} in READ ONLY mode")
    elif drive_connection is None:
        LOG.info(f"Writing report to {report_filename!r} locally")
        write_csv(report_data, report_filename)
    else:
        drive_connection.write_file(
            report_filename,
            generate_csv(report_data),
            mimetype="text/csv",
            parents=[configuration.report.output_location],
        )


def get_user_usage_cache(configuration):
    # Attempt to read CSV holding cached user quota usage
    try:
        user_list = read_csv(configuration.report.mydrive_user_usage_cache)
    except FileNotFoundError:
        LOG.info("No user quota cache found")
        return {}
    LOG.info(f"Found {len(user_list)} users in quota cache")
    # Convert to dict of {user_id: usage}
    return {user["id"]: user["usage"] for user in user_list}


def update_user_usage_cache(configuration, user_quota_cache, mydrive_usage, write_mode=False):
    # Update user_quota_cache with any new usage data
    for user in mydrive_usage:
        user_quota_cache[user["id"]] = user["usage"]
    # Write to CSV
    LOG.info(f"Writing {len(user_quota_cache)} users to quota cache")
    if write_mode:
        write_csv(
            [{"id": k, "usage": v} for k, v in user_quota_cache.items()],
            configuration.report.mydrive_user_usage_cache,
        )
    else:
        LOG.info("Skipping writing user quota cache in READ ONLY mode")


def find_report_request_file(configuration, drive_connection):
    if configuration.report.output_location is None:
        # Search locally
        with os.scandir(".") as files:
            request_files = [
                {"id": None, "name": file.name}
                for file in files
                if file.is_file() and file.name.startswith("report-request-")
            ]
    else:
        # Search in shared drive
        request_files = drive_connection.search_for_files("report-request-")

    while len(request_files) > 0:
        file = request_files.pop(0)
        LOG.info(f"Found report request file {file['id']!r} - {file['name']!r}")

        # Strip extension if present, make sure hyphens are used not underscores and lower case
        report_name = file["name"].replace("_", "-").split(".")[0].lower()

        # Get report type and id from file name
        parts = report_name.split("-")[2:]

        # report-request-{id} assumed to be report-request-inst-{id}
        if len(parts) == 1:
            parts = ["inst", parts[0]]
        # group ids have hyphens in them so join id back together
        elif len(parts) > 2 and parts[0] in ("g", "group"):
            parts = ["g", "-".join(parts[1:])]

        if len(parts) == 2 and parts[0] in (
            "i",
            "inst",
            "institution",
            "g",
            "group",
            "u",
            "user",
        ):
            if parts[0] in ("i", "inst", "institution"):
                report_type = ReportType.INSTITUTION
            elif parts[0] in ("g", "group"):
                report_type = ReportType.GROUP
            else:
                report_type = ReportType.USER
            return (report_type, parts[1], file["id"], file["name"])

        LOG.warning(f"Invalid report request file name {file['name']!r} - ignored")
    # No valid report file found
    return (None, None, None, None)


# Management Command Function - `gdrivemanager shared-drive-report`


def shared_drive_report(configuration, write_mode=False):
    # If outputting report to Google Drive, we need a drive connection to it
    if configuration.report.output_location is not None:
        output_drive_connection = GoogleDriveConnection(configuration, write_mode=write_mode)
    else:
        output_drive_connection = None

    # Find cached shared drive list in shared_storage_drive
    shared_drive_connection = GoogleDriveConnection(configuration, write_mode=False)
    shared_drives_cache = read_shared_drive_cache(shared_drive_connection)
    if shared_drives_cache is None:
        LOG.error("No shared drive list found, cannot report shared drives without it")
        return
    shared_drives = shared_drives_cache.get("shared_drives", [])
    LOG.info(f"Found {len(shared_drives)} shared drives")

    # Get report data for all shared drives
    all_crsids = set()
    shared_drive_output = []
    for drive in shared_drives:
        crsids, drive_data = get_shared_drive_data(configuration, drive)
        all_crsids.update(crsids)
        shared_drive_output.append(drive_data)

    # Lookup all users to get their cancelled status and institution(s)
    LOG.info(f"Fetching data from Lookup for {len(all_crsids)} users")
    lookup_connection = LookupConnection(configuration)
    crsids_to_lookup_data = {
        u["id"]: u for u in lookup_connection.retrieve_users_from_list(all_crsids)
    }
    LOG.info(f"{len(crsids_to_lookup_data)} of {len(all_crsids)} users found in Lookup")

    # Add institution(s) to and split users for each shared drive
    report_output = sorted(
        [
            reformat_shared_drive_data(drive, crsids_to_lookup_data)
            for drive in shared_drive_output
        ],
        key=lambda x: (1, x["name"]) if x["usage"] == "Unknown" else (-x["usage"], x["name"]),
    )

    # Write to CSV
    report_filename = configuration.report.all_shared_drive_filename.format(
        timestamp=datetime.now().strftime("%Y%m%d-%H%M%S")
    )
    write_report(
        configuration, report_output, report_filename, output_drive_connection, write_mode
    )


def get_shared_drive_data(configuration, drive):
    # Get all users and their permissions
    crsids = set()
    users = {}
    for permission in drive.get("permissions", []):
        # Some permissions have blank `emailAddress`s - ignore these as believed to be
        # deleted users
        if permission.get("emailAddress", "") == "":
            continue

        # Skip the management user as we don't want them to appear in the report
        if (
            configuration.google.management_user is not None
            and permission["emailAddress"] == configuration.google.management_user
        ):
            continue

        user_data = {
            "email": permission["emailAddress"],
            "role": permission["role"],
            "crsid": None,
            "in_domain": False,
            "is_group": False,
        }

        # If the email address is in the domain and is a CRSid, add it to the set
        if permission["emailAddress"].endswith("@" + configuration.google.domain_name):
            user_data["in_domain"] = True
            if user_email_is_crsid({"primaryEmail": permission["emailAddress"]}):
                crsid = permission["emailAddress"].split("@")[0]
                user_data["crsid"] = crsid
                crsids.add(crsid)
        elif permission.get("type") == "group":
            user_data["is_group"] = True
            group_domain = permission["emailAddress"].split("@")[1]
            if group_domain in configuration.report.group_domains:
                user_data["in_domain"] = True
        users[permission["emailAddress"]] = user_data

    # Add on augmented file permission users
    augmented_permissions = []
    if drive.get("augmented_permissions"):
        for file in drive["augmented_permissions"]:
            for permission in file.get("permissions", []):
                user_data = {
                    "email": permission.get("emailAddress"),
                    "role": permission["role"],
                }
                if permission.get("type") == "anyone":
                    user_data["email"] = "anyoneWithLink"
                elif permission.get("emailAddress", "") == "":
                    # Some permissions have blank `emailAddress`s - ignore these as believed to be
                    # deleted users
                    continue
                elif permission["emailAddress"].endswith(
                    "@" + configuration.google.domain_name
                ) and user_email_is_crsid({"primaryEmail": permission["emailAddress"]}):
                    user_data["email"] = permission["emailAddress"].split("@")[0]

                augmented_permissions.append(user_data)

    # Return set of CRSids and drive data with permissions replaced by users data
    drive_data = {k: v for k, v in drive.items() if k != "permissions"}
    drive_data["users"] = users
    drive_data["augmented_permissions"] = augmented_permissions
    return crsids, drive_data


def reformat_shared_drive_data(drive, crsids_to_lookup_data):
    # Get a set of all institutions from all users of the drive
    institutions = set()
    # Also, separate users in to:
    # - active_cam_managers: active domain users with organizer or fileOrganizer role
    # - active_cam_others: active domain users with another role
    # - cancelled_cam_users: cancelled domain users with any role
    # - external_users: non-domain users
    users = {
        "active_cam_managers": [],
        "active_cam_others": [],
        "cancelled_cam_users": [],
        "other_users": [],
    }
    for _, user in drive["users"].items():
        user_with_role = _user_with_short_role(user)

        if not user["in_domain"]:
            users["other_users"].append(user_with_role)
            continue

        if user["is_group"]:
            if user["role"] in ["organizer", "fileOrganizer"]:
                users["active_cam_managers"].append(user["email"])
            else:
                users["active_cam_others"].append(user_with_role)
            continue

        if user["crsid"] is None:
            users["other_users"].append(user_with_role)
            continue

        lookup_data = crsids_to_lookup_data.get(user["crsid"])
        # Treat as cancelled if not found in Lookup
        if lookup_data is None:
            users["cancelled_cam_users"].append(user_with_role)
            continue

        # Add institutions to set even for cancelled domain users
        institutions.update(lookup_data.get("insts", []))

        if lookup_data.get("cancelled"):
            users["cancelled_cam_users"].append(user_with_role)
            continue

        if user["role"] in ["organizer", "fileOrganizer"]:
            users["active_cam_managers"].append(
                user["crsid"]
            )  # just CRSid as they are all managers
        else:
            users["active_cam_others"].append(user_with_role)

    # reformat the augmented permissions to users with short roles
    augmented_users = set()
    for permission in drive["augmented_permissions"]:
        user_with_role = _user_with_short_role(permission)
        augmented_users.add(user_with_role)

    # Return reformatted drive data ready for CSV output
    return {
        "id": drive["id"],
        "name": drive["name"],
        "active_cam_managers": ", ".join(sorted(users["active_cam_managers"])),
        "active_cam_others": ", ".join(sorted(users["active_cam_others"])),
        "cancelled_cam_users": ", ".join(sorted(users["cancelled_cam_users"])),
        "other_users": ", ".join(sorted(users["other_users"])),
        "augmented_users": ", ".join(sorted(augmented_users)),
        "files": drive["files"] if drive.get("files") is not None else "Unknown",
        "usage": drive["usage"] if drive.get("usage") is not None else "Unknown",
        "as_of": (
            str(drive["last_updated"]) if drive.get("last_updated") is not None else "Unknown"
        ),
        "institutions": ", ".join(sorted(institutions)),
        "created": (
            str(drive["createdTime"]) if drive.get("createdTime") is not None else "Unknown"
        ),
        "orgUnitId": drive["orgUnitId"] if drive.get("orgUnitId") is not None else "Unknown",
        "last_removed": (
            str(drive["last_removed"]) if drive.get("last_removed") is not None else "Unknown"
        ),
        "last_restored": (
            str(drive["last_restored"]) if drive.get("last_restored") is not None else "Unknown"
        ),
    }


def _user_with_short_role(user):
    # Merge file permissions roles to single letters
    role_to_short_role_mapping = {
        "organizer": "M",
        "fileOrganizer": "M",
        "writer": "W",
        "commenter": "R",
        "reader": "R",
    }
    id = user["crsid"] if user.get("crsid") is not None else user.get("email")
    short_role = role_to_short_role_mapping.get(user["role"], "?")
    return f"{id} [{short_role}]"
