import csv
import io
import itertools
import re
from collections import namedtuple

FOLDER_MIMETYPE = "application/vnd.google-apps.folder"

crsid_email_regex = re.compile("^[a-z][a-z0-9]{3,7}@.*$")


# Filter Functions


def user_email_is_crsid(user):
    return bool(crsid_email_regex.match(user.get("primaryEmail", "")))


def user_marked_for_scan(user):
    action = user.get("customSchemas", {}).get("UCam", {}).get(UcamField.ACTION, None)
    return isinstance(action, str) and action.startswith("scan")


def user_marked_for_recovery(user):
    action = user.get("customSchemas", {}).get("UCam", {}).get(UcamField.ACTION, None)
    return action == "recover"


def user_marked_for_manual_attention(user):
    action = user.get("customSchemas", {}).get("UCam", {}).get(UcamField.ACTION, None)
    return isinstance(action, str) and action.startswith("manual-")


def file_is_shared(file):
    return file.get("shared", False)


class UcamField:
    """
    Enum-like class to hold constants - doesn't inherit from enum as we don't need Enum
    functionality, and using it means we would need to convert from Enums to strings.
    """

    ACTION = "mydrive-shared-action"
    RESULT = "mydrive-shared-result"
    FILECOUNT = "mydrive-shared-filecount"
    DOC = "mydrive-shared-doc"


def sort_files_for_permission_removal(data):
    """
    Sort provided list so that higher files/folders in a hierarchy are closer to the top.
    Despite files typically having a single parent this needs to cope with multiple.
    Choose the deepest parent when working out depth so file is guaranteed to come after
    all its parents.
    """

    files_by_fileid = {f["id"]: f for f in data}
    all_fileids = set(files_by_fileid.keys())

    def get_and_set_depth(f):
        # Already set to just return it
        if f.get("depth") is not None:
            return f["depth"]

        # Recurse up parents, if necessary
        known_parents = [p for p in f.get("parents", []) if p in all_fileids]
        if len(known_parents) == 0:
            f["depth"] = 0
        else:
            f["depth"] = max([get_and_set_depth(files_by_fileid[p]) for p in known_parents]) + 1

        # Set isFolder flag too
        f["isFolder"] = f.get("mimeType", "") == FOLDER_MIMETYPE

        return f["depth"]

    # Give each file a depth (number of known parents)
    for file in data:
        get_and_set_depth(file)

    # Return the list of files sorted by folders first then depth
    return sorted(data, key=lambda x: (1 - int(x["isFolder"]), x["depth"]))


def read_csv(filename: str):
    with open(filename) as f:
        reader = csv.DictReader(f)
        return list(reader)


def write_csv(data: list[dict], filename: str):
    with open(filename, "w") as f:
        writer = csv.DictWriter(f, fieldnames=data[0].keys())
        writer.writeheader()
        writer.writerows(data)


def generate_csv(data: list[dict]):
    with io.StringIO() as f:
        writer = csv.DictWriter(f, fieldnames=data[0].keys())
        writer.writeheader()
        writer.writerows(data)
        return f.getvalue()


def human_size(num, units=["B", "KiB", "MiB", "GiB", "TiB", "PiB"]):
    """
    Returns a human readable string representation of bytes

    """
    if num < 1024 or len(units) < 2:
        if units[0] == "B":
            return f"{num:.0f} B"
        else:
            return f"{num:.2f} {units[0]}"
    else:
        return human_size(num / 1024.0, units[1:])


PermissionsInfo = namedtuple("PermissionsInfo", ["historic", "crsid", "google_id"])


def info_from_filename(permissions_document_name):
    # Ensure shared drive recovery files are ignored
    if permissions_document_name.startswith("shared-drive-"):
        return None
    split_name = permissions_document_name.split("-")
    # If split doesn't return expected length, return None
    if len(split_name) not in (6, 7):
        return None
    historic = False
    if split_name[0] == "HISTORIC":
        historic = True
        split_name.pop(0)
    crsid = split_name[0]
    google_id = split_name[1]
    return PermissionsInfo(historic, crsid, google_id)


def merge_dict(a, b):
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                merge_dict(a[key], b[key])
        else:
            a[key] = b[key]
    return a


def grouper(iterable, *, n):
    """
    Group an iterable into chunks of at most *n* elements. A generator which yields iterables
    representing slices of *iterable*.

    >>> [list(i) for i in grouper('ABCDEFGH', n=3)]
    [['A', 'B', 'C'], ['D', 'E', 'F'], ['G', 'H']]
    >>> def generator(stop):
    ...     for x in range(stop):
    ...         yield x
    >>> [list(i) for i in grouper(generator(10), n=3)]
    [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9]]
    >>> [list(i) for i in grouper(generator(12), n=3)]
    [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]

    The implementation of this function attempts to be efficient; the chunks are iterables which
    are generated on demand rather than being constructed first. Hence this function can deal with
    iterables which would fill memory if intermediate chunks were stored.

    >>> i = grouper(generator(100000000000000000000), n=1000000000000000)
    >>> next(next(i))
    0

    """
    it = iter(iterable)
    while True:
        next_chunk_it = itertools.islice(it, n)
        try:
            first = next(next_chunk_it)
        except StopIteration:
            return
        yield itertools.chain((first,), next_chunk_it)
