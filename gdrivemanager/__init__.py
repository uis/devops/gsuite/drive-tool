"""
Drive Management Tool

Usage:
    gdrivemanager (-h | --help)
    gdrivemanager [options] scan
    gdrivemanager [options] recover [--user=CRSID] [--rescan]
    gdrivemanager [options] shared-drive-usage [--skip-rebuild] [--driveid=DRIVEID]
    gdrivemanager [options] shared-drive-permissions (remove | restore) --driveid=DRIVEID
    gdrivemanager [options] report
        (--instid=INSTID [--children] | --user=CRSID | --groupid=GROUPID | --request)
        [--cache-users]
    gdrivemanager [options] reset-manual
    gdrivemanager [options] clean-recovery-drive
    gdrivemanager [options] shared-drive-report

Options:
    -h, --help                  Show a brief usage summary.
    -q, --quiet                 Reduce logging verbosity.
    -v, --verbose               Increase logging verbosity.
    -c, --configuration=FILE    Specify configuration file to load.
    -w, --write                 Actually try to make the changes.

        --rescan                Rescan for existing restored permissions so they
                                don't need to be restored again.

        --skip-rebuild          Skips the rebuild of the shared drive list before
                                scanning the shared drive(s).
        --driveid=DRIVEID       Force a scan of just the specified shared drive
                                and update the cache.

    -i, --instid=INSTID         Lookup Institution ID to create a report for.
        --children              Recursively include the child institutions of
                                the institution specified by --instid.
    -u, --user=CRSID            CRSid to recover or create a report for.
    -g, --groupid=GROUPID       Group ID (or shortname) to create a report for.
    -r, --request               Search for request file in the report folder.
        --cache-users           Cache user quota in a local file (dev only)

Commands:
    scan                     Scan google drives which are marked as requiring
                             scanning.
    recover                  Recover google drives which are marked for recovery.
    shared-drive-usage       Update the cached shared drive usage and permissions.
    shared-drive-permissions Remove or restore permissions on a shared drive.
    report                   Generate a report for a given institution or user.
    reset-manual             Reset the manual attention action for all such users.
    clean-recovery-drive     Clean the shared recovery drive, deleting unused
                             recovery documents and merging duplicates.
    shared-drive-report      Generate a report for all shared drives.

"""

import logging
import os
import socket
import sys

import docopt

from . import manage
from .parse_config import load_configuration, parse_configuration

LOG = logging.getLogger(os.path.basename(sys.argv[0]))


def main():
    opts = docopt.docopt(__doc__, options_first=False)

    # Configure logging
    logging.basicConfig(
        level=(
            logging.WARN
            if opts["--quiet"]
            else logging.DEBUG if opts["--verbose"] else logging.INFO
        )
    )

    # HACK: make the googleapiclient.discovery module less spammy in the logs
    logging.getLogger("googleapiclient.discovery").setLevel(logging.WARN)
    logging.getLogger("googleapiclient.discovery_cache").setLevel(logging.ERROR)

    LOG.info("Loading configuration")
    configuration = parse_configuration(load_configuration(opts["--configuration"]))

    # Set connection timeout
    socket.setdefaulttimeout(configuration.connection.timeout)

    if opts["scan"]:
        manage.scan(
            configuration,
            write_mode=opts["--write"],
        )
    elif opts["recover"]:
        manage.recover(
            configuration,
            user_id=opts["--user"],
            rescan=opts["--rescan"],
            write_mode=opts["--write"],
        )
    elif opts["shared-drive-usage"]:
        manage.scan_shared_drives(
            configuration,
            skip_rebuild=opts["--skip-rebuild"],
            drive_id=opts["--driveid"],
            write_mode=opts["--write"],
        )
    elif opts["report"]:
        if opts["--request"]:
            manage.report(
                configuration,
                manage.ReportType.REQUEST,
                cache_users=opts["--cache-users"],
                write_mode=opts["--write"],
            )
        elif opts["--instid"]:
            manage.report(
                configuration,
                manage.ReportType.INSTITUTION,
                entity_id=opts["--instid"],
                include_children=opts["--children"],
                cache_users=opts["--cache-users"],
                write_mode=opts["--write"],
            )
        elif opts["--user"]:
            manage.report(
                configuration,
                manage.ReportType.USER,
                entity_id=opts["--user"],
                cache_users=opts["--cache-users"],
                write_mode=opts["--write"],
            )
        elif opts["--groupid"]:
            manage.report(
                configuration,
                manage.ReportType.GROUP,
                entity_id=opts["--groupid"],
                cache_users=opts["--cache-users"],
                write_mode=opts["--write"],
            )
        else:
            raise RuntimeError("No report type could be determined")
    elif opts["reset-manual"]:
        manage.reset_manual_actions(configuration, write_mode=opts["--write"])
    elif opts["clean-recovery-drive"]:
        manage.clean_recovery_drive(configuration, write_mode=opts["--write"])
    elif opts["shared-drive-report"]:
        manage.shared_drive_report(configuration, write_mode=opts["--write"])
    elif opts["shared-drive-permissions"]:
        if opts["remove"]:
            manage.remove_shared_drive_permissions(
                configuration,
                drive_id=opts["--driveid"],
                write_mode=opts["--write"],
            )
        elif opts["restore"]:
            manage.restore_shared_drive_permissions(
                configuration,
                drive_id=opts["--driveid"],
                write_mode=opts["--write"],
            )
        else:
            raise RuntimeError("No action could be determined")
