# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.8] - 2024-03-26

### Updated

- Clear last_updated as well as last_scanned to force rescan after shared drive
  permissions have been removed or restored.
- Remove duplicated augmented permissions in report.

## [1.1.7] - 2024-03-20

### Added

- Shared drive list cache is rebuilt on every run unless --skip-rebuild given
- Shared drive permissions are calculated when scanning each drive rather than
  all when rebuilding the list
- Reduced chance of attempting to scan an already deleted drive
- Shared drive list cache is re-read, merged then re-write with changes rather
  than rewritten with potentially hours out of date historic data
- Add operations `shared-drive-permissions remove` and `restore` to remove all
  permissions from a shared drive after saving a recovery file, and restore the
  permissions saved in a recovery file to a shared drive, respectively.
- Added `last_removed` and `last_restored` to shared drive report output

## [1.1.6] - 2024-03-08

### Added

- Retrieve augmented permissions on files in shared drives when scanning them
- Show these augmented permissions in the shared drive report
- Recognise lookup groups in the shared drive permissions
- Add a management user to shared drives when no manager could be used for scanning
- Hide this management user from shared drive reports
- Include createdTime and orgUnitId in shared drive report as we are getting them

## [1.1.5] - 2024-03-01

### Added

- shared-drive-report operation to generate csv of all shared drives with usage,
  institutions and users with various roles, active status and @cam domain

## [1.1.4] - 2024-02-29

### Added

- Separate when a shared drive scan was attempted and successful
- Add orgUnitId and createdTime to shared drive cached file

## [1.1.3] - 2024-02-12

### Added

- If any scanned user has their action set to needing manual scanning ("manual-*")
  then exit the operation with an error (to be detected by pipeline run)

## [1.1.2] - 2023-10-17

### Added

- Additional options for "recover" operation:
   - --user=CRSid to target a single user without needing to scan all users for
     those marked for recovery
   - --rescan to scan for existing permissions and only add those remaining

## [1.1.1] - 2023-07-10

### Updated

- Hotfix errors with report generation

## [1.1.0] - 2023-07-06

### Updated

- Allow reports for institutions to optionally recursively include child
  institutions.
- Add institutions to the report outputs
- Allow report output (CSVs) to be saved to a Google Drive location
- Add --request report option that searches for a "report-request-{type}-{id}"
  file in the report output location
- Option for local caching of user MyDrive usage for multiple dev report runs

## [1.0.2] - 2023-06-26

### Updated

- Use PyYAML CLoader/CDumper for dealing with large recovery documents

## [1.0.1] - 2023-06-22

### Updated

- Check if manager can access shared drive before attempt to list files
- Permission document cleaning trashes rather than deletes unwanted files

## [1.0.0] - 2023-06-16

### Updated

- Change `--really-do-this` to `--write`.

### Added

- New command for cleaning up permissions documents.

## [0.2.6] - 2023-06-08

### Updated

- Change "invalidSharingRequest" handling to capture missing Google accounts
  and retry the batch (and future batches) excluding requests for these
- Retries (typically from quota limits) double in length each time, allowing a
  faster initial retry and pseudo-exponential further retries

## [0.2.5] - 2023-06-06

### Added

- Add an ignoring 400s errors with a reason "invalidSharingRequest"
- Operation (reset-manual) to set all users marked for manual attention (action
  starting with "manual-") to appropriate 'normal' scan action.
- Allow reporting by Lookup group in addition to individual and institution

## [0.2.4] - 2023-06-05

### Added

- Configurable timeout when scanning a user's files/folders, marking them for
  manual attention

## [0.2.3] - 2023-06-05

### Updated

- Modify recovery operation to mark permissions document as historic once it has
  been processed.

## [0.2.2] - 2023-05-26

### Updated

- Skip recovering user permissions if the emailAddress is blank
- Test Drive API connection when attempting to connect as a manager to get the
  file usage of a shared drive

## [0.2.1] - 2023-05-23

### Updated

- Reduce updates to shared drive cache to a configurable minimum number of minutes
- Get all users from Google instead of performing individual existence checks
  when reporting for a large number of users

## [0.2.0] - 2023-05-18

### Added

- Configuration and Connection for querying Lookup via API Gateway
- Operation to scan shared drives, count file and usage values and maintain a
  cache for use by other operations
- Operation to report on MyDrive and shared drive usage for institutions or
  individual users

## [0.1.3] - 2023-05-04

### Updated

- Changed the scan operation significantly to split folder and file scans into
  separate elements, to drastically improve speed of scan runs.
- Improved scan resilience if failures occur mid-operation.

## [0.1.2] - 2023-04-21

### Updated

- Sort shared files to be removed by folders first then by their depth
- Include parents and whether the file resource is a folder in recovery documents

## [0.1.1] - 2023-04-13

### Updated

- Update custom schema values immediately after removing permissions
- Ignore 404s of individual requests when removing batches of file permissions
- Safety check for previously created recovery document
- Use earliest file for recovery

## [0.1.0] - 2023-04-06

### Added

- Initial version.
- Scan functionality to scan and remove permissions from users.
- Recover functionality to recover shared permissions.
